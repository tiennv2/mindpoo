<?php

namespace ZendX\Functions;

/**
 * Shortcode Class
 * Author: DN
 * Description: Tạo mã ngắn để sử dụng trong bài đăng (Giống WordPress)
 */
class Shortcode
{
    public function __construct($_model)
    {
        $this->_model = $_model;
    }

    public function build_shortcode($code)
    {
        $arr_item = [];
        $code = explode(',', $code);
        foreach ($code as $item_code) {
            $arr_code = explode('-', $item_code);
            if(count($arr_code) > 1){
                switch ($arr_code[0]) {
                    case 'p':
                        $table = 'Post\Model\PostItemTable';
                        break;
                    case 'c':
                        $table = 'Post\Model\PostCategoryTable';
                        break;
                    case 'l':
                        $table = 'Post\Model\PageTable';
                        break;
                    default:
                        $table = 'Post\Model\PostItemTable';
                        break;
                }
                $index_code = strip_tags($arr_code[1]);
            }else{
                $index_code = strip_tags($arr_code[0]);
            }
            $item = $this->_model->get($table)->getItem(array('index' => $index_code), array('task' => 'get-by-index'));
            if($item) $arr_item[] = array('name' => $item['name'], 'alias' => $item['alias']);
        }

        $xhtml = '';
        if($arr_item){
            $xhtml  .=  '<div class="clearfix"></div>';
            $xhtml  .=  '<div class="hdit-related-posts">';
            $xhtml  .=  '<h4 class="title"><i class="fas fa-link"></i> Có thể bạn quan tâm:</h4>';
            $xhtml  .=  '<ul>';
            foreach ($arr_item as $item) {
                $xhtml  .=  '<li>
                                <i class="fas fa-angle-right"></i>
                                <a style="color:#289DCC; margin-left: 5px;" href="'. DOMAIN .'/'. $item['alias'] .'" target="_blank">'. $item['name'] .'</a>
                            </li>';
            }
            $xhtml  .=  '</ul>';
            $xhtml  .=  '</div>';
            $xhtml  .=  '<div class="clearfix"></div>';
        }

        return $xhtml;
    }

    public function callback_shortcode($content)
    {
        return preg_replace_callback('~\\[post=(.+?)\]~', function($match) {
            return $this->build_shortcode($match[1]);
        }, $content);
    }
}