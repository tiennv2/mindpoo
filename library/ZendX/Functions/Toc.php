<?php
namespace ZendX\Functions;
class Toc {
    private $options;
    public function __construct() {
        $this->options = array();
        $this->options['heading_levels'] = array('1', '2', '3', '4', '5', '6');
    }
    // convert accented characters to ASCII
    private function remove_accents($str) {
        $filter = new \ZendX\Filter\CreateAlias();
        return $filter->filter($str);
    }
    // Returns a clean url to be used as the destination anchor target
    private function url_anchor_target( $title ) {
        $return = false;
        if ( $title ) {
            $return = trim( strip_tags($title) );
            // convert accented characters to ASCII
            $return = $this->remove_accents($return);
            // replace newlines with spaces (eg when headings are split over multiple lines)
            $return = str_replace( array("\r", "\n", "\n\r", "\r\n"), ' ', $return );
            // remove &amp;
            $return = str_replace( '&amp;', '', $return );
            // remove non alphanumeric chars
            $return = preg_replace( '/[^a-zA-Z0-9 \-_]*/', '', $return );
            // convert spaces to _
            $return = str_replace(
                array('  ', ' '),
                '_',
                $return
            );
            // remove trailing - and _
            $return = rtrim( $return, '-_' );
            // lowercase everything?
            if ( $this->options['lowercase'] ) $return = strtolower($return);
            // if blank, then prepend with the fragment prefix
            // blank anchors normally appear on sites that don't use the latin charset
            if ( !$return ) {
                $return = ( $this->options['fragment_prefix'] ) ? $this->options['fragment_prefix'] : '_';
            }
            // hyphenate?
            if ( $this->options['hyphenate'] ) {
                $return = str_replace('_', '-', $return);
                $return = str_replace('--', '-', $return);
            }
        }
        return $return;
    }
    private function build_hierarchy( &$matches ) {
        $current_depth = 100;	// headings can't be larger than h6 but 100 as a default to be sure
        $html = '';
        $numbered_items = array();
        $numbered_items_min = null;
        // reset the internal collision collection
        $this->collision_collector = array();
        // find the minimum heading to establish our baseline
        for ($i = 0; $i < count($matches); $i++) {
            if ( $current_depth > $matches[$i][2] )
                $current_depth = (int)$matches[$i][2];
        }
        $numbered_items[$current_depth] = 0;
        $numbered_items_min = $current_depth;
        for ($i = 0; $i < count($matches); $i++) {
            if ( $current_depth == (int)$matches[$i][2] )
                $html .= '<li>';
            // start lists
            if ( $current_depth != (int)$matches[$i][2] ) {
                for ($current_depth; $current_depth < (int)$matches[$i][2]; $current_depth++) {
                    $numbered_items[$current_depth + 1] = 0;
                    $html .= '<ul><li>';
                }
            }
            // list item
            if ( in_array($matches[$i][2], $this->options['heading_levels']) ) {
                $html .= '<a href="#' . $this->url_anchor_target( $matches[$i][0] ) . '">';
                if ( $this->options['ordered_list'] ) {
                    // attach leading numbers when lower in hierarchy
                    $html .= '<span class="toc_number toc_depth_' . ($current_depth - $numbered_items_min + 1) . '">';
                    for ($j = $numbered_items_min; $j < $current_depth; $j++) {
                        $number = ($numbered_items[$j]) ? $numbered_items[$j] : 0;
                        $html .= $number . '.';
                    }
                    $html .= ($numbered_items[$current_depth] + 1) . '</span> ';
                    $numbered_items[$current_depth]++;
                }
                $html .= strip_tags($matches[$i][0]) . '</a>';
            }

            // end lists
            if ( $i != count($matches) - 1 ) {
                if ( $current_depth > (int)$matches[$i + 1][2] ) {
                    for ($current_depth; $current_depth > (int)$matches[$i + 1][2]; $current_depth--) {
                        $html .= '</li></ul>';
                        $numbered_items[$current_depth] = 0;
                    }
                }
                if ( $current_depth == (int)@$matches[$i + 1][2] )
                    $html .= '</li>';
            }
            else {
                // this is the last item, make sure we close off all tags
                for ($current_depth; $current_depth >= $numbered_items_min; $current_depth--) {
                    $html .= '</li>';
                    if ( $current_depth != $numbered_items_min ) $html .= '</ul>';
                }
            }
        }
        return $html;
    }
    public function extract_headings( &$find, &$replace, $content = '', $options = null ) {
        $matches = array();
        $anchor = '';
        $items = false;
        // reset the internal collision collection as the_content may have been triggered elsewhere
        // eg by themes or other plugins that need to read in content such as metadata fields in
        // the head html tag, or to provide descriptions to twitter/facebook
        $this->collision_collector = array();
        if ( is_array($find) && is_array($replace) && $content ) {
            // get all headings
            // the html spec allows for a maximum of 6 heading depths
            if ( preg_match_all('/(<h([1-6]{1})[^>]*>).*<\/h\2>/msuU', $content, $matches, PREG_SET_ORDER) ) {
                // remove undesired headings (if any) as defined by heading_levels
                if ( count($this->options['heading_levels']) != 6 ) {
                    $new_matches = array();
                    for ($i = 0; $i < count($matches); $i++) {
                        if ( in_array($matches[$i][2], $this->options['heading_levels']) )
                            $new_matches[] = $matches[$i];
                    }
                    $matches = $new_matches;
                }
                // remove specific headings if provided via the 'exclude' property
                if ( $this->options['exclude'] ) {
                    $excluded_headings = explode('|', $this->options['exclude']);
                    if ( count($excluded_headings) > 0 ) {
                        for ($j = 0; $j < count($excluded_headings); $j++) {
                            // escape some regular expression characters
                            // others: http://www.php.net/manual/en/regexp.reference.meta.php
                            $excluded_headings[$j] = str_replace(
                                array('*'),
                                array('.*'),
                                trim($excluded_headings[$j])
                            );
                        }
                        $new_matches = array();
                        for ($i = 0; $i < count($matches); $i++) {
                            $found = false;
                            for ($j = 0; $j < count($excluded_headings); $j++) {
                                if ( @preg_match('/^' . $excluded_headings[$j] . '$/imU', strip_tags($matches[$i][0])) ) {
                                    $found = true;
                                    break;
                                }
                            }
                            if (!$found) $new_matches[] = $matches[$i];
                        }
                        if ( count($matches) != count($new_matches) )
                            $matches = $new_matches;
                    }
                }
                // remove empty headings
                $new_matches = array();
                for ($i = 0; $i < count($matches); $i++) {
                    if ( trim( strip_tags($matches[$i][0]) ) != false )
                        $new_matches[] = $matches[$i];
                }
                if ( count($matches) != count($new_matches) )
                    $matches = $new_matches;
                // check minimum number of headings
                if ( count($matches) >= $this->options['start'] ) {
                    for ($i = 0; $i < count($matches); $i++) {
                        // get anchor and add to find and replace arrays
                        $anchor = $this->url_anchor_target( $matches[$i][0] );
                        $find[] = $matches[$i][0];
                        $replace[] = str_replace(
                            array(
                                $matches[$i][1],				// start of heading
                                '</h' . $matches[$i][2] . '>'	// end of heading
                            ),
                            array(
                                $matches[$i][1] . '<span id="' . $anchor . '">',
                                '</span></h' . $matches[$i][2] . '>'
                            ),
                            $matches[$i][0]
                        );
                        // assemble flat list
                        if ( !$this->options['show_heirarchy'] ) {
                            $items .= '<li><a href="#' . $anchor . '">';
                            if ( $this->options['ordered_list'] ) $items .= count($replace) . ' ';
                            $items .= strip_tags($matches[$i][0]) . '</a></li>';
                        }
                    }
                    // build a hierarchical toc?
                    // we could have tested for $items but that var can be quite large in some cases
                    $items = $this->build_hierarchy( $matches );
                }
            }
        }
        if(!empty($items)) {
            $class = !empty($options['toc_class_default']) ? ' '. $options['toc_class_default'] : '';
            $items = '<div class="box_toc'. $class .'" id="box_toc">
                        <div class="toc_title">
                            Nội dung 
                            <span class="control" onclick="javascript:$(this).parents(\'#box_toc\').find(\'.toc_content\').slideToggle(); $(this).find(\'span\').toggle();">
                                <span>[Hiện]</span>
                                <span style="display: none;">[Ẩn]</span>
                            </span>
                        </div>
                        <div class="toc_content">
                            <ul class="toc_list">'. $items .'</ul>
                        </div>
                    </div>
                    <style type="text/css" media="screen">
                        .box_toc{ margin-bottom: 15px; border: 1px solid #999; max-width: 300px;}
                        .box_toc .toc_list li a{word-wrap: break-word;}
                        .box_toc .toc_list ul ul{list-style: none;}
                        .box_toc .toc_title{ padding: 15px; font-size: 16px; font-weight: 500; display: flex; justify-content: center;}
                        .box_toc .toc_title span{ color: #237c3a; cursor: pointer; margin-left: 5px;}
                        .box_toc .toc_title span:hover{ color: #000;}
                        .box_toc .toc_content{ display: none; padding: 0 15px 15px; font-size: 15px;}
                        .box_toc .toc_content ul{ padding-left: 15px;}
                        @media screen and (min-width: 550px){
                            .box_toc{float: right; margin-left: 15px; }
                        }
                    </style>
                    ';
        }
        return $items;
    }
    public function mb_find_replace( &$find = false, &$replace = false, &$string = '' ) {
        if ( is_array($find) && is_array($replace) && $string ) {
            // check if multibyte strings are supported
            if ( function_exists( 'mb_strpos' ) ) {
                for ($i = 0; $i < count($find); $i++) {
                    $string =
                        mb_substr( $string, 0, mb_strpos($string, $find[$i]) ) .	// everything befor $find
                        $replace[$i] .												// its replacement
                        mb_substr( $string, mb_strpos($string, $find[$i]) + mb_strlen($find[$i]) )	// everything after $find
                    ;
                }
            }
            else {
                for ($i = 0; $i < count($find); $i++) {
                    $string = substr_replace(
                        $string,
                        $replace[$i],
                        strpos($string, $find[$i]),
                        strlen($find[$i])
                    );
                }
            }
        }
        return $string;
    }
    //* Add lazy class for data-src attribute images
    public function replace_images( $content, $options ) {
        $dom = new \DOMDocument();
        $dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
        foreach ($dom->getElementsByTagName('img') AS $key => $img) {
            if($options['img_lazy'] == 1) {
                $class = !empty($img->getAttribute('class')) ? $img->getAttribute('class') .' lazy' : 'lazy';
                $img->setAttribute( 'data-src', $img->getAttribute('src') );
                $img->setAttribute( 'class', $class );
                $img->removeAttribute( 'src' );
            }
            if(!empty($options['img_alt']) && empty($img->getAttribute('alt'))) {
                $alt = $options['img_alt'] .' - '. $key;
                $img->setAttribute( 'alt', $alt );
            }
        }
        $content = $dom->saveHTML();
        return $content;
    }
    public function the_content($content, $options = null) {
        if($options['toc_show'] == 1) {
            $find = $replace = array();
            $toc_list = $this->extract_headings($find, $replace, $content, $options);
            $content = $toc_list . $this->mb_find_replace($find, $replace, $content);
        }
        if($options['img_replace'] == 1) {
            $content = $this->replace_images($content, $options);
        }
        return $content;
    }
}