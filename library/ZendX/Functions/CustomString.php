<?php
namespace ZendX\Functions;

class CustomString {
    
    public function __construct() {
        
    }

    public function cutString($string, $number, $more_string = '...') {
        $words = explode(" ", $string, $number + 1);

        if(count($words) == $number + 1) {
            array_pop($words);
            return implode(" ", $words) . $more_string;
        }

        return implode(" ", $words);
    }

    public function stripTagsContent($string) {
        // ----- remove HTML TAGs -----
        $string = preg_replace ('/<[^>]*>/', ' ', $string);
        // ----- remove control characters -----
        $string = str_replace("&nbsp;", '', $string);
        $string = str_replace("\r", '', $string);
        $string = str_replace("\n", ' ', $string);
        $string = str_replace("\t", ' ', $string);
        // ----- remove multiple spaces -----
        $string = trim(preg_replace('/ {2,}/', ' ', $string));

        return $string;
    }

    public function convertQuestion($content, $arr_questions){
        $content = preg_replace_callback('/\([a-zA-Z0-9:;&%\s]+\)/i', function($m) use ($arr_questions) {
            $value  = explode(' ', str_replace('&nbsp;', ' ', preg_replace('/[\(\)]/i', '', $m[0])));
            $index  = $value[0];
            $style  = $value[1] ? ' style="'. $value[1] .'"' : '';
            $item_question = $arr_questions[$index];
            
            if($item_question['type'] == 'text') {
                $result = '<span class="answer-control type-input"><input type="text" name="answers['. $index .']" '. $style .' /></span>';
            }
            if($item_question['type'] == 'textarea') {
                $result = '<span class="answer-control type-input"><textarea name="answers['. $index .']" '. $style .'></textarea></span>';
            }
            
            if(in_array($item_question['type'], ['select', 'radio'])) {
                $str_option = '';
                if($item_question['type'] == 'select') $str_option = '<option value="">- Chọn -</option>';
                $d = 0;
                foreach (explode(';', $item_question['options']) AS $option_data) {
                    $d++;
                    $option_data = explode('__', trim($option_data));
                    $text_option = $option_data[0];
                    if(count($option_data) > 1) $text_option .= '. '. $option_data[1];
                    if($item_question['type'] == 'select') $str_option .= '<option value="'. $option_data[0] .'">'. trim($text_option) .'</option>';
                    if($item_question['type'] == 'radio'){
                        if($d>1) $str_option .= '<br />';
                        $str_option .= '<label> <input type="radio" name="answers['. $index .']" value="'. $option_data[0] .'"> <span>'. trim($text_option) .'</span> </label>';
                    };
                }
                if($item_question['type'] == 'select') $result = '<span class="answer-control type-select"><select name="answers['. $index .']" '. $style .'>'. $str_option .'</select></span>';
                if($item_question['type'] == 'radio') $result = '<div class="answer-control type-radio">'. $str_option .'</div>';
            }
            return $result;
        }, $content);
        return $content;
    }

    public function convertAnswer($content, $arr_questions, $arr_answers){
        $content = preg_replace_callback('/\([a-zA-Z0-9:;&%\s]+\)/i', function($m) use ($arr_questions, $arr_answers) {
            $value  = explode(' ', str_replace('&nbsp;', ' ', preg_replace('/[\(\)]/i', '', $m[0])));
            $index  = $value[0];
            $style  = $value[1] ? ' style="'. $value[1] .'"' : '';
            $item_question = $arr_questions[$index];
            
            if(in_array($item_question['type'], ['text','textarea'])) {
                $str_incorrect = ($arr_answers[$index] != $item_question['result']) ? '<span class="text-red line-through">'. $arr_answers[$index] .'</span>' : '';
                $class_option = ($arr_answers[$index] == $item_question['result']) ? 'text-green' : false;
                $result = '<span class="answer-result type-input"> '. $str_incorrect .'  <b class="'.  $class_option .'"><u>'. $item_question['result'] .'</u></b></span>';
            }
            
            if(in_array($item_question['type'], ['select','radio'])) {
                $str_option = '';
                foreach (explode(';', $item_question['options']) AS $key_option => $option_data) {
                    $option_data = explode('__', trim($option_data));
                    $class_option = 'incorrect';
                    $correct_option = false;
                    if($item_question['result'] == $option_data[0]){
                        $class_option   = 'text-bold';
                        $correct_option = true;
                    }
                    if(!empty($arr_answers[$index]) && $arr_answers[$index] ==  $option_data[0]){
                        if($correct_option) $class_option .= ' text-green';
                        else $class_option = 'text-red line-through';
                    }
                    $str_text_option = $option_data[0];
                    if(count($option_data) > 1) $str_text_option .= '. '. $option_data[1];
                    $str_option .= '<li class="'. $class_option .'">'. trim($str_text_option) .'</li>';
                }
                $result = '<ul class="answer-result type-choose">'. $str_option .'</ul>';
            }
        
            return $result;
        }, $content);
        return $content;
    }
}