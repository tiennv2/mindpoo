<?php
namespace ZendX\System;

use Zend\Session\Container;

class ContactInfo {
	
	public function __construct(){
		$ssInfo	= new Container(APP_KEY . '_contact');
	}
	
	public function storeInfo($data){
		$ssInfo	= new Container(APP_KEY . '_contact');
		//$ssInfo->setExpirationSeconds(7200);
		$ssInfo->contact	= $data['contact'];
	}
	
	public function destroyInfo(){
		$ssInfo	= new Container(APP_KEY . '_contact');
		$ssInfo->getManager()->getStorage()->clear(APP_KEY . '_contact');
	}
	
	public function getContactInfo($element = null){
		$ssInfo = new Container(APP_KEY . '_contact');
		$contactInfo = $ssInfo->contact;
		
		$result	= ($element == null) ? $contactInfo : $contactInfo->$element;
		return $result;
	}
	
	public function setContactInfo($element, $value){
		$ssInfo = new Container(APP_KEY . '_contact');
		$ssInfo->contact[$element] = $value;
	}
}