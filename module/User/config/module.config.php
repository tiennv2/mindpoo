<?php

namespace User;

return array (
	'controllers' => array(
		'invokables' => array(
			'User\Controller\Index' => Controller\IndexController::class,
			'User\Controller\Course' => Controller\CourseController::class,
		)
	),
    'view_manager' => array(
        'doctype'					=> 'HTML5',
        'template_path_stack'		=> array(__DIR__ . '/../view'),
    )
);


