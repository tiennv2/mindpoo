<?php
namespace User\Filter;

class Login {
	protected $_error;
	
	public function __construct($data = null, $sm){
	    $dataParam = $data['data'];
	    
	    if(!empty($dataParam)) {
	        if(empty($dataParam['username'])) {
	            return $this->_error = 'Điện thoại hoặc Email bắt buộc phải nhập';
	        }
	        
	        if(empty($dataParam['password'])) {
	            return $this->_error = 'Mật khẩu bắt buộc phải nhập';
	        }
	    }
	}
	
	public function getError() {
	    return $this->_error;
	} 
}