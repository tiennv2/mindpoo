<?php
namespace User\Filter;

class Register {
	protected $_error;
	
	public function __construct($data = null, $sm){
	    $dataParam = $data['data'];
	    
	    if(!empty($dataParam)) {
	        if(empty($dataParam['name'])) {
	            return $this->_error = 'Họ tên bắt buộc phải nhập';
	        }
	        
	        if(empty($dataParam['phone'])) {
	            return $this->_error = 'Số điện thoại bắt buộc phải nhập';
	        } else {
	            $contact = $sm->get('Admin\Model\ContactTable')->getItem(array('phone' => $dataParam['phone']), array('task' => 'by-phone'));
	            if(!empty($contact)) {
	                return $this->_error = 'Tài khoản đã tồn tại.';
	            }
	        }
	        
	        if(empty($dataParam['email'])) {
	            return $this->_error = 'Email bắt buộc phải nhập';
	        } else {
	            $validator = new \Zend\Validator\EmailAddress();
	            if (!$validator->isValid($dataParam['email'])) {
	                return $this->_error = 'Email không đúng định dạng';
	            }
	        }
	        
	        if(empty($dataParam['password'])) {
	            return $this->_error = 'Mật khẩu bắt buộc phải nhập';
	        } else {
	            $validator = new \Zend\Validator\StringLength(array('min' => 6));
	            if (!$validator->isValid($dataParam['password'])) {
	                return $this->_error = 'Mật khẩu phải lớn hơn 6 ký tự';
	            }
	        }
	    }
	}
	
	public function getError() {
	    return $this->_error;
	} 
}