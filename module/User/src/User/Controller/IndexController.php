<?php

namespace User\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use ZendX\System\ContactInfo;
use Zend\Form\FormInterface;
use Zend\Session\Container;

class IndexController extends ActionController {
    protected $_settings;
    protected $_userInfo;
    
    public function init() {
    	// Thiết lập session filter
    	$ssFilter = new Container('User');
    	
        $this->setLayout('layout/user');
        
        // Lấy thông tin user đăng nhập
        $userInfo = new ContactInfo();
        $this->_userInfo = $userInfo->getContactInfo();
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 5;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data']  = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray(), $this->params()->fromRoute());
        $this->_params['route'] = $this->params()->fromRoute();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['language'] = $this->_language;
        $this->_viewModel['params'] = $this->_params;
        
        // Thiết lập Meta
        $this->_params['meta'] = array(
        	'author' => $this->_settings['General.Meta.Author']['value'],
        	'image' => $this->_settings['General.Meta.Image']['image'],
        	'title' => $this->_settings['General.Meta.Title']['value'],
        	'description' => $this->_settings['General.Meta.Description']['description'],
        	'keywords' => $this->_settings['General.Meta.Keywords']['description'],
        );
        
        if($this->_params['action'] != 'login' && $this->_params['action'] != 'logout' && $this->_params['action'] != 'register' && $this->_params['action'] != 'forgot-password' && $this->_params['action'] != 'confirm-forgot-password') {
        	if(empty($this->_userInfo)) {
        		if(!empty($_GET['utm_source'])) {
        			$ssFilter->utm_source = $_GET['utm_source'];
        		}
        		$this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'login'));
        	}
        }
    }
    
    public function indexAction() {
    	$this->_params['meta']['title'] = $this->_language['member'][$this->_language['language_active']];
        $product = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->listItem(null, array('task' => 'cache-product'));
        $product_cart = $this->getServiceLocator()->get('Admin\Model\ProductCartTable')->listItem(array('contact_id' => $this->_userInfo['id']), array('task' => 'by-contact'));
        $this->_viewModel['course_item']         = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(array( "where" => array( "type" => "free" )), array('task' => 'list-free'));
        $this->_viewModel['course_category']     = $this->getServiceLocator()->get('Admin\Model\CourseCategoryTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['course_item_default'] = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['contact'] = $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $this->_userInfo['id']));
        $this->_viewModel['product'] = $product;
        $this->_viewModel['product_cart'] = $product_cart;
        return new ViewModel($this->_viewModel);
    }
    
    public function updateProfileAction() {
        $this->_params['meta']['title'] = $this->_language['update_profile'][$this->_language['language_active']];
        $userInfo = new Container('User');
         
        $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $this->_userInfo['id']));
        
        $myForm = new \User\Form\Contact($this->getServiceLocator());
        $myForm->setInputFilter(new \User\Filter\Contact(array('id' => $contact['id'])));
        $myForm->bind($contact);
        
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
        
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $this->_params['data']['id'] = $this->_userInfo['id'];
                
                $result = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'edit-item'));
        
                $this->flashMessenger()->addMessage($this->_language['data_post_success'][$this->_language['language_active']]);
                return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
            }
        }
        
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['contact']    = $contact;
        return new ViewModel($this->_viewModel);
    }
    
    // Chưa update lên erp
    public function changePasswordAction() {
    	$this->_params['meta']['title'] = 'Thay đổi mật khẩu';
    	
        $contact_erp = json_decode(\ZendX\Functions\CallApi::getContent(DOMAIN_ERP. '/api/langgo-contact/get-info', ['id' => $this->_userInfo['id']]), true);
        $contact = $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $this->_userInfo['id']));
        if($this->getRequest()->isPost()){
            $post   = $this->_params['data'];
            $errors = array();
            $flag   = true;
            
            if(md5($post['password_current']) != $contact['password']) {
                $errors[] = '<b>Mật khẩu cũ:</b> không chính xác';
                $flag = false;
            }
            
            $validator = new \Zend\Validator\StringLength(array('min' => 6));
            if (!$validator->isValid($post['password'])) {
                $errors[] = '<b>Mật khẩu mới:</b> phải lớn hơn 6 ký tự và bao gồm cả chữ và số';
                $flag = false;
            }
            
            if(md5($post['password']) != md5($post['password_confirm'])) {
                $errors[] = '<b>Xác nhận mật khẩu mới:</b> không đúng';
                $flag = false;
            }
            
            if($flag == true){
                $this->_params['data']['id']   = $contact['id'];
                $this->_params['data']['task'] = 'change-password';
                $result = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'change-password'));
    
                $this->flashMessenger()->addMessage('Mật khẩu của bạn đã được đổi thành công.');
                return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
            }
        }
    
        $this->_viewModel['errors']     = $errors;
        $this->_viewModel['contact']    = $contact;
        return new ViewModel($this->_viewModel);
    }
    
    public function loginAction() {
        // Thiết lập session filter
        $ssFilter = new Container('User');
        
        $this->_params['meta']['title'] = 'Đăng nhập';
        
        $userInfo = new ContactInfo();
        if(!empty($userInfo->getContactInfo())) {
            if(!empty($_GET['redirect'])) {
                return $this->redirect()->toUrl($_GET['redirect']);
            } else {
                return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
            }
        }
        
        $result = array();
        if($this->getRequest()->isPost()) {
            $filter = new \User\Filter\Login($this->_params, $this->getServiceLocator());
        
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem($this->_params['data'], array('task' => 'login'));
                
                if(empty($contact)) {
                    $result['error'] = 'Tài khoản hoặc mật khẩu không đúng';
                } else {
                    $userInfo = new ContactInfo();
                    $userInfo->storeInfo(array('contact' => $contact));
                    
                    return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
                }
            }
        }
        
        $this->_viewModel['result'] = $result;
        return new ViewModel($this->_viewModel);
    }
    
    public function logoutAction() {
        $userInfo = new ContactInfo();
        $userInfo->destroyInfo();
    
        return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'login'));
    }
    
    public function registerAction() {
    	// Thiết lập session filter
    	$ssFilter = new Container('User');
    	$this->_params['meta']['title'] = 'Đăng ký tài khoản';
    	
        $result = array();
        if($this->getRequest()->isPost()) {
            $filter = new \User\Filter\Register($this->_params, $this->getServiceLocator());
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                $id = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'add-item'));
                $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $id), null);
                // Đăng nhập luôn
                if(empty($contact)) {
                    $result['error'] = 'Tài khoản hoặc mật khẩu không đúng';
                } else {
                    $userInfo = new ContactInfo();
                    $userInfo->storeInfo(array('contact' => $contact));
                    
                    $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
                }
                $this->flashMessenger()->addMessage('Đăng ký tài khoản thành công.');
            }
        }
        
        $this->_viewModel['result'] = $result;
        return new ViewModel($this->_viewModel);
    }
    
    // Chưa update lên erp
    public function forgotPasswordAction() {
    	$this->_params['meta']['title'] = 'Lấy lại mật khẩu';
    	
        $result = array();
        if($this->getRequest()->isPost()) {
            $filter = new \User\Filter\ForgotPassword($this->_params, $this->getServiceLocator());
    
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                $contact_erp = json_decode(\ZendX\Functions\CallApi::getContent(DOMAIN_ERP. '/api/langgo-contact/get-info', ['email' => $this->_params['data']['email'], 'task' => 'by-email']), true);
                $contact = $contact_erp['contact'];
                if(empty($contact)) {
                    $result['error'] = 'Địa chỉ email không tồn tại';
                } else {
                    $gid           = new \ZendX\Functions\Gid();
                    $password_code = $gid->random(6);
                    $result        = json_decode(\ZendX\Functions\CallApi::getContent(DOMAIN_ERP. '/api/langgo-contact/update-contact', ['id' => $contact['id'], 'password_code' => $password_code]), true);
                    if($result['success']){
                        // Gửi email
                        $linkPassword = $this->url()->fromRoute('routeUser/default', array('controller' => 'index', 'action' => 'confirm-forgot-password', 'id' => $contact['id'], 'code' => $password_code), array('force_canonical' => true));
                        
                        $options['fromName']    = strtoupper($_SERVER['HTTP_HOST']);
                        $options['to']          = $contact['email'];
                        $options['toName']      = $contact['name'];
                        $options['subject']     = 'XAC NHAN LAY LAI MAT KHAU';
                        $options['content']     = '
                            <p>Xin chào: <strong>'. $contact['name'] .'</strong></p>
                            <p>
                            	Theo yêu cầu xin lại mật khẩu mới của bạn. Chúng tôi gửi mail này để xác nhận có phải bạn muốn xin lại mật khẩu.<br> 
                               	Nếu đồng ý Bạn vui lòng nhấn vào liên kết bên dưới để xác nhận mật khẩu mới.
                            </p>
                            <p><a href="'. $linkPassword .'">'. $linkPassword .'</a></p>
                            <p>Nếu Bạn không thực hiện việc yêu cầu lấy lại mật khẩu vui lòng xóa email này.</p>
                        ';
                        $mailObj = new \ZendX\Mail\Mail();
                        $mailObj->sendMail($options);
                        
                        $this->flashMessenger()->addMessage('Mật khẩu đã được gửi vào địa chỉ email của bạn. Vui lòng kiểm tra trong Inbox hoặc Spam');
                        return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'forgot-password'));
                    }
                }
            }
        }
    
        $this->_viewModel['result'] = $result;
        return new ViewModel($this->_viewModel);
    }
    
    // Chưa update lên erp
    public function confirmForgotPasswordAction() {
    	$this->_params['meta']['title'] = 'Xác nhận lấy lại mật khẩu';
    	
        $contact_erp = json_decode(\ZendX\Functions\CallApi::getContent(DOMAIN_ERP. '/api/langgo-contact/get-info', array('id' => $this->_params['route']['id'], 'password_code' => $this->_params['route']['code'], 'task' => 'forgot-password')), true);
        $contact = $contact_erp['contact'];

        if(empty($this->_params['route']['id']) || empty($this->_params['route']['code']) || empty($contact)) {
            return $this->redirect()->toRoute('routePost/default', array('controller' => 'notice', 'action' => 'no-data'));
        }
        
        $result = array();
        if($this->getRequest()->isPost()) {
            $filter = new \User\Filter\ConfirmForgotPassword($this->_params, $this->getServiceLocator());
    
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                // Update password mới
                $this->_params['data']['id']   = $contact['id'];
                $this->_params['data']['task'] = 'change-password';
                $result = \ZendX\Functions\CallApi::getContent(DOMAIN_ERP. '/api/langgo-contact/update-contact', $this->_params['data']);
                
                // Đăng nhập
                $userInfo = new ContactInfo();
                $userInfo->storeInfo(array('contact' => $contact));
                
                return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
            }
        }
    
        $this->_viewModel['result'] = $result;
        return new ViewModel($this->_viewModel);
    }
}
