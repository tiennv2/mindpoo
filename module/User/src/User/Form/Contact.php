<?php
namespace User\Form;
use \Zend\Form\Form as Form;

class Contact extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Họ và tên
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder' => 'Họ tên'
			),
		));
		
		// Điện thoại
		$this->add(array(
			'name'			=> 'phone',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control mask_phone',
				'placeholder' => 'Số điện thoại'
			),
		));
		
		// Email
		$this->add(array(
			'name'			=> 'email',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder' => 'Địa chỉ email'
			),
		));
		
		// Địa chỉ
		$this->add(array(
			'name'			=> 'address',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder' => 'Địa chỉ'
			),
		));
	}
}