<?php
namespace Admin\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Import extends InputFilter {
	
	public function __construct($options = null){
	    $dbAdapter     = GlobalAdapterFeature::getStaticAdapter();
	    $optionId      = $options['id'];
	    $optionData    = $options['data'];
	    $optionRoute   = $options['route'];
	    
		// File Import
		$this->add(array(
			'name'		=> 'file_import',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				),
				/* array(
					'name'		=> 'FileSize',
					'options'	=> array(
						'min'	=> '10Kb',
						'max'	=> '2MB',
					),
					'break_chain_on_failure'	=> true
				), */
				array(
					'name'		=> 'FileExtension',
					'options'	=> array(
						'extension'		=> array('xlsx'),
						'messages'	=> array(
							\Zend\Validator\File\Extension::FALSE_EXTENSION => 'Chỉ chấp nhập định dạng excel .xlsx'
						)
					),
					'break_chain_on_failure'	=> true
				),
			)
		));
	}
}