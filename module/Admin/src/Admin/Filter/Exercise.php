<?php
namespace Admin\Filter;

use Zend\InputFilter\InputFilter;

class Exercise extends InputFilter {
	
	public function __construct($options = null){
		$data = $options['data'];
	    $exclude = null;
	    if(!empty($data['id'])) {
	        $exclude = array(
	            'field' => 'id',
	            'value' => $data['id']
	        );
	    }
		// Name
		$this->add(array(
			'name'		=> 'name',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				)
			)
		));

		// type
		$this->add(array(
			'name'		=> 'type',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'    => 'NotEmpty',
					'options' => array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure' => true
				)
			)
		));

		// time_limit
		$time_limit = array(
			'name'		=> 'time_limit',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'    => 'NotEmpty',
					'options' => array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure' => true
				)
			)
		);

		switch ($data['type']) {
			case 'listening':
				$this->add(array(
					'name'		=> 'audio',
					'required'	=> true,
					'validators'	=> array(
						array(
							'name'    => 'NotEmpty',
							'options' => array(
						        'messages'	=> array(
						            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
						        )
						    ),
							'break_chain_on_failure' => true
						)
					)
				));
				break;
			case 'writing':
				$this->add(array(
					'name'		=> 'min_word',
					'required'	=> true,
					'validators'	=> array(
						array(
							'name'    => 'NotEmpty',
							'options' => array(
						        'messages'	=> array(
						            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
						        )
						    ),
							'break_chain_on_failure' => true
						)
					)
				));
				break;
			case 'speaking':
				$time_limit = array(
					'name'		=> 'time_limit',
					'required'	=> false,
				);
				break;
		}
		
		$this->add($time_limit);

		// Ordering
		$this->add(array(
		    'name'		=> 'ordering',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'Digits',
		            'break_chain_on_failure'	=> true
		        )
		    )
		));
		
		// Status
		$this->add(array(
		    'name'		=> 'status',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'break_chain_on_failure'	=> true
		        )
		    )
		));
		
		// Layout
		$this->add(array(
		    'name'		=> 'layout',
		    'required'	=> false,
		));
	}
}