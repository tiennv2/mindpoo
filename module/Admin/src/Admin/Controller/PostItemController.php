<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class PostItemController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\PostItemTable';
        $this->_options['formName'] = 'formAdminPostItem';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']          = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'created';
        $this->_params['ssFilter']['order']             = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_keyword']    = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_status']     = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_category']   = $ssFilter->filter_category;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage']  = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge( $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray() );
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
    
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option    = intval($data['pagination_option']);
    
            $ssFilter->order_by             = $data['order_by'];
            $ssFilter->order                = $data['order'];
    
            $ssFilter->filter_keyword       = $data['filter_keyword'];
            $ssFilter->filter_status        = $data['filter_status'];
            $ssFilter->filter_category      = $data['filter_category'];

            if(!empty($data['filter_reset'])){
                $ssFilter->getManager()->getStorage()->clear(__CLASS__ . $action);
            }
        }
    
        $this->goRoute();
    }
    
    public function indexAction() {
        $myForm	= new \Admin\Form\Search\PostItem($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['items']      = $items;
        $this->_viewModel['count']      = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['type']       = array( 'default'	=> 'Bài viết', 'product' => 'Sản phẩm', 'gallery' => 'Album ảnh', 'video' => 'Video', 'file' => 'Tài liệu');
        $this->_viewModel['caption']    = 'Bài viết - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    public function formAction() {
        $ssFilter   = new Container(__CLASS__ . 'Form');
        $myForm     = $this->getForm();
        
        $task = 'add-item';
        $caption = 'Bài viết - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\PostItem(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Bài viết - Sửa';
            }
        }
        
        if($this->_params['route']['code'] == 'copy') {
            $caption = 'Bài viết - Copy';
            $task = 'add-item';
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            $images = $this->_params['data']['images'];
            $videos = $this->_params['data']['videos'];
            $product_model = $this->_params['data']['product_model'];
            
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $this->_params['data']['images'] = $images;
                $this->_params['data']['videos'] = $videos;
                $this->_params['data']['product_model'] = $product_model;
                $last_shortcode_item = $this->getTable()->getItem(null, array('task' => 'last-shortcode'));
                $this->_params['data']['shortcode'] = $last_shortcode_item['shortcode'] ? $last_shortcode_item['shortcode']+1 : 1;
                
                // Lưu lại category đã chọn
                $ssData['post_category_id'] = $this->_params['data']['post_category_id'];
                $ssFilter->data = $ssData;
                
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'form'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'form', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        $this->_viewModel['ssData']     = $ssFilter->data;
        return new ViewModel($this->_viewModel);
    }
}
