<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class ProductController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\PostItemTable';
        $this->_options['formName'] = 'formAdminProduct';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']              = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'created';
        $this->_params['ssFilter']['order']                 = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_keyword']        = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_status']         = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_category']       = $ssFilter->filter_category;
        $this->_params['ssFilter']['filter_product_status'] = $ssFilter->filter_product_status;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage']  = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge( $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray() );
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
    
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option        = intval($data['pagination_option']);
            $ssFilter->order_by                 = $data['order_by'];
            $ssFilter->order                    = $data['order'];
            $ssFilter->filter_keyword           = $data['filter_keyword'];
            $ssFilter->filter_status            = $data['filter_status'];
            $ssFilter->filter_category          = $data['filter_category'];
            $ssFilter->filter_product_status    = $data['filter_product_status'];

            if(!empty($data['filter_reset'])){
                $ssFilter->getManager()->getStorage()->clear(__CLASS__ . $action);
            }
        }
    
        $this->goRoute();
    }
    
    public function indexAction() {
        $myForm	= new \Admin\Form\Search\Product($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-product'));
        
        if($this->getRequest()->isXmlHttpRequest()) {
            if(!empty($this->_params['data']['data'])) {
                $update = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->saveItem($this->_params['data']['data'], array('task' => 'update-product'));
            }
            
            return $this->response;
        }
        
        $this->_viewModel['myForm']	        = $myForm;
        $this->_viewModel['items']          = $items;
        $this->_viewModel['count']          = $this->getTable()->countItem($this->_params, array('task' => 'list-product'));
        $this->_viewModel['category']       = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listItem(null, array('task' => 'list-product')), array('key' => 'id', 'value' => 'name', 'level' => true, 'level_start' => 2));
        $this->_viewModel['product_unit']   = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'product-unit')), array('task' => 'cache')), array('key' => 'name', 'value' => 'name'));
        $this->_viewModel['product_status'] = array(1 => 'Còn hàng', 0 => 'Hết hàng');
        $this->_viewModel['box_new']        = array(1 => 'Có', 0 => 'Không');
        $this->_viewModel['box_best']       = array(1 => 'Có', 0 => 'Không');
        $this->_viewModel['box_highlight']  = array(1 => 'Có', 0 => 'Không');
        $this->_viewModel['box_hot']        = array(1 => 'Có', 0 => 'Không');
        $this->_viewModel['caption']        = 'Sản phẩm - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    public function formAction() {
        $ssFilter   = new Container(__CLASS__ . 'Form');
        $myForm     = $this->getForm();
        
        $task = 'add-item';
        $caption = 'Sản phẩm - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\Product(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Sản phẩm - Sửa';
            }
        }
        
        if($this->_params['route']['code'] == 'copy') {
            $caption = 'Sản phẩm - Copy';
            $task = 'add-item';
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            $images = $this->_params['data']['images'];
            $videos = $this->_params['data']['videos'];
            
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $this->_params['data']['images'] = $images;
                $this->_params['data']['videos'] = $videos;
                
                // Lưu lại category đã chọn
                $ssData['post_category_id'] = $this->_params['data']['post_category_id'];
                $ssFilter->data = $ssData;
                
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'form'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'form', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        $this->_viewModel['ssData']     = $ssFilter->data;
        return new ViewModel($this->_viewModel);
    }
    
    public function categoryAction() {
        $item = $this->getTable()->getItem(array('id' => $this->_params['route']['id']), null);
    
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = 'Loại sản phẩm - '. $item['name'];
        return new ViewModel($this->_viewModel);
    }
    
    public function formCategoryAction() {
        $myForm = new \Admin\Form\ProductCategory($this->getServiceLocator());
    
        if(!empty($this->_params['data']['id'])) {
            $item = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->getItem(array('id' => $this->_params['data']['id']));
            
            if(!empty($item['options'])) {
                $options = unserialize($item['options']);
                if(isset($this->_params['data']['code']) && $this->_params['data']['code'] != '') {
                    $category = $options[$this->_params['data']['code']];
                    $myForm->setData($category);
                }
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\ProductCategory($this->_params));
                $myForm->setData($this->_params['data']);
    
                if($myForm->isValid()){
                    $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['item'] = $item;
    
                    $result = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->saveItem($this->_params, array('task' => 'product-category'));
    
                    $this->flashMessenger()->addMessage('Thêm loại sản phẩm thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($this->_params['data']);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = 'Thêm loại sản phẩm';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    public function deleteCategoryAction() {
        if(!empty($this->_params['data']['id']) && $this->_params['data']['code'] != '') {
            $item = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $this->_params['item'] = $item;
                $result = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->saveItem($this->_params, array('task' => 'delete-product-category'));

                $this->flashMessenger()->addMessage('Thêm loại sản phẩm thành công');
                echo 'success';
                return $this->response;
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = 'Xóa loại sản phẩm';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    public function importAction() {
        $myForm			= new \Admin\Form\Import($this->getServiceLocator());
        $myForm->setInputFilter(new \Admin\Filter\Import($this->_params));
    
        $this->_viewModel['caption'] = 'Import sản phẩm';
        $this->_viewModel['myForm']	 = $myForm;
        $viewModel = new ViewModel($this->_viewModel);
    
        if($this->getRequest()->isXmlHttpRequest()) {
            if($this->getRequest()->isPost()){
                if(empty($this->_params['data']['post_category_id'])) {
                    echo 'Không tìm thấy danh mục';
                } else {
                    $item = $this->getTable()->getItem($this->_params['data'], array('task' => 'import'));
                    if(!empty($item)) {
                        $this->_params['item'] = $item;
                        $item = $this->getTable()->saveItem($this->_params, array('task' => 'import-update'));
                        echo 'Cập nhật';
                    } else {
                        $item = $this->getTable()->saveItem($this->_params, array('task' => 'import-insert'));
                        echo 'Thêm mới';
                    }
                }
                return $this->response;
            }
        } else {
            if($this->getRequest()->isPost()){
                $myForm->setData($this->_params['data']);
    
                if($myForm->isValid()){
                    if(!empty($this->_params['data']['file_import']['tmp_name'])){
                        $upload 		= new \ZendX\File\Upload();
                        $file_import	= $upload->uploadFile('file_import', PATH_FILES . '/import/', array('task' => 'rename', 'file_name' => 'import'));
                    }
                    $viewModel->setVariable('file_import', $file_import);
                    $viewModel->setVariable('import', true);
    
                    require_once PATH_VENDOR . '/Excel/PHPExcel/IOFactory.php';
                    $objPHPExcel = \PHPExcel_IOFactory::load(PATH_FILES . '/import/import.xlsx');
                    	
                    $sheetData = $objPHPExcel->getActiveSheet(1)->toArray(null, true, true, true);
                    $viewModel->setVariable('sheetData', $sheetData);
    
                    $post_category = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listItem(null, array('task' => 'list-all')), array('key' => 'index', 'value' => 'object'));
                    $viewModel->setVariable('post_category', $post_category);
                }
            }
        }
    
        return $viewModel;
    }
    
    public function exportAction() {
        $date               = new \ZendX\Functions\Date();
    
        $items              = $this->getTable()->listItem($this->_params, array('task' => 'list-product', 'paginator' => false));
        $post_category      = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listItem(null, array('task' => 'cache'));
    
        //Include PHPExcel
        require_once PATH_VENDOR . '/Excel/PHPExcel.php';
    
        // Config
        $config = array(
            'sheetData' => 0,
            'headRow' => 1,
            'startRow' => 2,
            'startColumn' => 0,
        );
    
        // Column
        $arrColumn = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ');
    
        // Data Export
        $arrData = array(
            array('field' => 'product_code', 'title' => 'Mã sản phẩm'),
            array('field' => 'name', 'title' => 'Tên sản phẩm'),
            array('field' => 'post_category_id', 'title' => 'ID Danh mục', 'type' => 'data_source', 'data_source' => $post_category, 'data_source_field' => 'index'),
            array('field' => 'product_price', 'title' => 'Giá'),
            array('field' => 'status', 'title' => 'Hiển thị'),
            array('field' => 'box_new', 'title' => 'Mới'),
            array('field' => 'box_highlight', 'title' => 'Nổi bật'),
            array('field' => 'box_hot', 'title' => 'Hot'),
            array('field' => 'box_best', 'title' => 'Bán chạy'),
            array('field' => 'product_status', 'title' => 'Tình trạng'),
            array('field' => 'product_price_show', 'title' => 'Hiện giá'),
            array('field' => 'product_unit', 'title' => 'Đơn vị'),
        );
    
        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();
    
        // Set document properties
        $objPHPExcel->getProperties()->setCreator('Admin')
                                     ->setTitle("Export");
    
        // Dữ liệu tiêu đề cột
        $startColumn = $config['startColumn'];
        foreach ($arrData AS $key => $data) {
            $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $config['headRow'], $data['title']);
            $objPHPExcel->getActiveSheet()->getStyle($arrColumn[$startColumn] . $config['headRow'])->getFont()->setBold(true);
            $startColumn++;
        }
    
        // Dữ liệu data
        $startRow = $config['startRow'];
        foreach ($items AS $item) {
            $startColumn = $config['startColumn'];
            foreach ($arrData AS $key => $data) {
                switch ($data['type']) {
                    case 'date':
                        $formatDate = $data['format'] ? $data['format'] : 'd/m/Y';
                        $value = $date->fomartToView($item[$data['field']], $formatDate);
                        break;
                    case 'data_source':
                        $field = $data['data_source_field'] ? $data['data_source_field'] : 'name';
                        $value = $data['data_source'][$item[$data['field']]][$field];
                        break;
                    default:
                        $value = $item[$data['field']];
                }
    
                $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $startRow, $value);
                $startColumn++;
            }
            $startRow++;
        }
    
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Export.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
    
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
    
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    
        return $this->response;
    }
}
