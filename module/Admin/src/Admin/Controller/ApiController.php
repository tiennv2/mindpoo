<?php
namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\Json\Json;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class ApiController extends ActionController {
    
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function selectAction() {
        $adapter        = $this->getServiceLocator()->get('dbConfig');
        $tableGateway   = new TableGateway($this->_params['data']['data-table'], $adapter, null);
        $table          = new \Admin\Model\ApiTable($tableGateway);
        
        $items = $table->listItem($this->_params, null);
        
        $results = array();
        
        if($items->count() > 0) {
            $data_id = $this->_params['data']['data-id'];
            $data_text = explode(',', $this->_params['data']['data-text']);
            
            $results[] = array('id' => '', 'text' => ' - Chọn - ');
            foreach ($items AS $item) {
                $text = '';
                for ($i = 0; $i < count($data_text) ; $i++) {
                    if($i == 0) {
                        $text .= $item[$data_text[$i]];
                    } else {
                        if(!empty($item[$data_text[$i]])) {
                            $text .= ' - ' . $item[$data_text[$i]];
                        }
                    }
                }
                $results[] = array(
                    'id' => $item[$data_id],
                    'text' => $text,
                );
            }
        }
        
        if(!empty($this->_params['route']['id'])) {
            $results = $results[1];
        }
        
        if(!empty($this->_params['data']['term'])) {
            $termResults = array();
            foreach ($results AS $result) {
                $pos = strpos(strtolower($result['text']), strtolower($this->_params['data']['term']));
                if($pos !== false) {
                    $termResults[] = $result;
                }
            }
            echo Json::encode($termResults);
        } else {
            echo Json::encode($results);
        }
        
        return $this->response;
    }
    
    public function dataAction() {
        $adapter        = $this->getServiceLocator()->get('dbConfig');
        $tableGateway   = new TableGateway($this->_params['data']['data-table'], $adapter, null);
        $table          = new \Admin\Model\ApiTable($tableGateway);
    
        $items          = $table->listItem($this->_params, null);
    
        if(!empty($this->_params['route']['id'])) {
            $results = $items->current();
        } else {
            foreach ($items AS $item) {
                $results[$item['id']] = $item;
            }
        }
    
        echo Json::encode($results);
    
        return $this->response;
    }
    
    public function countAction() {
        $adapter        = $this->getServiceLocator()->get('dbConfig');
        $tableGateway   = new TableGateway($this->_params['data']['data-table'], $adapter, null);
        $table          = new \Admin\Model\ApiTable($tableGateway);
    
        $results        = $table->countItem($this->_params, null);
    
        echo Json::encode($results);
    
        return $this->response;
    }
    
    public function languageAction() {
        $ssSystem = new Container('system');
        if($this->_params['route']['code'] != $ssSystem->language) {
            $ssSystem->language = $this->_params['route']['code'];
        }
        
        if(!$this->getRequest()->isXmlHttpRequest()) {
            $this->redirect()->toRoute('routeHome');
        }
        
        return $this->response;
    }
    
    public function sitemapAction() {
        $filename = PATH_APPLICATION . '/sitemap.xml';
        $arrValue = array();
    
        // Đường dẫn trang chủ
        $arrValue[] = array(
            'loc' => $this->url()->fromRoute('routeHome', array(), array('force_canonical' => true)),
            'lastmod' => '',
            'changefreq' => '',
            'priority' => '',
        );
         
        // Đường dẫn Danh mục
        $categories = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listItem(null, array('task' => 'cache'));
        foreach ($categories AS $key => $val) {
            $arrValue[] = array(
                'loc' => $this->url()->fromRoute('routePostCategory', array('alias' => $val['alias']), array('force_canonical' => true)),
                'lastmod' => '',
                'changefreq' => '',
                'priority' => '',
            );
        }
         
        // Đường dẫn bài viết
        $items = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->listItem(null, array('task' => 'list-all'));
        foreach ($items AS $key => $val) {
            $arrValue[] = array(
                'loc' => $this->url()->fromRoute('routePostItem', array('category_alias' => $val['category_alias'], 'alias' => $val['alias']), array('force_canonical' => true)),
                'lastmod' => '',
                'changefreq' => '',
                'priority' => '',
            );
        }
    
    
        $doc = new \DOMDocument();
    
        //Tạo
        $doc->version   = '1.0';
        $doc->encoding  = 'UTF-8';
    
        $note_goc   =   $doc->createElement( 'urlset' );
        $doc        ->  appendChild($note_goc);
        $note_goc   ->  appendChild($doc->createAttribute("xmlns"))
                    ->  appendChild($doc->createTextNode("http://www.sitemaps.org/schemas/sitemap/0.9"));
    
        foreach ( $arrValue AS $key => $val ) {
            $note_url = $doc->createElement( 'url' );
            $note_goc->appendChild($note_url);
             
            $loc        = $doc->createElement( 'loc', $val['loc'] );
            //$lastmod      = $doc->createElement( 'lastmod', $val['lastmod'] );
             
            $note_url->appendChild($loc);
            //$note_url->appendChild($lastmod);
        }
    
        $doc->formatOutput = true;
    
        $doc->saveXML();
        $doc->save($filename);
    
        $this->_viewModel['caption'] = 'Tạo sitemap';
        return new ViewModel($this->_viewModel);
    }

    public function rssAction() {
        $filename = PATH_APPLICATION . '/rss.xml';

        $arrValue = array();

        $user = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $post_category = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listItem(null, array('task' => 'cache'));

        // Sản phẩm
        $items = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->listItem(null, array('task' => 'list-all'));
        foreach ($items AS $key => $val) {
            $url = $this->url()->fromRoute('routePostItem', array('alias' => $val['alias']), array('force_canonical' => true));
            $arrValue[] = array(
                'title'         => str_replace('&', 'và', $val['name']),
                'link'          => $url,
                'pubDate'       => $val->created,
                'created_by'    => $user[$val->created_by]['fullname'] ?: $user[$val->created_by]['username'],
                'post_category' => $post_category[$val['post_category_id']]['name'],
                'description'   => trim($val['description']),
                'image_link'    => DOMAIN . $val['image'],
                'id'            => $val['index'],
                'content'       => $val['content'],
            );
        }

        $doc = new \DOMDocument();

        //Tạo
        $doc->version   = '1.0';
        $doc->encoding  = 'UTF-8';

        $note_goc = $doc->createElement( 'rss' );
        $doc -> appendChild($note_goc);
        $note_goc -> appendChild($doc->createAttribute("version")) -> appendChild($doc->createTextNode("2.0"));
        $note_goc -> appendChild($doc->createAttribute("xmlns:g")) -> appendChild($doc->createTextNode("http://base.google.com/ns/1.0"));
        $note_goc -> appendChild($doc->createAttribute("xmlns:content")) -> appendChild($doc->createTextNode("http://purl.org/rss/1.0/modules/content/"));
        $note_goc -> appendChild($doc->createAttribute("xmlns:wfw")) -> appendChild($doc->createTextNode("http://wellformedweb.org/CommentAPI/"));
        $note_goc -> appendChild($doc->createAttribute("xmlns:dc")) -> appendChild($doc->createTextNode("http://purl.org/dc/elements/1.1/"));
        $note_goc -> appendChild($doc->createAttribute("xmlns:atom")) -> appendChild($doc->createTextNode("http://www.w3.org/2005/Atom"));
        $note_goc -> appendChild($doc->createAttribute("xmlns:sy")) -> appendChild($doc->createTextNode("http://purl.org/rss/1.0/modules/syndication/"));
        $note_goc -> appendChild($doc->createAttribute("xmlns:slash")) -> appendChild($doc->createTextNode("http://purl.org/rss/1.0/modules/slash/"));

        $channel = $doc->createElement( 'channel' );
        $note_goc -> appendChild($channel);

        $title = $doc->createElement( 'title', 'Công Ty Giáo Dục MindPoo' );
        $channel -> appendChild($title);

        $atom = $doc->createElement( 'atom:link', DOMAIN );
        $channel -> appendChild($atom);
        $atom -> appendChild($doc->createAttribute("href")) -> appendChild($doc->createTextNode(DOMAIN));
        $atom -> appendChild($doc->createAttribute("rel")) -> appendChild($doc->createTextNode("self"));
        $atom -> appendChild($doc->createAttribute("type")) -> appendChild($doc->createTextNode("application/rss+xml"));

        $link = $doc->createElement( 'link', DOMAIN );
        $channel -> appendChild($link);

        $description = $doc->createElement( 'description', 'Công Ty Giáo Dục MindPoo.' );
        $channel -> appendChild($description);

        $lastBuildDate = $doc->createElement( 'lastBuildDate', date("l, d M Y H:i:s"));
        $channel -> appendChild($lastBuildDate);

        $language = $doc->createElement( 'language', 'vi');
        $channel -> appendChild($language);

        $sy_updatePeriod = $doc->createElement( 'sy:updatePeriod', 'hourly');
        $channel -> appendChild($sy_updatePeriod);

        $sy_updateFrequency = $doc->createElement( 'sy:updateFrequency', '1');
        $channel -> appendChild($sy_updateFrequency);

        foreach ( $arrValue AS $key => $val ) {
            $item = $doc->createElement( 'item' );
            $channel->appendChild($item);

            $item_title       = $doc->createElement( 'title', $val['title'] );
            $item_link        = $doc->createElement( 'link', $val['link'] );
            $item_pubDate     = $doc->createElement( 'pubDate', $val['pubDate'] );
            $item_creator     = $doc->createElement( 'dc:creator',  $val['created_by'] );
            $item_category    = $doc->createElement( 'category',  htmlspecialchars($val['post_category']) );
            $item_guid        = $doc->createElement( 'guid', $val['link'] );
            $item_description = $doc->createElement( 'description', htmlspecialchars($val['description']) );
            $item_content     = $doc->createElement( 'content:encoded', htmlspecialchars($val['content']) );
            $item_id          = $doc->createElement( 'g:id', $val['id'] );
            $item_image_link  = $doc->createElement( 'g:image_link', $val['image_link'] );

            $item->appendChild($item_title);
            $item->appendChild($item_link);
            $item->appendChild($item_pubDate);
            $item->appendChild($item_creator);
            $item->appendChild($item_category);
            $item->appendChild($item_guid);
            $item->appendChild($item_description);
            $item->appendChild($item_content);
            $item->appendChild($item_id);
            $item->appendChild($item_image_link);

            $item_guid -> appendChild($doc->createAttribute("isPermaLink")) -> appendChild($doc->createTextNode("false"));
        }

        $doc->formatOutput = true;

        $doc->saveXML();
        $doc->save($filename);

        $this->_viewModel['caption'] = 'Tạo Rss';
        return new ViewModel($this->_viewModel);
    }
}