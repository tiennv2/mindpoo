<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class FormDataController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\FormDataTable';
        $this->_options['formName'] = 'formAdminFormData';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']          = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'created';
        $this->_params['ssFilter']['order']             = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_status']     = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_keyword']    = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_form']       = !empty($ssFilter->filter_form) ? $ssFilter->filter_form : $this->_params['route']['id'];
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : 50;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
    
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option    = intval($data['pagination_option']);
    
            $ssFilter->order_by             = $data['order_by'];
            $ssFilter->order                = $data['order'];
    
            $ssFilter->filter_status        = $data['filter_status'];
            $ssFilter->filter_keyword       = $data['filter_keyword'];
            $ssFilter->filter_form          = $data['filter_form'];

            if(!empty($data['filter_reset'])){
                $ssFilter->getManager()->getStorage()->clear(__CLASS__ . $action);
            }
        }
    
        $this->goRoute();
    }
    
    public function indexAction() {
        $ssFilter	= new Container(__CLASS__);
        $aclInfo    = new \ZendX\System\UserInfo();
        $this->_params['groupInfo'] = $aclInfo->getGroupInfo();
        $this->_params['permissionInfo'] = $aclInfo->getPermission();
        
        // Lấy danh sách các form.
        $listForm = $this->getServiceLocator()->get('Admin\Model\FormTable')->listItem($this->_params, array('task' => 'list-all'));
        
        if(!empty($this->_params['route']['id'])) {
            $currentForm = $this->getServiceLocator()->get('Admin\Model\FormTable')->getItem(array('id' => $this->_params['route']['id']));
            $ssFilter->filter_form = $currentForm['id'];
        }
        
        if(empty($ssFilter->filter_form)) {
            $currentForm = $listForm->current();
            $ssFilter->filter_form = $currentForm['id'];
        }
        
        $this->_params['ssFilter']['filter_form'] = $ssFilter->filter_form;
        
        $currentForm = $this->getServiceLocator()->get('Admin\Model\FormTable')->getItem(array('id' => $ssFilter->filter_form));
        if(empty($currentForm)) {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'no-access'));
        }
        
        $myForm	= new \Admin\Form\Search\FormData($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['myForm']	        = $myForm;
        $this->_viewModel['items']          = $items;
        $this->_viewModel['count']          = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['user']           = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['location_city']  = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "location-city" )), array('task' => 'cache'));

        $this->_viewModel['currentForm']    = $currentForm;
        $this->_viewModel['caption']        = 'Đăng ký - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    public function formAction() {
        $myForm			= $this->getForm();
        
        $task = 'add-item';
        $caption = 'Đăng ký - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\FormData(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Đăng ký - Sửa';
            }
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'form'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'form', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
}
