<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class IeltsController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\PostItemTable';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']        = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'created';
        $this->_params['ssFilter']['order']           = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_keyword']  = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_status']   = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_category'] = $ssFilter->filter_category;
        $this->_params['ssFilter']['filter_type']     = $ssFilter->filter_type;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage']  = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge( $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray() );
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
    
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option    = intval($data['pagination_option']);
    
            $ssFilter->order_by = $data['order_by'];
            $ssFilter->order    = $data['order'];
    
            $ssFilter->filter_keyword  = $data['filter_keyword'];
            $ssFilter->filter_status   = $data['filter_status'];
            $ssFilter->filter_category = $data['filter_category'];
            $ssFilter->filter_type     = $data['filter_type'];

            if(!empty($data['filter_reset'])){
                $ssFilter->getManager()->getStorage()->clear(__CLASS__ . $action);
            }
        }
    
        $this->goRoute();
    }
    
    public function indexAction() {
        $myForm	= new \Admin\Form\Search\Ielts($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $this->_viewModel['myForm'] = $myForm;
        $this->_viewModel['items']  = $this->getTable()->listItem($this->_params, array('task' => 'list-ielts'));
        $this->_viewModel['count']  = $this->getTable()->countItem($this->_params, array('task' => 'list-ielts'));
        $this->_viewModel['user']   = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['type']   = json_decode(TYPE_CATE, true);
        $this->_viewModel['caption']    = 'Ielts - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    public function formAction() {
        $ssFilter = new Container(__CLASS__ . 'Form');
        $myForm = new \Admin\Form\Ielts($this->getServiceLocator());
        
        $task = 'add-item';
        $caption = 'Ielts - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\Ielts(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Ielts - Sửa';
            }
        }
        
        if($this->_params['route']['code'] == 'copy') {
            $caption = 'Ielts - Copy';
            $task = 'add-item';
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            $myForm->setInputFilter(new \Admin\Filter\Ielts());
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                
                // Lưu lại category đã chọn
                $ssData['category_id'] = $this->_params['data']['post_category_id'];
                $ssFilter->data = $ssData;
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'form'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'form', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']  = $myForm;
        $this->_viewModel['item']    = $item;
        $this->_viewModel['caption'] = $caption;
        $this->_viewModel['ssData']  = $ssFilter->data;
        return new ViewModel($this->_viewModel);
    }
}
