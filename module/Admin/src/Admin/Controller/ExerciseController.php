<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class ExerciseController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\ExerciseTable';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']       = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'created';
        $this->_params['ssFilter']['order']          = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_keyword'] = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_status']  = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_post']    = $ssFilter->filter_post;

        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage']  = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge( $this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray() );
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
            $ssFilter->pagination_option = intval($data['pagination_option']);
            $ssFilter->order_by          = $data['order_by'];
            $ssFilter->order             = $data['order'];
            $ssFilter->filter_keyword    = $data['filter_keyword'];
            $ssFilter->filter_status     = $data['filter_status'];

            if(!empty($data['filter_reset'])){
                $ssFilter->getManager()->getStorage()->clear(__CLASS__ . $action);
            }
        }
        $this->goRoute();
    }
    
    public function indexAction() {
        $myForm	= new \Admin\Form\Search\PostItem($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        $ssFilter = new Container(__CLASS__);
        if($this->_params['route']['id']){
            $ssFilter->filter_post_test = $this->_params['route']['id'];
        }
        $post_id = $ssFilter->filter_post_test;
        $post_info = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->getItem(['id' => $post_id]);
        $this->_params['ssFilter']['filter_post'] = $post_id;
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['myForm']    = $myForm;
        $this->_viewModel['items']     = $items;
        $this->_viewModel['post_info'] = $post_info;
        $this->_viewModel['count']     = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['caption']   = $post_info['name'] .' - Danh sách bài tập';
        return new ViewModel($this->_viewModel);
    }
    
    public function formAction() {
        $ssFilter = new Container(__CLASS__);
        $this->_params['data']['post_id'] = $ssFilter->filter_post_test;
        $myForm = new \Admin\Form\Exercise($this->getServiceLocator());
        
        $task = 'add-item';
        $caption = 'Luyện thi - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\Exercise(array('data' => $this->_params['data'])));
                $option_item = $item['options'] ? unserialize($item['options']) : [];
                $myForm->setData(array_merge(json_decode(json_encode($item), true), $option_item));
                $task = 'edit-item';
                $caption = 'Luyện thi - Sửa';
                $this->_params['data']['item'] = $item;
            }
        }
        
        if($this->_params['route']['code'] == 'copy') {
            $caption = 'Luyện thi - Copy';
            $task = 'add-item';
        }
        $myForm->setData($this->_params['data']);
        if($this->getRequest()->isPost()){
            $controlAction = $this->_params['data']['control-action'];
            $myForm->setInputFilter(new \Admin\Filter\Exercise(array('data' => $this->_params['data'])));
            $this->_params['data']['questions'] = is_array($this->_params['data']['questions']) ? array_values($this->_params['data']['questions']) : null;
            $this->_params['data']['samples']   = is_array($this->_params['data']['samples']) ? array_values($this->_params['data']['samples']) : null;
            $this->_params['data']['arrange_sentence'] = is_array($this->_params['data']['arrange_sentence']) ? array_values($this->_params['data']['arrange_sentence']) : null;
            if($myForm->isValid()){
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
        
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'form'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'form', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']  = $myForm;
        $this->_viewModel['item']    = $item;
        $this->_viewModel['caption'] = $caption;
        $this->_viewModel['ssData']  = $ssFilter->data;
        return new ViewModel($this->_viewModel);
    }
}
