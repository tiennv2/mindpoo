<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;

class MenuTable extends NestedTable {
	
	protected $tableGateway;
	protected $userInfo;
	
	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway	= $tableGateway;
		$this->userInfo	= new \ZendX\System\UserInfo();
	}
	
	public function itemInSelectbox($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem  = New Container('system');
	            
	            $select -> order(array('left' => 'ASC'))
	                    -> where->greaterThan('level', 0);
	            $select -> where -> NEST
                	             -> equalTo('language', $ssSystem->language)
                	             -> or
                	             -> equalTo('language', '*')
                	             -> UNNEST;
	        })->toArray();
	    }
	    
	    if($options['task'] == 'list-level') {
	        $result = $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem  = New Container('system');
	            
	            $node = $arrParam['node'];
	            
	            $select -> columns(array('id', 'level'))
	                    -> order(array('level' => 'DESC'))
	                    -> limit(1)
	                    -> where->greaterThanOrEqualTo('left', $node['left'])
	                    -> where->lessThanOrEqualTo('right', $node['right']);
	            $select -> where -> NEST
                	             -> equalTo('language', $ssSystem->language)
                	             -> or
                	             -> equalTo('language', '*')
                	             -> UNNEST;
	        })->current();
	    }
	    
	    if($options['task'] == 'form-category') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem  = New Container('system');
	            
	            $select -> order(array('left' => 'ASC'));
	            $select -> where -> NEST
                	             -> equalTo('language', $ssSystem->language)
                	             -> or
                	             -> equalTo('language', '*')
                	             -> UNNEST;
	            
	            if(!empty($arrParam['node'])) {
	                $node = $arrParam['node'];
    	            $select-> where -> greaterThanOrEqualTo('left', $node['left'])
    	                            -> lessThanOrEqualTo('right', $node['right']);
	            }
	        })->toArray();
	    }
	    
	    if($options['task'] == 'form-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem  = New Container('system');
	            
	            $select    -> order(array('left' => 'ASC'))
	                       -> where->greaterThan('level', 0);
	            $select -> where -> NEST
                	             -> equalTo('language', $ssSystem->language)
                	             -> or
                	             -> equalTo('language', '*')
                	             -> UNNEST;
	        })->toArray();
	    }
	
	    return $result;
	}
	
	public function countItem($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem  = New Container('system');
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	            $select -> where->greaterThan('level', 0);
	            $select -> where -> equalTo('language', $ssSystem->language);
	            
	        })->current();
	    }
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem  = New Container('system');
	            $node      = $arrParam['node'];
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	            $select -> where->greaterThan('level', 0)
                        -> where->greaterThanOrEqualTo('left', $node['left'])
	                    -> where->lessThanOrEqualTo('right', $node['right']);
	            
	            if($node['code'] != 'Menu.Admin') {
                    $select -> where -> NEST
                    	             -> equalTo(TABLE_MENU .'.language', $ssSystem->language)
                    	             -> or
                    	             -> equalTo(TABLE_MENU .'.language', '*')
                    	             -> UNNEST;
                }
	            
	        })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem = New Container('system');
	            
	            $select -> order(array('left' => 'ASC'))
	                    -> where->greaterThan('level', 0);
	            $select -> where -> equalTo('language', $ssSystem->language);
	        })->toArray();
	    }
	    
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $ssFilter   = $arrParam['ssFilter'];
                $ssSystem   = New Container('system');
                $paginator  = $arrParam['paginator'];
                $node       = $arrParam['node'];
                
                $select -> join(TABLE_POST_CATEGORY, TABLE_POST_CATEGORY .'.id = '. TABLE_MENU .'.param_id', array('category_name' => 'name', 'category_alias' => 'alias'), 'left')
                        -> limit($paginator['itemCountPerPage'])
				        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage'])
                        -> order(TABLE_MENU .'.left ASC')
	                    -> where->greaterThan(TABLE_MENU .'.level', 0)
	                    -> where->greaterThan(TABLE_MENU .'.left', $node['left'])
	                    -> where->lessThanOrEqualTo(TABLE_MENU .'.right', $node['right']);
                
                if($node['code'] != 'Menu.Admin') {
                    $select -> where -> NEST
                    	             -> equalTo(TABLE_MENU .'.language', $ssSystem->language)
                    	             -> or
                    	             -> equalTo(TABLE_MENU .'.language', '*')
                    	             -> UNNEST;
                }
			});
		}
		
		if($options['task'] == 'list-branch') {
		    $result = $this->listNodes($arrParam, array('task' => 'list-branch'))->toArray();
		}
		
		if($options['task'] == 'list-edit') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $ssSystem  = New Container('system');
		        
		        $select   -> order(array('left' => 'ASC'))
		                  -> where->greaterThan('level', 0)
		                          ->NEST
		                          ->lessThan('left', $arrParam['left'])
		                          ->or
		                          ->greaterThan('left', $arrParam['right'])
		                          ->UNNEST
		                          ->lessThanOrEqualTo('level', 1);
		        $select -> where -> equalTo(TABLE_MENU .'.language', $ssSystem->language);
		    })->toArray();
		}
		
		if($options['task'] == 'list-by-code') {
		    $nodeInfo = $this->getItem(array('code' => $arrParam['code']), array('task' => 'code'));
		
		    $result	= $this->tableGateway->select(function (Select $select) use ($nodeInfo){
		        $ssSystem  = New Container('system');
		        
		        $select -> join(TABLE_POST_CATEGORY, TABLE_POST_CATEGORY .'.id = '. TABLE_MENU .'.param_id', array('category_name' => 'name', 'category_alias' => 'alias'), 'left')
		                -> order(TABLE_MENU .'.left ASC')
        		        -> where -> equalTo(TABLE_MENU .'.status', 1)
        		                 -> greaterThan(TABLE_MENU .'.level', 0)
        		                 -> between(TABLE_MENU .'.left', $nodeInfo->left, $nodeInfo->right);
		        
		        if($nodeInfo->code != 'Menu.Admin') {
		            $select -> where -> NEST
                		             -> equalTo(TABLE_MENU .'.language', $ssSystem->language)
                		             -> or
                		             -> equalTo(TABLE_MENU .'.language', '*')
                		             -> UNNEST;
		        }
		    })->toArray();
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
					$select->where->equalTo('id', $arrParam['id']);
			})->current();
		}
		
		if($options['task'] == 'code') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select->where->equalTo('code', $arrParam['code']);
		    })->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    $ssSystem = New Container('system');
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier();
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
		    $id = $gid->getId();
			$data	= array(
				'id'           => $id,
				'name'         => $arrData['name'],
				'param_id'     => $arrData['param_id'],
				'link'         => $arrData['link'],
				'target'       => $arrData['target'],
				'class'        => $arrData['class'],
				'code'         => $arrData['code'],
				'icon'         => $arrData['icon'],
				'description'  => $filter->filter($arrData['description']),
				'content'      => $filter->filter($arrData['content']),
				'image'        => $image->getFull(),
				'image_medium' => $image->getMedium(),
				'image_thumb'  => $image->getThumb(),
				'created'      => date('Y-m-d H:i:s'),
				'created_by'   => $this->userInfo->getUserInfo('id'),
				'language'     => $ssSystem->language
			);
			
			$this->insertNode($data, $arrRoute['reference'], array('position' => $arrRoute['type']));
			return $id;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
				'name'         => $arrData['name'],
				'param_id'     => $arrData['param_id'],
				'link'         => $arrData['link'],
				'target'       => $arrData['target'],
				'class'        => $arrData['class'],
				'code'         => $arrData['code'],
				'icon'         => $arrData['icon'],
				'description'  => $filter->filter($arrData['description']),
				'content'      => $filter->filter($arrData['content']),
				'image'        => $image->getFull(),
				'image_medium' => $image->getMedium(),
				'image_thumb'  => $image->getThumb(),
			);
			
			$this->updateNode($data, $id, null);
			return $id;
		}
		
		if($options['task'] == 'move-item') {
		    $data	= array(
		        'name'              => $arrParam['name'],
		        'parent'            => $arrParam['parent'],
		    );
		    	
		    if($arrParam['parent'] == $arrParam['id']) {
		        $arrParam['parent'] = null;
		    }
		    $this->updateNode($data, $arrParam['id'], $arrParam['parent']);
		    return $arrParam['id'];
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        foreach ($arrParam['cid'] AS $id) {
	            $this->removeNode($id, array('type' => 'only'));
	        }
	        
	        return count($arrParam['cid']);
	    }
	
	    return false;
	}
	
	public function moveItem($arrParam = null, $options = null){
	    if($options == null) {
	        if(!empty($arrParam['move-id'])) {
	            if($arrParam['move-type'] == 'up') {
                    $this->moveUp($arrParam['move-id']);
	            } elseif ($arrParam['move-type'] == 'down') {
	                $this->moveDown($arrParam['move-id']);
	            }
	            return true;
	        }
	    }
	
	    return false;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        if(!empty($arrParam['cid'])) {
    	        $data	= array( 'status'	=> ($arrParam['status'] == 1) ? 0 : 1 );
    			$this->tableGateway->update($data, array("id IN('". implode(',', $arrParam['cid']) ."')"));
	        }
	        return true;
	    }
	    
	    return false;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    $result = 0;
	    
	    if($options['task'] == 'change-ordering') {
            foreach ($arrParam['cid'] AS $id) {
                $data	= array( 'ordering'	=> $arrParam['ordering'][$id] );
                $where  = array('id' => $id);
                $this->tableGateway->update($data, $where);
            }
            
            return count($arrParam['cid']);
	    }
	    return false;
	}
}