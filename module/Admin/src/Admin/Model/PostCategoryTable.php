<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;
use Zend\Session\Container;

class PostCategoryTable extends NestedTable {
	
	protected $tableGateway;
	protected $userInfo;
	
	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway	= $tableGateway;
		$this->userInfo	= new \ZendX\System\UserInfo();
	}
	
	public function itemInSelectbox($arrParam = null, $options = null){
	    if($options['task'] == 'list-level') {
	        $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem  = New Container('system');
	            
	            $select -> columns(array('id', 'level'))
	                    -> order(array('level' => 'DESC'))
	                    -> limit(1);
	            $select -> where -> NEST
	                             -> equalTo('language', $ssSystem->language)
	                             -> or
	                             -> equalTo('language', '*')
	                             -> UNNEST;
	        })->current();
	        
	        $result = array();
	        if(!empty($items)) {
	            for ($i = 1; $i <= $items->level; $i++) {
	                $result[$i] = 'Level ' . $i;
	            }
	        }
	    }
	    
	    if($options['task'] == 'form-category') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem  = New Container('system');
	            
	            $select -> order(array('left' => 'ASC'));
	            $select -> where -> NEST
	                             -> equalTo('language', $ssSystem->language)
	                             -> or
	                             -> equalTo('language', '*')
	                             -> UNNEST;
	        })->toArray();
	    }
	    
	    if($options['task'] == 'form-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem  = New Container('system');
	            
	            $select -> order(array('left' => 'ASC'))
	                    -> where->greaterThan('level', 0);
	            $select -> where -> NEST
	                             -> equalTo('language', $ssSystem->language)
	                             -> or
	                             -> equalTo('language', '*')
	                             -> UNNEST;
	        })->toArray();
	    }
	
	    return $result;
	}
	
	public function countItem($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssSystem  = New Container('system');
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	            $select -> where -> equalTo('language', $ssSystem->language);
	        })->current();
	    }
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];
	            $ssSystem  = New Container('system');
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	            $select -> where -> greaterThan('level', 0);
	            $select -> where -> equalTo('language', $ssSystem->language);
	            
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select->where->equalTo('status', $ssFilter['filter_status']);
	            }
	            
	            if(isset($ssFilter['filter_level']) && $ssFilter['filter_level'] != '') {
	                $select->where->lessThanOrEqualTo('level', $ssFilter['filter_level']);
	            }
	            
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
				}
	        })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
			    
                $select -> limit($paginator['itemCountPerPage'])
				        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage'])
                        -> order('left ASC')
	                    -> where->greaterThan('level', 0);
                $select -> where -> equalTo('language', $ssSystem->language);
				
				if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
				    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
				}
				
				if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
				    $select->where->equalTo('status', $ssFilter['filter_status']);
				}
				
				if(isset($ssFilter['filter_level']) && $ssFilter['filter_level'] != '') {
				    $select->where->lessThanOrEqualTo('level', $ssFilter['filter_level']);
				}
				
			    if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
				}
				
			});
		}
		
		if($options['task'] == 'list-all') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		    	$arrData = $arrParam['data'];
		        $ssSystem  = New Container('system');
		        
		        $select -> order('left ASC')
        		        -> where->greaterThan('level', 0);

        		if($arrData['type']){
        			$select -> where -> equalTo('type', $arrData['type']);
        		}
		        $select -> where -> equalTo('language', $ssSystem->language);
		    });
		}
		
		if($options['task'] == 'list-post') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $ssSystem  = New Container('system');
		        $select -> order('left ASC')
        		        -> where -> greaterThan('level', 0)
		                         -> notEqualTo('type', 'product');
		        $select -> where -> equalTo('language', $ssSystem->language);
		    });
		}
		
		if($options['task'] == 'list-product') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $ssSystem  = New Container('system');
		        
		        $select -> order('left ASC')
        		        -> where -> greaterThan('level', 1)
		                         -> equalTo('type', 'product');
		        $select -> where -> equalTo('language', $ssSystem->language);
		    });
		}
		
		if($options['task'] == 'cache') {
		    $cache = $this->getServiceLocator()->get('cache');
		    $cache_key = 'PostCategory';
		    $result = $cache->getItem($cache_key);
		    
		    if (empty($result)) {
		        $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		            $select -> order('left ASC')
		                    -> where -> greaterThan('level', 0);
		        });
	            $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	             
	            $cache->setItem($cache_key, $result);
		    }
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
					$select->where->equalTo('id', $arrParam['id']);
			})->current();
		}
	
		if($options['task'] == 'alias') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
					$select->where->equalTo('alias', $arrParam['alias']);
			})->current();
		}

		if($options['task'] == 'get-by-index') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('index', $arrParam['index']);
    		})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    $ssSystem = New Container('system');
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier();
	    $alias    = new \ZendX\Filter\CreateAlias();
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'                => $id,
				'name'              => $arrData['name'],
				'alias'             => !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
			    'description'       => $arrData['description'],
			    'content'           => $arrData['content'],
				'image'             => $image->getFull(),
				'image_medium'      => $image->getMedium(),
			    'image_thumb'       => $image->getThumb(),
				'created'           => date('Y-m-d H:i:s'),
				'created_by'        => $this->userInfo->getUserInfo('id'),
				'meta_url'          => $arrData['meta_url'],
				'meta_title'        => $arrData['meta_title'],
				'meta_keywords'     => $arrData['meta_keywords'],
				'meta_description'  => $arrData['meta_description'],
				'status'            => $arrData['status'],
				'type'              => $arrData['type'],
				'layout'            => $arrData['layout'],
				'box_hot'           => $arrData['box_hot'],
				'box_highlight'     => $arrData['box_highlight'],
				'box_home'          => $arrData['box_home'],
				'box_footer'        => $arrData['box_footer'],
				'box_left'          => $arrData['box_left'],
				'box_right'         => $arrData['box_right'],
				'parent'            => $arrData['parent'],
			    'language'          => $ssSystem->language
			);
			
			$this->insertNode($data, $arrData['parent'], array('position' => 'right'));
			return $id;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
				'name'              => $arrData['name'],
				'alias'             => !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
			    'description'       => $arrData['description'],
			    'content'           => $arrData['content'],
			    'image'             => $image->getFull(),
			    'image_medium'      => $image->getMedium(),
			    'image_thumb'       => $image->getThumb(),
			    'meta_url'          => $arrData['meta_url'],
			    'meta_title'        => $arrData['meta_title'],
			    'meta_keywords'     => $arrData['meta_keywords'],
			    'meta_description'  => $arrData['meta_description'],
				'status'            => $arrData['status'],
			    'type'              => $arrData['type'],
			    'layout'            => $arrData['layout'],
			    'box_hot'           => $arrData['box_hot'],
			    'box_highlight'     => $arrData['box_highlight'],
			    'box_home'          => $arrData['box_home'],
			    'box_footer'        => $arrData['box_footer'],
			    'box_left'          => $arrData['box_left'],
			    'box_right'         => $arrData['box_right'],
				'parent'            => $arrData['parent'],
			);
			
			if($arrParam['parent'] == $arrData['id']) {
			    $arrParam['parent'] = null;
			}
			$this->updateNode($data, $id, $arrData['parent']);
			return $id;
		}
		
		if($options['task'] == 'update-view') {
		    $id = $arrData['id'];
		    $data    = array( 'view' => new Expression('(`view` + ?)', array(1)) );
    	    $where   = new Where();
    	    $where->equalTo('id', $id);
    	    $this->tableGateway->update($data, $where);
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        foreach ($arrParam['cid'] AS $id) {
	            $this->removeNode($id, array('type' => 'only'));
	        }
	        
	        return count($arrParam['cid']);
	    }
	
	    return false;
	}
	
	public function moveItem($arrParam = null, $options = null){
	    if($options == null) {
	        if(!empty($arrParam['move-id'])) {
	            if($arrParam['move-type'] == 'up') {
                    $this->moveUp($arrParam['move-id']);
	            } elseif($arrParam['move-type'] == 'down') {
	                $this->moveDown($arrParam['move-id']);
	            }
	            return true;
	        }
	    }
	
	    return false;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        if(!empty($arrParam['cid'])) {
    	        $data	= array( 'status'	=> ($arrParam['status'] == 1) ? 0 : 1 );
    	        $where = new Where();
    	        $where->in('id', $arrParam['cid']);
    			$this->tableGateway->update($data, $where);
	        }
	        return true;
	    }
	    
	    return false;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    $result = 0;
	    
	    if($options['task'] == 'change-ordering') {
            foreach ($arrParam['cid'] AS $id) {
                $data	= array( 'ordering'	=> $arrParam['ordering'][$id] );
                $where  = array('id' => $id);
                $this->tableGateway->update($data, $where);
            }
            
            return count($arrParam['cid']);
	    }
	    return false;
	}
	
	public function refresh($arrParam = null, $options = null){
	    
	    if($options['task'] == 'list') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $select -> order(array('left' => 'ASC'));
	        });
	        
            return $result;
	    }
	    
        if($options['task'] == 'update') {
            $arrData = $arrParam['data'];
            
		    $id      = $arrData['id'];
		    $data    = array();
		    
		    if(isset($arrData['level'])) {
		        $data['level'] = $arrData['level'];
		    }
		    if(isset($arrData['left'])) {
		        $data['left'] = $arrData['left'];
		    }
		    if(isset($arrData['right'])) {
		        $data['right'] = $arrData['right'];
		    }
		    
		    if(!empty($data)) {
        	    $where   = new Where();
        	    $where->equalTo('id', $id);
        	    $this->tableGateway->update($data, $where);
		    }
		    return $id;
		}
	}
}