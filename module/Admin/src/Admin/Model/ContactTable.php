<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class ContactTable extends DefaultTable {
	
	public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select -> where -> equalTo('status', $ssFilter['filter_status']);
	            }
	            
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select -> where -> NEST
                			         -> like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> OR
                			         -> like('phone', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> OR
                			         -> like('email', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> OR
                			         -> like('address', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> UNNEST;
				}
            })->current();
    	    
    	    return $result->count;
	    }
	}
	
	public function listItem($arrParam = null, $options = null){
	    
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
			    
                $select -> limit($paginator['itemCountPerPage'])
				        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
				
				if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
				    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
				}
				
			    if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select -> where -> equalTo('status', $ssFilter['filter_status']);
	            }
	            
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select -> where -> NEST
                			         -> like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> OR
                			         -> like('phone', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> OR
                			         -> like('email', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> OR
                			         -> like('address', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> UNNEST;
				}
				
			});
			
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$select -> where -> equalTo('id', $arrParam['id']);
			})->current();
		}
	
		if($options['task'] == 'login') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$select -> where -> NEST
				                 -> equalTo('phone', $arrParam['username'])
				                 -> OR
				                 -> equalTo('email', $arrParam['username'])
				                 -> UNNEST;
				$select -> where -> equalTo('password', md5($arrParam['password']));
				$select -> where -> equalTo('status', 1);
			})->current();
		}
	
		if($options['task'] == 'by-phone') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$select -> where -> equalTo('phone', $arrParam['phone']);
			})->current();
		}
	
		if($options['task'] == 'by-email') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$select -> where -> equalTo('email', $arrParam['email']);
			})->current();
		}
	
		if($options['task'] == 'forgot-password') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$select -> where -> equalTo('id', $arrParam['id'])
				                 -> equalTo('password_code', $arrParam['password_code']);
			})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	     
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier();
	    $gid      = new \ZendX\Functions\Gid();
	    $date     = new \ZendX\Functions\Date();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'        => $id,
				'name'      => $arrData['name'],
				'phone'     => $arrData['phone'],
				'email'     => $arrData['email'],
				'address'   => $arrData['address'],
				'password'  => !empty($arrData['password']) ? md5($arrData['password']) : md5('12345678'),
				'sex'       => $arrData['sex'],
				'birthday'  => $arrData['birthday'],
				'content'   => $arrData['content'],
				'company'   => $arrData['company'],
				'status'    => $arrData['status'] ?: 1,
				'created'   => date('Y-m-d H:i:s'),
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array();
			
			if(!empty($arrData['name'])) {
			    $data['name'] = $arrData['name'];
			}
			if(!empty($arrData['phone'])) {
			    $data['phone'] = $arrData['phone'];
			}
			if(!empty($arrData['email'])) {
			    $data['email'] = $arrData['email'];
			}
			if(!empty($arrData['address'])) {
			    $data['address'] = $arrData['address'];
			}
			if(!empty($arrData['sex'])) {
			    $data['sex'] = $arrData['sex'];
			}
			if(!empty($arrData['birthday'])) {
			    $data['birthday'] = $arrData['birthday'];
			}
			if(!empty($arrData['content'])) {
			    $data['content'] = $arrData['content'];
			}
			if(!empty($arrData['company'])) {
			    $data['company'] = $arrData['company'];
			}
			if(isset($arrData['status'])) {
			    $data['status'] = $arrData['status'];
			}
			if(!empty($arrData['password'])) {
			    $data['password'] = md5($arrData['password']);
			}
			
			if(!empty($data)) {
			    $this->tableGateway->update($data, array('id' => $arrData['id']));
			}
			return $arrData['id'];
		}
		
		// Cập nhật password_code
		if($options['task'] == 'update-password_code') {
		    $id = $arrData['id'];
		    $password_code = $gid->random(6);
		    $data = array(
		        'password_code' => $password_code,
		    );
		
		    $this->tableGateway->update($data, array('id' => $id));
		    return $password_code;
		}
		
		// Đổi mật khẩu
		if($options['task'] == 'change-password') {
		    $id = $arrData['id'];
		    $data = array(
				'password'      => md5($arrData['password']),
				'password_code' => null,
		    );
		
		    $this->tableGateway->update($data, array('id' => $id));
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'delete-item') {
	        $items = $this->listItem($arrData, array('task' => 'list-cid'));
	        
	        $where = new Where();
	        $where->in('id', $arrData['cid']);
	        $this->tableGateway->delete($where);
	        
	        return count($arrData['cid']);
	    }
	
	    return false;
	}
	
    public function changeStatus($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'change-status') {
	        if(!empty($arrData['cid'])) {
    	        $data	= array( 'status'	=> ($arrData['status'] == 1) ? 0 : 1 );
    			$this->tableGateway->update($data, array("id IN('". implode("','", $arrData['cid']) ."')"));
	        }
	        return true;
	    }
	    
	    return false;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'change-ordering') {
            foreach ($arrData['cid'] AS $id) {
                $data	= array( 'ordering'	=> $arrData['ordering'][$id] );
                $where  = array('id' => $id);
                $this->tableGateway->update($data, $where);
            }
            
            return count($arrData['cid']);
	    }
	    return false;
	}
}