<?php
namespace Admin\Model;

use Zend\Session\Container;
use Zend\Db\Sql\Select;
use ZendX\Functions\Date;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

class PostTestTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                
                $select -> join( TABLE_POST_CATEGORY, TABLE_POST_TEST . '.category_id = '. TABLE_POST_CATEGORY .'.id', array(), 'left' );
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> where -> notEqualTo(TABLE_POST_CATEGORY .'.type', 'product');
                 
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select->where->equalTo(TABLE_POST_TEST .'.status', $ssFilter['filter_status']);
                }
                
                if(!empty($ssFilter['filter_category'])) {
                    $categories = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listNodes(array('id' => $ssFilter['filter_category']), array('task' => 'list-branch'));
                    $ids = array();
                    foreach ($categories AS $category) {
                        $ids[] = $category['id'];
                    }
                    if(count($ids) > 0) {
                        $select->where->in(TABLE_POST_TEST .'.category_id', $ids);
                    }
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> like(TABLE_POST_TEST .'.name', '%'. trim($ssFilter['filter_keyword']) . '%');
    			}
            })->current();
	    }
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                $paginator = $arrParam['paginator'];
                
    			$select -> join( TABLE_POST_CATEGORY, TABLE_POST_TEST . '.category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias', 'category_type' => 'type'), $select::JOIN_LEFT )
    			        -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
    			$select -> where -> notEqualTo(TABLE_POST_CATEGORY .'.type', 'product');
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select -> where -> equalTo(TABLE_POST_TEST .'.status', $ssFilter['filter_status']);
    			}
    			
    			if(!empty($ssFilter['filter_category'])) {
    			    $categorys = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listNodes(array('id' => $ssFilter['filter_category']), array('task' => 'list-branch'));
    			    $ids = array();
    			    foreach ($categorys AS $category) {
    			        $ids[] = $category['id'];
    			    }
    			    if(count($ids) > 0) {
    			        $select -> where -> in(TABLE_POST_TEST .'.category_id', $ids);
    			    }
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> like(TABLE_POST_TEST .'.name', '%'. trim($ssFilter['filter_keyword']) . '%');
    			}
    		});
		}
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
		
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    $ssSystem = New Container('system');
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $number   = new \ZendX\Functions\Number();
	    $gid      = new \ZendX\Functions\Gid();
	    $filter   = new \ZendX\Filter\Purifier();
	    $alias    = new \ZendX\Filter\CreateAlias();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'               => $id,
				'name'             => $arrData['name'],
				'alias'            => !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
				'description'      => $arrData['description'],
				'content'          => $arrData['content'],
				'image'            => $image->getFull(),
				'image_medium'     => $image->getMedium(),
				'image_thumb'      => $image->getThumb(),
				'category_id'      => $arrData['category_id'],
				'ordering'         => $arrData['ordering'],
				'status'           => $arrData['status'],
				'created'          => date('Y-m-d H:i:s'),
				'created_by'       => $this->userInfo->getUserInfo('id'),
				'meta_url'         => $arrData['meta_url'],
				'meta_title'       => $arrData['meta_title'],
				'meta_keywords'    => $arrData['meta_keywords'],
				'meta_description' => $arrData['meta_description'],
				'layout'           => $arrData['layout'],
				'type'             => $arrData['type'],
				'box_hot'          => $arrData['box_hot'],
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
		    $arr_field = ['name', 'alias', 'description', 'content', 'image', 'category_id', 'ordering', 'status', 'meta_url', 'meta_title', 'meta_keywords', 'meta_description', 'layout', 'type', 'box_hot'];
			$data = [];
		    foreach ($arrData as $key => $value) {
		    	if(in_array($key, $arr_field)){
		    		if($key == 'image'){
						$data['image']        = $image->getFull();
						$data['image_medium'] = $image->getMedium();
						$data['image_thumb']  = $image->getThumb();
		    		}else{
		    			$data[$key] = $value;
		    		}
		    	}
		    }
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}

		if($options['task'] == 'update-view') {
		    $id 		= $arrData['id'];
		    $data    	= array( 'view' => new Expression('(`view` + ?)', array(1)) );
    	    $where   	= new Where();
    	    $where->equalTo('id', $id);
    	    $this->tableGateway->update($data, $where);
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}