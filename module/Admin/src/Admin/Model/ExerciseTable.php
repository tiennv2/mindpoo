<?php
namespace Admin\Model;

use Zend\Session\Container;
use Zend\Db\Sql\Select;
use ZendX\Functions\Date;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

class ExerciseTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                 
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select->where->equalTo('status', $ssFilter['filter_status']);
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> like('name', '%'. trim($ssFilter['filter_keyword']) . '%');
    			}
    			if(!empty($ssFilter['filter_post'])){
    				$select -> where -> equalTo('post_id', $ssFilter['filter_post']);
    			}
            })->current();
	    }
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                $paginator = $arrParam['paginator'];
                
    			$select -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select -> where -> equalTo('status', $ssFilter['filter_status']);
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> like('name', '%'. trim($ssFilter['filter_keyword']) . '%');
    			}
    			if(!empty($ssFilter['filter_post'])){
    				$select -> where -> equalTo('post_id', $ssFilter['filter_post']);
    			}
    		});
		}
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrItem  = $arrParam['item'];
	    $arrRoute = $arrParam['route'];
	    $ssSystem = New Container('system');
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $number   = new \ZendX\Functions\Number();
	    $gid      = new \ZendX\Functions\Gid();
	    $filter   = new \ZendX\Filter\Purifier();
	    $alias    = new \ZendX\Filter\CreateAlias();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data_options = [];
			foreach ($arrData as $key => $value) {
				if(in_array($key, ['note_audio', 'min_word', 'max_word'])){
					$data_options[$key] = $value;
				}
			}
			$data	= array(
				'id'               => $id,
				'name'             => $arrData['name'],
				'alias'            => !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
				'content'          => $arrData['content'],
				'post_id'          => $arrData['post_id'],
				'audio'            => $arrData['audio'],
				'time_limit'       => $arrData['time_limit'],
				'type'             => $arrData['type'],
				'content_question' => $arrData['content_question'],
				'questions'        => is_array($arrData['questions']) ? serialize($arrData['questions']) : $arrData['questions'],
				'content_result'   => $arrData['content_result'],
				'samples'          => is_array($arrData['samples']) ? serialize($arrData['samples']) : $arrData['samples'],
				'arrange_sentence' => is_array($arrData['arrange_sentence']) ? serialize($arrData['arrange_sentence']) : $arrData['arrange_sentence'],
				'ordering'         => $arrData['ordering'],
				'status'           => $arrData['status'],
				'created'          => date('Y-m-d H:i:s'),
				'created_by'       => $this->userInfo->getUserInfo('id'),
				'meta_url'         => $arrData['meta_url'],
				'meta_title'       => $arrData['meta_title'],
				'meta_keywords'    => $arrData['meta_keywords'],
				'meta_description' => $arrData['meta_description'],
				'options'          => serialize($data_options),
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrItem['id'] ?: $arrData['id'];
		    $data_options = $arrItem['options'] ? unserialize($arrItem['options']) : [];
		    $arr_field = ['name', 'alias', 'content', 'audio', 'time_limit', 'type', 'content_question', 'questions', 'content_result', 'samples', 'ordering', 'status', 'meta_url', 'meta_title', 'meta_keywords', 'meta_description'];
			$data = [];
		    foreach ($arrData as $key => $value) {
		    	if(in_array($key, $arr_field)){
		    		if(in_array($key, ['questions', 'samples', 'arrange_sentence'])){
						$data[$key] = is_array($value) ? serialize($value) : $value;
		    		}else{
		    			$data[$key] = $value;
		    		}
		    	}elseif(in_array($key, ['note_audio', 'min_word', 'max_word'])){
		    		$data_options[$key] = $value;
		    	}
		    }
			$data['options'] = serialize($data_options);

			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}

		if($options['task'] == 'update-view') {
		    $id 		= $arrData['id'];
		    $data    	= array( 'view' => new Expression('(`view` + ?)', array(1)) );
    	    $where   	= new Where();
    	    $where->equalTo('id', $id);
    	    $this->tableGateway->update($data, $where);
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}