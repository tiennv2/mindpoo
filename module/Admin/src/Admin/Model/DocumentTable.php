<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Where;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Expression;
use Zend\Session\Container;

class DocumentTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $configs   = $arrParam['configs'];
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> where -> equalTo('language', $ssSystem->language);
                
                $select -> where->equalTo('code', $configs['code']);
                 
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select->where->equalTo('status', $ssFilter['filter_status']);
                }
                
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('description', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('url', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('content', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
            })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
			    $configs   = $arrParam['configs'];
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                
                $select -> columns( array( '*', 
                                'date_begin' => new Expression("DATE_FORMAT(date_begin, '%d/%m/%Y')"),
                                'date_end' => new Expression("DATE_FORMAT(date_end, '%d/%m/%Y')"),
                            ))
    			        -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage'])
    			        -> where->equalTo('code', $configs['code']);
                $select -> where -> equalTo('language', $ssSystem->language);
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select->where->equalTo('status', $ssFilter['filter_status']);
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('description', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('url', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('content', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
    		});
		}
		
	    if($options['task'] == 'list-all') {
            $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $ssSystem  = New Container('system');
                
                $select -> where -> equalTo('language', $ssSystem->language);
                
                if(!empty($arrParam['order'])) {
                    $select->order($arrParam['order']);
                } else {
                    $select->order(array('ordering' => 'ASC', 'created' => 'ASC'));
                }
                
                if(!empty($arrParam['where'])) {
                    foreach ($arrParam['where'] AS $key => $value) {
                        if(!empty($value)) {
                            $select->where->equalTo($key, $value);
                        }
                    }
                }
            });
	    }
		
	    if($options['task'] == 'cache') {
	        $ssSystem  = New Container('system');
	        
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = $arrParam['table'] .'_'. $arrParam['where']['code'] .'_'. $ssSystem->language;
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam, $ssSystem){
	                $select -> where -> equalTo('language', $ssSystem->language);
	                
	                if(!empty($arrParam['order'])) {
                        $select->order($arrParam['order']);
	                }
	                if(!empty($arrParam['where'])) {
	                    foreach ($arrParam['where'] AS $key => $value) {
	                        if(!empty($value)) {
                                $select->where->equalTo($key, $value);
	                        }
	                    }
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> columns( array( '*',
            			        'date_begin' => new Expression("DATE_FORMAT(date_begin, '%d/%m/%Y')"),
            			        'date_end' => new Expression("DATE_FORMAT(date_end, '%d/%m/%Y')"),
            			    ))
                        ->where->equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrConfigs    = $arrParam['configs'];
	    $arrData       = $arrParam['data'];
	    $arrRoute      = $arrParam['route'];
	    $ssSystem      = New Container('system');
	    
	    $filter        = new \ZendX\Filter\Purifier();
	    $image         = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $date          = new \ZendX\Functions\Date();
	    $number        = new \ZendX\Functions\Number();
	    $gid           = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
		    // Lưu các giá trị cố định
			$id = $gid->getId();
			$data	= array(
				'id'            => $id,
				'code'          => $arrConfigs['code'],
				'created'       => date('Y-m-d H:i:s'),
				'created_by'    => $this->userInfo->getUserInfo('id'),
			    'language'      => $ssSystem->language
			);
			
			// Lấy các field được setting
			foreach ($arrConfigs['form']['fields'] AS $filed) {
			    switch ($filed['options']['to_data']) {
			        case 'image':
			            $valueData = $image->getFull();
			            $data[$filed['name']. '_medium'] = $image->getMedium();
			            $data[$filed['name']. '_thumb'] = $image->getThumb();
			            break;
			        case 'date':
			            $valueData = $date->formatToData($arrData[$filed['name']]);
			            break;
			        case 'datetime':
			            $valueData = $date->formatToData($arrData[$filed['name']], 'Y-m-d H:i:s');
			            break;
			        case 'integer':
			            $valueData = $number->formatToData($arrData[$filed['name']]);
			            break;
			        default:
			            $valueData = $arrData[$filed['name']];
			    }
			    
			    $data[$filed['name']] = $valueData ? $valueData : null;
			}
			
			$this->tableGateway->insert($data);
			return $id;
		}
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array();
			
			// Lấy các field được setting
			foreach ($arrConfigs['form']['fields'] AS $filed) {
			    switch ($filed['options']['to_data']) {
			        case 'image':
			            $valueData = $image->getFull();
			            $data[$filed['name']. '_medium'] = $image->getMedium();
			            $data[$filed['name']. '_thumb'] = $image->getThumb();
			            break;
			        case 'date':
			            $valueData = $date->formatToData($arrData[$filed['name']]);
			            break;
			        case 'datetime':
			            $valueData = $date->formatToData($arrData[$filed['name']], 'Y-m-d H:i:s');
			            break;
			        case 'integer':
			            $valueData = $number->formatToData($arrData[$filed['name']]);
			            break;
			        default:
			            $valueData = $arrData[$filed['name']];
			    }
			    
			    $data[$filed['name']] = $valueData ? $valueData : null;
			}
			
			if(!empty($data)) {
			    $this->tableGateway->update($data, array('id' => $id));
			}
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $arrData  = $arrParam['data'];
    	    $arrRoute = $arrParam['route'];
    	    
            $where = new Where();
            $where->in('id', $arrData['cid']);
            $this->tableGateway->delete($where);
            
            $result = count($arrData['cid']);
	    }
	    
	    if($options['task'] == 'code') {
            $result = $this->countItem(array('configs' => array('code' => $arrParam['code'])), array('task' => 'list-item'));
            
            $where = new Where();
            $where->equalTo('code', $arrParam['code']);
            $this->tableGateway->delete($where);
            
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $arrData  = $arrParam['data'];
    	    $arrRoute = $arrParam['route'];
    	    
    	    $result = false;
            if(!empty($arrData['cid'])) {
    	        $data	= array( 'status'	=> ($arrData['status'] == 1) ? 0 : 1 );
    			$this->tableGateway->update($data, array("id IN('". implode("','", $arrData['cid']) ."')"));
                $result = true;
            }
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $arrData  = $arrParam['data'];
    	    $arrRoute = $arrParam['route'];
    	    
            foreach ($arrData['cid'] AS $id) {
                $data	= array('ordering'	=> $arrData['ordering'][$id]);
                $where  = array('id' => $id);
                $this->tableGateway->update($data, $where);
            }
            
            $result = count($arrData['cid']);
	    }
	    return $result;
	}
}