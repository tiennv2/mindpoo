<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;

class ProductCartTable extends DefaultTable {
	
	protected $tableGateway;
	protected $userInfo;
	
	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway	= $tableGateway;
		$this->userInfo	= new \ZendX\System\UserInfo();
	}
	
	
	public function countItem($arrParam = null, $options = null){
	   if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
			  
                $select -> join(TABLE_CONTACT, TABLE_CONTACT .'.id='. TABLE_PRODUCT_CART .'.contact_id', array(), 'inner');
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                
			    if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                                     -> like(TABLE_CONTACT .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> OR
                                     -> like(TABLE_CONTACT .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> OR
                                     -> like(TABLE_CONTACT .'.email', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> UNNEST;
                }
				
			})->current();
		}
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
			    
                $select -> join(TABLE_CONTACT, TABLE_CONTACT .'.id='. TABLE_PRODUCT_CART .'.contact_id', array('name', 'birthday', 'email', 'phone', 'sex', 'address', 'company', 'school'), 'inner');
                $select -> limit($paginator['itemCountPerPage'])
				        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage'])
                        -> order(TABLE_PRODUCT_CART .'.created DESC');
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                                     -> like(TABLE_CONTACT .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> OR
                                     -> like(TABLE_CONTACT .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> OR
                                     -> like(TABLE_CONTACT .'.email', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> UNNEST;
                }
				
			})->toArray();
		}
		
		if($options['task'] == 'by-contact') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                //$select -> join(TABLE_CONTACT, TABLE_CONTACT .'.id='. TABLE_PRODUCT_CART .'.contact_id', array('name', 'birthday', 'email', 'phone', 'sex', 'address', 'company', 'school'), 'inner');
                $select -> order(TABLE_PRODUCT_CART .'.created DESC');
                $select -> where -> equalTo(TABLE_PRODUCT_CART .'.contact_id', $arrParam['contact_id']);
			});
		}
		
		return $result;
	}
	
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    $ssFilter = new Container('myCart');
	    $productCart = $ssFilter->data;
	    
	    $filter   = new \ZendX\Filter\Purifier(array( array('HTML.AllowedElements', '') ));
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'                => $id,
				'contact_id'        => $arrData['contact_id'],
				'product'           => serialize($productCart),
				'note'              => !empty($arrData['note']) ? $filter->filter(trim($arrData['note'])) : null,
				'bank'              => !empty($arrData['bank']) ? $filter->filter(trim($arrData['bank'])) : null,
				'discount'          => !empty($arrData['discount']) ? $filter->filter(trim($arrData['discount'])) : null,
			    'created'           => date('Y-m-d H:i:s'),
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	    
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	    
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}