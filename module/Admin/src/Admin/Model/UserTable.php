<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Where;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {
	
    protected $tableGateway;
	protected $userInfo;
	protected $serviceLocator;
	
	public function __construct(TableGateway $tableGateway) {
	    $this->tableGateway	= $tableGateway;
	    $this->userInfo	= new \ZendX\System\UserInfo();
	}
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
	    $this->serviceLocator = $serviceLocator;
	}
	
	public function getServiceLocator() {
	    return $this->serviceLocator;
	}
	
	public function countItem($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select->where->equalTo('status', $ssFilter['filter_status']);
	            }
	            
	            if(isset($ssFilter['filter_user_group']) && $ssFilter['filter_user_group'] != '') {
				    $select->where->literal('FIND_IN_SET(\''. $ssFilter['filter_user_group'] .'\', user_group_id)');
				}
	            
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select->where->NEST
                			      ->like('fullname', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('username', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('phone', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('email', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->UNNEST;
				}
            })->current();
    	    
    	    return $result->count;
	    }
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	            $select -> where -> notEqualTo('id', '0000000000-0000-0000-0000-111111111111');
	            
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select->where->equalTo('status', $ssFilter['filter_status']);
	            }
	            
	            if(isset($ssFilter['filter_user_group']) && $ssFilter['filter_user_group'] != '') {
				    $select->where->literal('FIND_IN_SET(\''. $ssFilter['filter_user_group'] .'\', user_group_id)');
				}
	            
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select->where->NEST
                			      ->like('fullname', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('username', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('phone', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('email', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->UNNEST;
				}
	        })->current();
    	    
	        return $result->count;
	    }
	}
	
	public function listItem($arrParam = null, $options = null){
	    
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'User';
	        $result = $cache->getItem($cache_key);
	        
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select-> order('fullname ASC');
	            });
	            $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	            
	            $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    if($options['task'] == 'list-all') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $select -> where -> notEqualTo('id', '0000000000-0000-0000-0000-111111111111');
	        })->toArray();
	    }
	    
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
			    
                $select -> limit($paginator['itemCountPerPage'])
				        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage'])
                        -> where -> notEqualTo('id', '0000000000-0000-0000-0000-111111111111');
				
				if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
				    $select -> order(array(TABLE_USER .'.'. $ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
				}
				
				if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
				    $select->where->equalTo(TABLE_USER .'.status', $ssFilter['filter_status']);
				}
				
				if(isset($ssFilter['filter_user_group']) && $ssFilter['filter_user_group'] != '') {
				    $select->where->literal('FIND_IN_SET(\''. $ssFilter['filter_user_group'] .'\', user_group_id)');
				}
				
			    if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select->where->NEST
                			      ->like(TABLE_USER .'.fullname', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_USER .'.username', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_USER .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_USER .'.email', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->UNNEST;
				}
				
			});
			
		}
		
		if($options['task'] == 'list-cid') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select -> where -> in('id', $arrParam['cid'])
		                         -> notEqualTo('id', '0000000000-0000-0000-0000-111111111111');
		    })->toArray();
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
					$select->where->equalTo('id', $arrParam['id']);
			})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	     
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier();
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'                    => $id,
				'username'              => $arrData['username'],
				'email'                 => $arrData['email'],
				'fullname'              => $arrData['fullname'],
				'phone'                 => $arrData['phone'],
				'password'              => md5($arrData['password']),
				'ordering'              => $arrData['ordering'],
				'status'                => $arrData['status'],
			    'active_code'           => $arrData['active_code'],
			    'password_status'       => $arrData['password_status'],
				'created'               => date('Y-m-d H:i:s'),
				'created_by'            => $this->userInfo->getUserInfo('id'),
				'user_group_id'         => implode(',', $arrData['user_group_id']),
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
				'username'              => $arrData['username'],
				'email'                 => $arrData['email'],
				'fullname'              => $arrData['fullname'],
			    'phone'                 => $arrData['phone'],
				'ordering'              => $arrData['ordering'],
				'status'                => $arrData['status'],
			    'active_code'           => $arrData['active_code'],
			    'password_status'       => $arrData['password_status'],
			    'user_group_id'         => implode(',', $arrData['user_group_id']),
			);
			
			if(!empty($arrData['password'])) {
			    $data['password'] = md5($arrData['password']);
			}
			
			$this->tableGateway->update($data, array('id' => $arrData['id']));
			return $arrData['id'];
		}
		
		if($options['task'] == 'change-password') {
		    $data	= array(
		        'password'        => md5($arrData['password_new']),
		    );
		    	
		    $this->tableGateway->update($data, array('id' => $this->userInfo->getUserInfo('id')));
		    return $this->userInfo->getUserInfo('id');
		}
		
		if($options['task'] == 'update-password') {
		    $data	= array(
		        'password'        => md5($arrData['password_new']),
		        'password_status' => 0,
		    );
		    	
		    $this->tableGateway->update($data, array('id' => $this->userInfo->getUserInfo('id')));
		    return $this->userInfo->getUserInfo('id');
		}
		
		if($options['task'] == 'update-login') {
		    $data	= array(
		        'login_ip'        => $_SERVER['REMOTE_ADDR'],
		        'login_time'      => date('Y-m-d H:i:s'),
		    );
		    	
		    $this->tableGateway->update($data, array('id' => $id));
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'delete-item') {
	        $items = $this->listItem($arrData, array('task' => 'list-cid'));
	        
	        $where = new Where();
	        $where->in('id', $arrData['cid']);
	        $where->notEqualTo('id', '0000000000-0000-0000-0000-111111111111');
	        $this->tableGateway->delete($where);
	        
	        return count($arrData['cid']);
	    }
	
	    return false;
	}
	
    public function changeStatus($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'change-status') {
	        if(!empty($arrData['cid'])) {
    	        $data	= array( 'status'	=> ($arrData['status'] == 1) ? 0 : 1 );
    			$this->tableGateway->update($data, array("id IN('". implode("','", $arrData['cid']) ."')"));
	        }
	        return true;
	    }
	    
	    return false;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'change-ordering') {
            foreach ($arrData['cid'] AS $id) {
                $data	= array( 'ordering'	=> $arrData['ordering'][$id] );
                $where  = array('id' => $id);
                $this->tableGateway->update($data, $where);
            }
            
            return count($arrData['cid']);
	    }
	    return false;
	}
}