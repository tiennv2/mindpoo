<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;

class CourseGroupTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                
                $select->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                
                $select->where->equalTo('course_item_id', $ssFilter['filter_course']);
                
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select->where->equalTo('status', $ssFilter['filter_status']);
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
            })->current();
	    
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
                
    			$select -> join(TABLE_COURSE_ITEM, TABLE_COURSE_ITEM .'.id = '. TABLE_COURSE_GROUP .'.course_item_id', array('course_name' => 'name'), 'inner')
    			        -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
    			
    			$select->where->equalTo('course_item_id', $ssFilter['filter_course']);
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select -> where -> equalTo('status', $ssFilter['filter_status']);
	            }
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
    			
    		});
		}
		
		if($options['task'] == 'list-all') {
			$items	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
    			$select -> where -> equalTo('course_item_id', $arrParam['course_item_id']);
    			$select -> order(array('ordering' => 'ASC', 'name' => 'ASC', 'created' => 'ASC'));
    		});
			
		    $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
		}

		if($options['task'] == 'list-course-group') {
			$items	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
    			$select -> where -> equalTo('course_item_id', $arrParam['course_id']);
    			$select -> order(array('ordering' => 'ASC', 'created' => 'ASC'));
    		});
			
		    $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
		}
		
		if($options['task'] == 'public-course') {
			$items	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
    			$select -> where -> equalTo('course_item_id', $arrParam['course_item_id'])
    			                 -> equalTo('status', 1);
    			$select -> order(array('ordering' => 'ASC', 'name' => 'ASC', 'created' => 'ASC'));
    		});
			
		    $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
		}

		if ($options['task'] == 'course-page') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $select -> where -> equalTo('course_item_id', $arrParam['course_item_id']);
	            $select -> order(array('ordering' => 'ASC'));
	        })->toArray();
	    }
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'CourseGroup' . $arrParam['data']['course_item_id'];
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select->order(array('ordering' => 'ASC', 'name' => 'ASC'))
	                       ->where->equalTo('status', 1);
	                
	                if(!empty($arrParam['data']['course_item_id'])) {
	                    $select->where->equalTo('course_item_id', $arrParam['data']['course_item_id']);
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData   = $arrParam['data'];
	    $arrRoute  = $arrParam['route'];
	    $arrCourse = $arrParam['course'];
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier();
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'                    => $id,
				'name'                  => $arrData['name'],
				'ordering'              => $arrData['ordering'],
				'status'                => $arrData['status'],
				'course_item_id'        => $arrCourse['id'],
				'created'               => date('Y-m-d H:i:s'),
				'created_by'            => $this->userInfo->getUserInfo('id'),
				'modified'              => date('Y-m-d H:i:s'),
				'modified_by'           => $this->userInfo->getUserInfo('id'),
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
				'name'          => $arrData['name'],
				'ordering'      => $arrData['ordering'],
				'status'        => $arrData['status'],
				'modified'      => date('Y-m-d H:i:s'),
				'modified_by'   => $this->userInfo->getUserInfo('id'),
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}