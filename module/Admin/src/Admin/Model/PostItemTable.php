<?php
namespace Admin\Model;

use Zend\Session\Container;
use Zend\Db\Sql\Select;
use ZendX\Functions\Date;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

class PostItemTable extends DefaultTable {
    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                
                $select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array(), 'left' );
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
                $select -> where -> notEqualTo(TABLE_POST_CATEGORY .'.type', 'product');
                 
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select->where->equalTo(TABLE_POST_ITEM .'.status', $ssFilter['filter_status']);
                }
                
                if(!empty($ssFilter['filter_category'])) {
                    $categories = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listNodes(array('id' => $ssFilter['filter_category']), array('task' => 'list-branch'));
                    $ids = array();
                    foreach ($categories AS $category) {
                        $ids[] = $category['id'];
                    }
                    if(count($ids) > 0) {
                        $select->where->in(TABLE_POST_ITEM .'.post_category_id', $ids);
                    }
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> like(TABLE_POST_ITEM .'.name', '%'. trim($ssFilter['filter_keyword']) . '%');
    			}
            })->current();
	    }
	    if($options['task'] == 'list-product') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                
                $select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array(), 'left' );
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
                $select -> where -> equalTo(TABLE_POST_CATEGORY .'.type', 'product');
                 
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select -> where -> equalTo(TABLE_POST_ITEM .'.status', $ssFilter['filter_status']);
                }
                 
                if(isset($ssFilter['filter_product_status']) && $ssFilter['filter_product_status'] != '') {
                    $select -> where -> equalTo(TABLE_POST_ITEM .'.product_status', $ssFilter['filter_product_status']);
                }
                
                if(!empty($ssFilter['filter_category'])) {
                    $categories = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listNodes(array('id' => $ssFilter['filter_category']), array('task' => 'list-branch'));
                    $ids = array();
                    foreach ($categories AS $category) {
                        $ids[] = $category['id'];
                    }
                    if(count($ids) > 0) {
                        $select -> where -> in(TABLE_POST_ITEM .'.post_category_id', $ids);
                    }
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> NEST
    		                         -> like(TABLE_POST_ITEM .'.name', '%'. trim($ssFilter['filter_keyword']) . '%')
    		                         -> OR
    		                         -> like(TABLE_POST_ITEM .'.product_code', '%'. trim($ssFilter['filter_keyword']) . '%')
    		                         -> UNNEST;
    			}
            })->current();
	    }

	    if($options['task'] == 'list-ielts') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                
                $select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array(), 'left' );
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
                $select -> where -> equalTo(TABLE_POST_CATEGORY .'.type', 'ielts');
                 
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select -> where -> equalTo(TABLE_POST_ITEM .'.status', $ssFilter['filter_status']);
                }

                if(isset($ssFilter['filter_type']) && $ssFilter['filter_type'] != '') {
                    $select -> where -> equalTo(TABLE_POST_ITEM .'.type', $ssFilter['filter_type']);
                }
                
                if(!empty($ssFilter['filter_category'])) {
                    $categories = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listNodes(array('id' => $ssFilter['filter_category']), array('task' => 'list-branch'));
                    $ids = array();
                    foreach ($categories AS $category) {
                        $ids[] = $category['id'];
                    }
                    if(count($ids) > 0) {
                        $select -> where -> in(TABLE_POST_ITEM .'.post_category_id', $ids);
                    }
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> NEST
    		                         -> like(TABLE_POST_ITEM .'.name', '%'. trim($ssFilter['filter_keyword']) . '%')
    		                         -> UNNEST;
    			}
            })->current();
	    }

        if($options['task'] == 'list-teacher') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                
                $select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array(), 'left' );
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
                $select -> where -> equalTo(TABLE_POST_CATEGORY .'.type', 'teacher');
                 
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select -> where -> equalTo(TABLE_POST_ITEM .'.status', $ssFilter['filter_status']);
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                                     -> like(TABLE_POST_ITEM .'.name', '%'. trim($ssFilter['filter_keyword']) . '%')
                                     -> UNNEST;
                }
            })->current();
        }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                $paginator = $arrParam['paginator'];
                
    			$select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias', 'category_type' => 'type'), $select::JOIN_LEFT )
    			        -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
    			$select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
    			$select -> where -> notEqualTo(TABLE_POST_CATEGORY .'.type', 'product');
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select -> where -> equalTo(TABLE_POST_ITEM .'.status', $ssFilter['filter_status']);
    			}
    			
    			if(!empty($ssFilter['filter_category'])) {
    			    $categorys = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listNodes(array('id' => $ssFilter['filter_category']), array('task' => 'list-branch'));
    			    $ids = array();
    			    foreach ($categorys AS $category) {
    			        $ids[] = $category['id'];
    			    }
    			    if(count($ids) > 0) {
    			        $select -> where -> in(TABLE_POST_ITEM .'.post_category_id', $ids);
    			    }
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> like(TABLE_POST_ITEM .'.name', '%'. trim($ssFilter['filter_keyword']) . '%');
				}
				//var_dump($select->getSqlString());die();
    			
    		});
		}
		
		if($options['task'] == 'list-product') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                $paginator = $arrParam['paginator'];
                
                if(!isset($options['paginator']) || $options['paginator'] != false) {
                    $select -> limit($paginator['itemCountPerPage'])
                            -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
                
    			$select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias', 'category_type' => 'type'), $select::JOIN_LEFT );
    			$select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
    			$select -> where -> equalTo(TABLE_POST_CATEGORY .'.type', 'product');
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select -> where -> equalTo(TABLE_POST_ITEM .'.status', $ssFilter['filter_status']);
    			}
    			
    			if(isset($ssFilter['filter_product_status']) && $ssFilter['filter_product_status'] != '') {
    			    $select -> where -> equalTo(TABLE_POST_ITEM .'.product_status', $ssFilter['filter_product_status']);
    			}
    			
			    if(!empty($ssFilter['filter_category'])) {
                    $categories = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listNodes(array('id' => $ssFilter['filter_category']), array('task' => 'list-branch'));
                    $ids = array();
                    foreach ($categories AS $category) {
                        $ids[] = $category['id'];
                    }
                    if(count($ids) > 0) {
                        $select -> where -> in(TABLE_POST_ITEM .'.post_category_id', $ids);
                    }
                }
                
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> NEST
    		                         -> like(TABLE_POST_ITEM .'.name', '%'. trim($ssFilter['filter_keyword']) . '%')
    		                         -> OR
    		                         -> like(TABLE_POST_ITEM .'.product_code', '%'. trim($ssFilter['filter_keyword']) . '%')
    		                         -> UNNEST;
    			}
    		});
		}

		if($options['task'] == 'list-ielts') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                $paginator = $arrParam['paginator'];
                
                if(!isset($options['paginator']) || $options['paginator'] != false) {
                    $select -> limit($paginator['itemCountPerPage'])
                            -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
                
    			$select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias', 'category_type' => 'type'), $select::JOIN_LEFT );
    			$select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
    			$select -> where -> equalTo(TABLE_POST_CATEGORY .'.type', 'ielts');
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select -> where -> equalTo(TABLE_POST_ITEM .'.status', $ssFilter['filter_status']);
    			}

                if(isset($ssFilter['filter_type']) && $ssFilter['filter_type'] != '') {
                    $select -> where -> equalTo(TABLE_POST_ITEM .'.type', $ssFilter['filter_type']);
                }
    			
			    if(!empty($ssFilter['filter_category'])) {
                    $categories = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listNodes(array('id' => $ssFilter['filter_category']), array('task' => 'list-branch'));
                    $ids = array();
                    foreach ($categories AS $category) {
                        $ids[] = $category['id'];
                    }
                    if(count($ids) > 0) {
                        $select -> where -> in(TABLE_POST_ITEM .'.post_category_id', $ids);
                    }
                }
                
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> NEST
    		                         -> like(TABLE_POST_ITEM .'.name', '%'. trim($ssFilter['filter_keyword']) . '%')
    		                         -> UNNEST;
    			}
    		});
		}

        if($options['task'] == 'list-teacher') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                $paginator = $arrParam['paginator'];
                
                if(!isset($options['paginator']) || $options['paginator'] != false) {
                    $select -> limit($paginator['itemCountPerPage'])
                            -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
                
                $select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias', 'category_type' => 'type'), $select::JOIN_LEFT );
                $select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
                $select -> where -> equalTo(TABLE_POST_CATEGORY .'.type', 'teacher');
                
                if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
                    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
                }
                
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select -> where -> equalTo(TABLE_POST_ITEM .'.status', $ssFilter['filter_status']);
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                                     -> like(TABLE_POST_ITEM .'.name', '%'. trim($ssFilter['filter_keyword']) . '%')
                                     -> UNNEST;
                }
            });
        }
		
		if($options['task'] == 'list-all') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		
		        $select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias', 'category_type' => 'type'), $select::JOIN_LEFT );
		        $select -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);
		    });
		}
		
		if($options['task'] == 'cache-product') {
		    $cache = $this->getServiceLocator()->get('cache');
		    $cache_key = 'AdminProduct';
		    $result = $cache->getItem($cache_key);
		    
		    if (empty($result)) {
		        $items	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		            $select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias', 'category_type' => 'type'), $select::JOIN_LEFT );
		            $select -> where -> equalTo(TABLE_POST_CATEGORY .'.type', 'product');
		        });
	            $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	            $cache->setItem($cache_key, $result);
		    }
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
		
		if($options['task'] == 'import') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select -> where -> equalTo('name', $arrParam['name'])
                                 -> equalTo('post_category_id', $arrParam['post_category_id']);
    		})->current();
		}

		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
        $arrItem  = $arrParam['item'];
	    $arrRoute = $arrParam['route'];
	    $ssSystem = New Container('system');
	    
        $image       = new \ZendX\Functions\Thumbnail($arrData['image']);
        $image_popup = new \ZendX\Functions\Thumbnail($arrData['image_popup']);
        $number      = new \ZendX\Functions\Number();
        $gid         = new \ZendX\Functions\Gid();
        $filter      = new \ZendX\Filter\Purifier();
		$alias       = new \ZendX\Filter\CreateAlias();
		
	    // Xử lý thêm nhiều hình ảnh
	    $images = [];
	    if(!empty($arrData['images'])) {
	        for ($i = 0; $i < count($arrData['images']['url']); $i++) {
	            $image_multi          = new \ZendX\Functions\Thumbnail($arrData['images']['url'][$i]);
	    
	            $images['url'][]      = $image_multi->getFull();
	            $images['medium'][]   = $image_multi->getMedium();
	            $images['thumb'][]    = $image_multi->getThumb();
	            $images['name'][]     = $arrData['images']['name'][$i];
	        }
	    }
		

	    // Xử lý thêm nhiều video
	    $videos = '';
	    if(!empty($arrData['videos'])) {
	        for ($i = 0; $i < count($arrData['videos']['id']); $i++) {
	            $image_multi          = new \ZendX\Functions\Thumbnail($arrData['videos']['image'][$i]);
	    
	            $videos['image'][]    = $image_multi->getFull() ? $image_multi->getFull() : $arrData['videos']['image'][$i];
	            $videos['medium'][]   = $image_multi->getMedium() ? $image_multi->getMedium() : $arrData['videos']['image'][$i];
	            $videos['thumb'][]    = $image_multi->getThumb() ? $image_multi->getThumb() : $arrData['videos']['image'][$i];
	            $videos['name'][]     = $arrData['videos']['name'][$i];
	            $videos['id'][]       = $arrData['videos']['id'][$i];
	        }
		}
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
            $data_options = [];
            if($image_popup->getFull()){
                $data_options['image_popup'] = $image_popup->getFull();
            }
			$data	= array(
                'id'               => $id,
                'name'             => $arrData['name'],
                'alias'            => !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
                'status'           => $arrData['status'],
                'ordering'         => $arrData['ordering'],
                'layout'           => $arrData['layout'],
                'description'      => $arrData['description'],
                'content'          => str_replace("&nbsp;", " ", $arrData['content']),
                'image'            => $image->getFull(),
                'image_medium'     => $image->getMedium(),
                'image_thumb'      => $image->getThumb(),
                'images'           => $images ? serialize($images) : '',
                'videos'           => $videos ? serialize($videos) : '',
                'created'          => date('Y-m-d H:i:s'),
                'created_by'       => $this->userInfo->getUserInfo('id'),
                'meta_url'         => $arrData['meta_url'],
                'meta_title'       => $arrData['meta_title'],
                'meta_keywords'    => $arrData['meta_keywords'],
                'meta_description' => $arrData['meta_description'],
                'box_hot'          => $arrData['box_hot'],
                'box_highlight'    => $arrData['box_highlight'],
                'box_home'         => $arrData['box_home'],
                'box_footer'       => $arrData['box_footer'],
                'box_left'         => $arrData['box_left'],
                'box_right'        => $arrData['box_right'],
                'post_category_id' => $arrData['post_category_id'],
                'language'         => $ssSystem->language,
                'options'          => serialize($data_options),
			);

			if(isset($arrData['type'])) {
				$data['type'] = $arrData['type'];
			}
            
	        // Sản phẩm
            $data['product_code']           = $arrData['product_code'];
            $data['product_price']          = $arrData['product_price'] ? $number->formatToData($arrData['product_price']) : null;
            $data['product_price_show']     = $arrData['product_price_show'];
            $data['product_sale_percent']   = $arrData['product_sale_percent'] ? $number->formatToData($arrData['product_sale_percent']) : null;
            $data['product_sale_price']     = $arrData['product_sale_price'] ? $number->formatToData($arrData['product_sale_price']) : null;
            $data['product_status']         = $arrData['product_status'];
            $data['product_unit']           = $arrData['product_unit'];
			
			$this->tableGateway->insert($data);
			return $id;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrItem['id'] ?: $arrData['id'];
		    $data_options = $arrItem['options'] ? unserialize($arrItem['options']) : [];
            if($image_popup->getFull()){
                $data_options['image_popup'] = $image_popup->getFull();
            }
			$data	= array(
                'name'             => $arrData['name'],
                'alias'            => !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
                'status'           => $arrData['status'],
                'ordering'         => $arrData['ordering'],
                'layout'           => $arrData['layout'],
                'description'      => $arrData['description'],
                'content'          => str_replace("&nbsp;", " ", $arrData['content']),
                'image'            => $image->getFull(),
                'image_medium'     => $image->getMedium(),
                'image_thumb'      => $image->getThumb(),
                'images'           => $images ? serialize($images) : '',
                'videos'           => $videos ? serialize($videos) : '',
                'meta_url'         => $arrData['meta_url'],
                'meta_title'       => $arrData['meta_title'],
                'meta_keywords'    => $arrData['meta_keywords'],
                'meta_description' => $arrData['meta_description'],
                'box_hot'          => $arrData['box_hot'],
                'box_highlight'    => $arrData['box_highlight'],
                'box_home'         => $arrData['box_home'],
                'box_footer'       => $arrData['box_footer'],
                'box_left'         => $arrData['box_left'],
                'box_right'        => $arrData['box_right'],
                'post_category_id' => $arrData['post_category_id'],
                'options'          => serialize($data_options),
			);

			if(isset($arrData['type'])) {
				$data['type'] = $arrData['type'];
			}

			// Sản phẩm
            $data['product_code']           = $arrData['product_code'];
            $data['product_price']          = $arrData['product_price'] ? $number->formatToData($arrData['product_price']) : null;
            $data['product_price_show']     = $arrData['product_price_show'];
            $data['product_sale_percent']   = $arrData['product_sale_percent'] ? $number->formatToData($arrData['product_sale_percent']) : null;
            $data['product_sale_price']     = $arrData['product_sale_price'] ? $number->formatToData($arrData['product_sale_price']) : null;
            $data['product_status']         = $arrData['product_status'];
            $data['product_unit']           = $arrData['product_unit'];

			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
		
		if($options['task'] == 'update-product') {
		    foreach ($arrParam AS $key => $val) {
    		    $id = $key;
    		
    		    $data	= array(
    		        'post_category_id'    => $val['post_category_id'],
    		        'product_unit'        => $val['product_unit'],
    		        'product_code'        => $val['product_code'],
    		        'product_status'      => $val['product_status'],
    		        'box_new'             => $val['box_new'],
    		        'box_best'            => $val['box_best'],
    		        'box_highlight'       => $val['box_highlight'],
    		        'box_hot'             => $val['box_hot'],
    		        'ordering'            => $val['ordering'],
    		    );
    		    
    		    $this->tableGateway->update($data, array('id' => $id));
		    }
		    return count($arrParam);
		}
		
		if($options['task'] == 'product-category') {
		    $arrItem = $arrParam['item'];
		    
		    $id = $arrItem['id'];
		    $product_options = !empty($arrItem['options']) ? unserialize($arrItem['options']) : array();
		    if($arrData['code'] == '') {
		        $product_options[] = array(
		            'name' => $arrData['name'],
		            'price' => $number->formatToData($arrData['price']),
		            'image_1' => $arrData['image_1'],
		            'image_2' => $arrData['image_2'],
		            'image_3' => $arrData['image_3'],
		            'image_4' => $arrData['image_4'],
		            'image_5' => $arrData['image_5'],
		            'image_6' => $arrData['image_6'],
		            'image_7' => $arrData['image_7'],
		            'image_8' => $arrData['image_8'],
		            'image_9' => $arrData['image_9'],
		            'image_10' => $arrData['image_10'],
		        );
            } else {
		        $product_options[$arrData['code']] = array(
		            'name' => $arrData['name'],
		            'price' => $number->formatToData($arrData['price']),
		            'image_1' => $arrData['image_1'],
		            'image_2' => $arrData['image_2'],
		            'image_3' => $arrData['image_3'],
		            'image_4' => $arrData['image_4'],
		            'image_5' => $arrData['image_5'],
		            'image_6' => $arrData['image_6'],
		            'image_7' => $arrData['image_7'],
		            'image_8' => $arrData['image_8'],
		            'image_9' => $arrData['image_9'],
		            'image_10' => $arrData['image_10'],
		        );
		    }
		
		    $data	= array(
		        'options'    => serialize($product_options),
		    );
		    
		    $this->tableGateway->update($data, array('id' => $id));
		    return $id;
		}
		
		if($options['task'] == 'delete-product-category') {
		    $arrItem = $arrParam['item'];
		    
		    $id = $arrItem['id'];
		    $product_options = !empty($arrItem['options']) ? unserialize($arrItem['options']) : array();
		    if(!empty($product_options)) {
		        unset($product_options[$arrData['code']]);
    		
    		    $data	= array(
    		        'options'    => serialize($product_options),
    		    );
    		    
    		    $this->tableGateway->update($data, array('id' => $id));
		    }
		    return $id;
		}
		
		if($options['task'] == 'import-update') {
		    $arrItem  = $arrParam['item'];
				
			$id = $arrItem['id'];
			$data	= array(
				'status'             	=> $arrData['status'],
			    'description'        	=> $arrData['description'],
			    'content'            	=> $arrData['content'],
			    'box_hot'            	=> !empty($arrData['box_hot']) ? $arrData['box_hot'] : 0,
			    'box_highlight'      	=> !empty($arrData['box_highlight']) ? $arrData['box_highlight'] : 0,
			    'box_new'          	    => !empty($arrData['box_new']) ? $arrData['box_new'] : 0,
			    'box_best'          	=> !empty($arrData['box_best']) ? $arrData['box_best'] : 0,
			    'product_code'   		=> $arrData['product_code'],
			    'product_price'   	 	=> $arrData['product_price'] ? $number->formatToData($arrData['product_price']) : null,
				'product_price_show'   	=> $arrData['product_price_show'],
			    'product_sale_percent'	=> $arrData['product_sale_percent'] ? $number->formatToData($arrData['product_sale_percent']) : null,
				'product_status'   		=> $arrData['product_status'],
				'product_unit'   		=> $arrData['product_unit'],
			);
		
			// Lưu liên hệ
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
		
		if($options['task'] == 'import-insert') {
			// Thêm mới liên hệ
			$id = $gid->getId();
			$data	= array(
			    'id'                    => $id,
				'name'               	=> $arrData['name'],
				'alias'              	=> !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
				'status'             	=> $arrData['status'],
			    'description'        	=> $arrData['description'],
			    'content'            	=> $arrData['content'],
			    'box_hot'            	=> !empty($arrData['box_hot']) ? $arrData['box_hot'] : 0,
			    'box_highlight'      	=> !empty($arrData['box_highlight']) ? $arrData['box_highlight'] : 0,
			    'box_new'          	    => !empty($arrData['box_new']) ? $arrData['box_new'] : 0,
			    'box_best'          	=> !empty($arrData['box_best']) ? $arrData['box_best'] : 0,
			    'post_category_id'   	=> $arrData['post_category_id'],
				'product_code'   		=> $arrData['product_code'],
				'product_price'   	 	=> $arrData['product_price'] ? $number->formatToData($arrData['product_price']) : null,
				'product_price_show'   	=> $arrData['product_price_show'],
			    'product_sale_percent'	=> $arrData['product_sale_percent'] ? $number->formatToData($arrData['product_sale_percent']) : null,
				'product_status'   		=> $arrData['product_status'],
				'product_unit'   		=> $arrData['product_unit'],
			    'ordering'           	=> 255,
			    'created'            	=> date('Y-m-d H:i:s'),
			    'created_by'         	=> $this->userInfo->getUserInfo('id'),
			);
				
			$this->tableGateway->insert($data);
			return $id;
		}

		if($options['task'] == 'update-view') {
		    $id 		= $arrData['id'];
		    $data    	= array( 'view' => new Expression('(`view` + ?)', array(1)) );
    	    $where   	= new Where();
    	    $where->equalTo('id', $id);
    	    $this->tableGateway->update($data, $where);
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}