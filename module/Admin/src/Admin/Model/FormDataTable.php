<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;

class FormDataTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                
                $select -> join(TABLE_CONTACT, TABLE_CONTACT .'.id='. TABLE_FORM_DATA .'.contact_id', array(), 'inner');
                $select -> where -> equalTo('form_id', $ssFilter['filter_form']);
                
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select -> where -> equalTo(TABLE_FORM_DATA .'.status', $ssFilter['filter_status']);
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> NEST
                			         -> like(TABLE_CONTACT .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> OR
                			         -> like(TABLE_CONTACT .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> OR
                			         -> like(TABLE_CONTACT .'.email', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> UNNEST;
    			}
            })->count();
	    }
	    
	    return $result;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
                
                $select -> join(TABLE_CONTACT, TABLE_CONTACT .'.id='. TABLE_FORM_DATA .'.contact_id', array('name', 'birthday', 'email', 'phone', 'sex', 'address', 'company', 'school'), 'inner');
    			$select -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage'])
    			        -> where -> equalTo('form_id', $ssFilter['filter_form']);
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select -> where -> equalTo(TABLE_FORM_DATA .'.status', $ssFilter['filter_status']);
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select -> where -> NEST
                			         -> like(TABLE_CONTACT .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> OR
                			         -> like(TABLE_CONTACT .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> OR
                			         -> like(TABLE_CONTACT .'.email', '%'. $ssFilter['filter_keyword'] . '%')
                			         -> UNNEST;
    			}
    		});
		}

		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
		
		if($options['task'] == 'get-by-phone') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select -> where -> equalTo('form_id', $arrParam['form_id'])
                                 -> equalTo('phone', $arrParam['phone']);
    		})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier(array( array('HTML.AllowedElements', '') ));
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'public-add') {
		    // Xóa những field đã lưu vào contact
		    unset($arrData['name']);
		    unset($arrData['birthday']);
		    unset($arrData['email']);
		    unset($arrData['phone']);
		    unset($arrData['sex']);
		    unset($arrData['address']);
		    unset($arrData['company']);
		    unset($arrData['school']);
		    unset($arrData['password']);
		    unset($arrData['status']);
		    
		    $id = $gid->getId();
		    $data = array(
		        'id' => $id
		    );
		    foreach ($arrData AS $key => $val) {
		        if(is_array($val)) {
		            $arrTmp = array();
		            foreach ($val AS $k => $v) {
		                $arrTmp[$k] = $filter->filter($v);
		            }
		            $value = serialize($arrTmp);
		        } else {
		            $value = $filter->filter($val);
		        }
		        $data[$key] = $value;
		    }
		    
			$data['status']  = 0;
			$data['created'] = date('Y-m-d H:i:s');
			
			$this->tableGateway->insert($data);
			
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}