<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class Exercise extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));

		// Id
		$this->add(array(
		    'name'			=> 'post_id',
		    'type'			=> 'Hidden',
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'name',
				'placeholder'	=> 'Nhập tên',
			    'onchange'      => 'javascript:createAlias(this, \'#alias\');'
			)
		));
		
		// Alias
		$this->add(array(
		    'name'			=> 'alias',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'alias',
		        'onchange'      => 'javascript:createAlias(\'#name\', \'#alias\');'
		    )
		));

		// Type
		$this->add(array(
			'name'			=> 'type',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		    	'empty_option'	=> '- Chọn -',
		        'value_options'	=> json_decode(TYPE_EXERCISE, true),
		    )
		));

		// Type
		$this->add(array(
			'name'			=> 'time_limit',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control mask_number',
			),
		));

		// min_word
		$this->add(array(
			'name'			=> 'min_word',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control mask_number',
			),
		));

		// max_word
		$this->add(array(
			'name'			=> 'max_word',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control mask_number',
			),
		));
		
		// Ordering
		$this->add(array(
		    'name'			=> 'ordering',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'value'         => 255,
		        'class'			=> 'form-control mask_number',
		        'id'			=> 'ordering',
		        'placeholder'	=> 'Thứ tự'
		    )
		));
		
		// Status
		$this->add(array(
			'name'			=> 'status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		));

		// Nghe
		// Audio
		$this->add(array(
		    'name'			=> 'audio',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'audio',
		        'placeholder'	=> 'Chọn audio',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'files',
		    ),
		));
		// Ghi chú Audio
		$this->add(array(
		    'name'			=> 'note_audio',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		    ),
		));

		// Content
		$this->add(array(
		    'name'			=> 'content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'content',
		    )
		));

		// content_question
		$this->add(array(
		    'name'			=> 'content_question',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'content_question',
		    )
		));

		// Content result
		$this->add(array(
		    'name'			=> 'content_result',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'content_result',
		    )
		));
		
		// Meta Url
		$this->add(array(
		    'name'			=> 'meta_url',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_url',
		    )
		));
		
		// Meta Title
		$this->add(array(
		    'name'			=> 'meta_title',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_title',
		    )
		));
		
		// Meta Keywords
		$this->add(array(
		    'name'			=> 'meta_keywords',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_keywords',
		    )
		));
		
		// Meta Description
		$this->add(array(
		    'name'			=> 'meta_description',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_description',
		    )
		));
	}
}