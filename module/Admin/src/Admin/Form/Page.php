<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class Page extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'name',
				'placeholder'	=> 'Nhập tên',
			    'onchange'      => 'javascript:createAlias(this, \'#alias\');'
			)
		));
		
		// Alias
		$this->add(array(
		    'name'			=> 'alias',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'alias',
		        'onchange'      => 'javascript:createAlias(\'#name\', \'#alias\');'
		    )
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image Course
		$this->add(array(
		    'name'			=> 'image_course',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Ordering
		$this->add(array(
		    'name'			=> 'ordering',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'value'         => 255,
		        'class'			=> 'form-control mask_number',
		        'id'			=> 'ordering',
		        'placeholder'	=> 'Thứ tự'
		    )
		));
		
		// Status
		$this->add(array(
			'name'			=> 'status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		));
		
		// Layout
		$template = new \ZendX\System\Template();
		$this->add(array(
		    'name'			=> 'layout',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'value_options'	=> $template->getTemplate(),
		    )
		));
		
		// Form
		$template = new \ZendX\System\Template();
		$this->add(array(
		    'name'			=> 'form',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'value_options'	=> $template->getForm(),
		    )
		));

		
		// Description
		$this->add(array(
		    'name'			=> 'description',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'description',
		    )
		));
		
		// Content
		$this->add(array(
		    'name'			=> 'content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'content',
		    )
		));
		
		// Meta Url
		$this->add(array(
		    'name'			=> 'meta_url',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_url',
		    )
		));
		
		// Meta Title
		$this->add(array(
		    'name'			=> 'meta_title',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_title',
		    )
		));
		
		// Meta Keywords
		$this->add(array(
		    'name'			=> 'meta_keywords',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_keywords',
		    )
		));
		
		// Meta Description
		$this->add(array(
		    'name'			=> 'meta_description',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_description',
		    )
		));
		
		// Box Hot
		$this->add(array(
		    'name'			=> 'box_hot',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Highlight
		$this->add(array(
		    'name'			=> 'box_highlight',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Home
		$this->add(array(
		    'name'			=> 'box_home',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Footer
		$this->add(array(
		    'name'			=> 'box_footer',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Left
		$this->add(array(
		    'name'			=> 'box_left',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Right
		$this->add(array(
		    'name'			=> 'box_right',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Form
		$this->add(array(
			'name'			=> 'form_id',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
			)
		));

		// Mã khóa học
		$this->add(array(
		    'name'			=> 'course_code',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		    )
		));
		
		// Diễn giả
		$this->add(array(
		    'name'			=> 'course_teacher_id',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		        'id'		=> 'course_teacher_id'
		    )
		));
		
		// Giá khóa học
		$this->add(array(
		    'name'			=> 'course_price',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control mask_currency',
		        'id'			=> 'course_price'
		    )
		));

		// Danh mục
		$this->add(array(
		    'name'			=> 'course_category_id',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		    )
		));

		// Gái vé sale
		$this->add(array(
		    'name'			=> 'course_price_sale',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		    )
		));
	}
}