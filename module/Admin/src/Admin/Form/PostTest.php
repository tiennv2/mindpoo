<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class PostTest extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'name',
				'placeholder'	=> 'Nhập tên',
			    'onchange'      => 'javascript:createAlias(this, \'#alias\');'
			)
		));
		
		// Alias
		$this->add(array(
		    'name'			=> 'alias',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'alias',
		        'onchange'      => 'javascript:createAlias(\'#name\', \'#alias\');'
		    )
		));
		
		// Post Category
		$this->add(array(
		    'name'			=> 'category_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\PostCategoryTable')->listItem(['data' => ['type' => 'post-test']], array('task' => 'list-all')), array('key' => 'id', 'value' => 'name', 'level' => true, 'level_start' => 1)),
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));

		// Type
		$this->add(array(
			'name'			=> 'type',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 'post' => 'Bài viết', 'test' => 'Bài luyện thi'),
		    )
		));
		
		// Ordering
		$this->add(array(
		    'name'			=> 'ordering',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'value'         => 255,
		        'class'			=> 'form-control mask_number',
		        'id'			=> 'ordering',
		        'placeholder'	=> 'Thứ tự'
		    )
		));
		
		// Status
		$this->add(array(
			'name'			=> 'status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		));
		
		// Layout
		$template = new \ZendX\System\Template();
		$this->add(array(
		    'name'			=> 'layout',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> 'Tự động theo danh mục',
		        'value_options'	=> $template->getLayout(),
		    )
		));
		
		// Description
		$this->add(array(
		    'name'			=> 'description',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'description',
		    )
		));
		
		// Content
		$this->add(array(
		    'name'			=> 'content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'content',
		    )
		));
		
		// Meta Url
		$this->add(array(
		    'name'			=> 'meta_url',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_url',
		    )
		));
		
		// Meta Title
		$this->add(array(
		    'name'			=> 'meta_title',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_title',
		    )
		));
		
		// Meta Keywords
		$this->add(array(
		    'name'			=> 'meta_keywords',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_keywords',
		    )
		));
		
		// Meta Description
		$this->add(array(
		    'name'			=> 'meta_description',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_description',
		    )
		));
	}
}