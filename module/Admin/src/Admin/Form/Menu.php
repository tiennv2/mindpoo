<?php
namespace Admin\Form;
use Zend\Form\Form as Form;

class Menu extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'name',
				'placeholder'	=> 'Nhập tên',
			)
		));
		
		// Post Category
		$this->add(array(
		    'name'			=> 'param_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listItem(null, array('task' => 'list-all')), array('key' => 'id', 'value' => 'name', 'level' => true, 'level_start' => 1)),
		    ),
		));
		
		// Link
		$this->add(array(
		    'name'			=> 'link',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'link',
		        'placeholder'	=> '',
		    )
		));
		
		// Target
		$this->add(array(
		    'name'			=> 'target',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		        'value'     => '_self',
		    ),
		    'options'		=> array(
		        'value_options'	=> array( '_self' => 'Tại trang (_self)', '_blank' => 'Cửa sổ mới (_blank)'),
		    )
		));

		// Link
		$this->add(array(
		    'name'			=> 'class',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'placeholder'	=> '',
		    )
		));
		
		// Icon
		$this->add(array(
		    'name'			=> 'icon',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'icon',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Code
		$this->add(array(
		    'name'			=> 'code',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'code',
		        'placeholder'	=> '',
		    )
		));
		
		// Description
		$this->add(array(
		    'name'			=> 'description',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'description',
		    )
		));
		
		// Content
		$this->add(array(
		    'name'			=> 'content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'content',
		    )
		));
	}
}