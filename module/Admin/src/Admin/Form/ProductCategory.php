<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class ProductCategory extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Code
		$this->add(array(
		    'name'			=> 'code',
		    'type'			=> 'Hidden',
		));
		
		// Modal
		$this->add(array(
		    'name'			=> 'modal',
		    'type'			=> 'Hidden',
		    'attributes'	=> array(
		        'value'		=> 'success',
		    ),
		));
		
		// Tên loại
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'placeholder'	=> ''
			)
		));
		
		// Giá bán
		$this->add(array(
			'name'			=> 'price',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control mask_currency',
				'placeholder'	=> ''
			)
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image_1',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_1',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image_2',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_2',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image_3',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_3',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image_4',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_4',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image_5',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_5',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image_6',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_6',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image_7',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_7',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image_8',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_8',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image_9',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_9',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image_10',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_10',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
	}
}