<?php
namespace Admin\Form\Search;
use \Zend\Form\Form as Form;

class Product extends Form{
    
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Keyword
		$this->add(array(
		    'name'			=> 'filter_keyword',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'placeholder'   => 'Từ khóa',
		        'class'			=> 'form-control input-sm',
		        'id'			=> 'filter_keyword',
		    ),
		));
		
		// Status
		$this->add(array(
		    'name'			=> 'filter_status',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Trạng thái -',
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		));
		
		// Tình trạng
		$this->add(array(
		    'name'			=> 'filter_product_status',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Tình trạng -',
		        'value_options'	=> array( 1	=> 'Còn hàng', 0 => 'Hết hàng'),
		    )
		));
		
		// Post Category
		$this->add(array(
		    'name'			=> 'filter_category',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Danh mục -',
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\PostCategoryTable')->listItem(null, array('task' => 'list-product')), array('key' => 'id', 'value' => 'name', 'level' => true, 'level_start' => 2)),
		    ),
		));
		
		// Submit
		$this->add(array(
		    'name'			=> 'filter_submit',
		    'type'			=> 'Submit',
		    'attributes'	=> array(
		        'value'     => 'Tìm',
		        'class'		=> 'btn btn-sm green btn-block',
		    ),
		));

		$this->add(array(
            'name'          => 'filter_reset',
            'type'          => 'Submit',
            'attributes'    => array(
                'class'         => 'btn btn-block btn-sm btn-danger',
                'value'         => 'Xóa'
            ),
        ));
		
		// Order
		$this->add(array(
		    'name'			=> 'order',
		    'type'			=> 'Hidden',
		));
		
		// Order By
		$this->add(array(
		    'name'			=> 'order_by',
		    'type'			=> 'Hidden',
		));
	}
}