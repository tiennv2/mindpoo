<?php
namespace Admin\Form\Search;
use \Zend\Form\Form as Form;

class ProductCart extends Form{
    
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Keyword
		$this->add(array(
		    'name'			=> 'filter_keyword',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'placeholder'   => 'Từ khóa',
		        'class'			=> 'form-control input-sm',
		        'id'			=> 'filter_keyword',
		    ),
		));
		
		// Submit
		$this->add(array(
		    'name'			=> 'filter_submit',
		    'type'			=> 'Submit',
		    'attributes'	=> array(
		        'value'     => 'Tìm',
		        'class'		=> 'btn btn-sm green btn-block',
		    ),
		));

		$this->add(array(
            'name'          => 'filter_reset',
            'type'          => 'Submit',
            'attributes'    => array(
                'class'         => 'btn btn-block btn-sm btn-danger',
                'value'         => 'Xóa'
            ),
        ));
	}
}