<?php

namespace Admin;

return array (
	'controllers' => array(
		'invokables' => array(
			'Admin\Controller\Api'            => Controller\ApiController::class,
			'Admin\Controller\Index'          => Controller\IndexController::class,
			'Admin\Controller\Menu'           => Controller\MenuController::class,
			'Admin\Controller\Setting'        => Controller\SettingController::class,
			'Admin\Controller\Dynamic'        => Controller\DynamicController::class,
			'Admin\Controller\Document'       => Controller\DocumentController::class,
			'Admin\Controller\Notice'         => Controller\NoticeController::class,
			'Admin\Controller\User'           => Controller\UserController::class,
			'Admin\Controller\UserGroup'      => Controller\UserGroupController::class,
			'Admin\Controller\UserPermission' => Controller\UserPermissionController::class,
			'Admin\Controller\PostCategory'   => Controller\PostCategoryController::class,
			'Admin\Controller\PostItem'       => Controller\PostItemController::class,
			'Admin\Controller\Form'           => Controller\FormController::class,
			'Admin\Controller\FormData'       => Controller\FormDataController::class,
			'Admin\Controller\ProductCart'    => Controller\ProductCartController::class,
			'Admin\Controller\Product'        => Controller\ProductController::class,
			'Admin\Controller\Contact'        => Controller\ContactController::class,
			'Admin\Controller\Page'           => Controller\PageController::class,
			'Admin\Controller\Ielts'          => Controller\IeltsController::class,
			'Admin\Controller\Exercise'       => Controller\ExerciseController::class,
			'Admin\Controller\Teacher'        => Controller\TeacherController::class,
			'Admin\Controller\CourseCategory'    => Controller\CourseCategoryController::class,
			'Admin\Controller\CourseItem'    	 => Controller\CourseItemController::class,
		    'Admin\Controller\CourseGroup'    	 => Controller\CourseGroupController::class,
			'Admin\Controller\CourseDetail'    	 => Controller\CourseDetailController::class,
		)
	),
	'view_manager' => array(
		'doctype'					=> 'HTML5',
		'display_not_found_reason' 	=> (APPLICATION_ENV == 'development') ? true : false,
		'not_found_template'       	=> 'error/404',
			
		'display_exceptions'       	=> (APPLICATION_ENV == 'development') ? true : false,
		'exception_template'       	=> 'error/index',
				
		'template_path_stack'		=> array(__DIR__ . '/../view'),
		'template_map' 				=> array(
			'layout/layout'         => PATH_TEMPLATE . '/frontend/main.phtml',
			'layout/frontend'       => PATH_TEMPLATE . '/frontend/main.phtml',
			'layout/backend'        => PATH_TEMPLATE . '/backend/main.phtml',
		    'error/layout'          => PATH_TEMPLATE . '/error/layout.phtml',
			'error/404'             => PATH_TEMPLATE . '/error/404.phtml',
			'error/index'           => PATH_TEMPLATE . '/error/index.phtml',
		),
		'default_template_suffix'  	=> 'phtml',
		'layout'					=> 'layout/layout'
	),
    'view_helper_config' => array(
        'flashmessenger' => array(
            'message_open_format' => '<div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><p>',
            'message_close_string' => '</p></div>',
            'message_separator_string' => '',
        )
    ),
);


