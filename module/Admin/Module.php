<?php

namespace Admin;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Module {
	public function onBootstrap(MvcEvent $e) {
		$eventManager        = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
		
		$adapter = $e->getApplication()->getServiceManager()->get('dbConfig');
		GlobalAdapterFeature::setStaticAdapter($adapter);
	}
	
    public function getConfig() {
        return array_merge(
            include __DIR__ . '/config/module.config.php',
            include __DIR__ . '/config/router.config.php'
        );
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            )
        );
    }
    
    public function getServiceConfig(){
        return array(
            'factories'	=> array(
                // Khai báo các Model
                'Admin\Model\DynamicTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_DYNAMIC, $adapter, null);
                    return new \Admin\Model\DynamicTable($tableGateway);
                },
                'Admin\Model\DocumentTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_DOCUMENT, $adapter, null);
                    return new \Admin\Model\DocumentTable($tableGateway);
                },
                'Admin\Model\MenuTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_MENU, $adapter, null);
                    return new \Admin\Model\MenuTable($tableGateway);
                },
                'Admin\Model\NestedTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_NESTED, $adapter, null);
                    return new \Admin\Model\NestedTable($tableGateway);
                },
                'Admin\Model\SettingTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_SETTING, $adapter, null);
                    return new \Admin\Model\SettingTable($tableGateway);
                },
                'Admin\Model\UserTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_USER, $adapter, null);
                    return new \Admin\Model\UserTable($tableGateway);
                },
                'Admin\Model\UserGroupTable' => function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_USER_GROUP, $adapter, null);
                    return new \Admin\Model\UserGroupTable($tableGateway);
                },
                'Admin\Model\UserPermissionTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_USER_PERMISSION, $adapter, null);
                    return new \Admin\Model\UserPermissionTable($tableGateway);
                },
                'Admin\Model\PostCategoryTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_POST_CATEGORY, $adapter, null);
                    return new \Admin\Model\PostCategoryTable($tableGateway);
                },
                'Admin\Model\PostItemTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_POST_ITEM, $adapter, null);
                    return new \Admin\Model\PostItemTable($tableGateway);
                },
                'Admin\Model\FormTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_FORM, $adapter, null);
                    return new \Admin\Model\FormTable($tableGateway);
                },
                'Admin\Model\FormDataTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_FORM_DATA, $adapter, null);
                    return new \Admin\Model\FormDataTable($tableGateway);
                },
                'Admin\Model\ProductCartTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_PRODUCT_CART, $adapter, null);
                    return new \Admin\Model\ProductCartTable($tableGateway);
                },
                'Admin\Model\ContactTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_CONTACT, $adapter, null);
                    return new \Admin\Model\ContactTable($tableGateway);
                },
                'Admin\Model\PageTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_PAGE, $adapter, null);
                    return new \Admin\Model\PageTable($tableGateway);
                },
                'Admin\Model\PostTestTable' => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_POST_TEST, $adapter, null);
                    return new \Admin\Model\PostTestTable($tableGateway);
                },
                'Admin\Model\ExerciseTable' => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_EXERCISE, $adapter, null);
                    return new \Admin\Model\ExerciseTable($tableGateway);
                },
                'Admin\Model\CourseCategoryTable'  => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_COURSE_CATEGORY, $adapter, null);
                    return new \Admin\Model\CourseCategoryTable($tableGateway);
                },
                'Admin\Model\CourseItemTable'   => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_COURSE_ITEM, $adapter, null);
                    return new \Admin\Model\CourseItemTable($tableGateway);
                },
                'Admin\Model\CourseGroupTable'  => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_COURSE_GROUP, $adapter, null);
                    return new \Admin\Model\CourseGroupTable($tableGateway);
                },
                'Admin\Model\CourseDetailTable' => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_COURSE_DETAIL, $adapter, null);
                    return new \Admin\Model\CourseDetailTable($tableGateway);
                },

                // Khai báo xác thực đăng nhập
                'AuthenticateService'	=> function ($sm) {
                    $dbTableAdapter = new \Zend\Authentication\Adapter\DbTable($sm->get('dbConfig'), TABLE_USER, 'username', 'password', 'MD5(?)');
                    $dbTableAdapter->getDbSelect()->where->equalTo('status', 1)
                                                  ->where->equalTo('active_code', 1);
                    
                    $authenticateServiceObj = new \Zend\Authentication\AuthenticationService(null, $dbTableAdapter);
                    return $authenticateServiceObj;
                },
                'MyAuth'	=> function ($sm) {
                    return new \ZendX\System\Authenticate($sm->get('AuthenticateService'));
                },
            ),
            'invokables' => array(
                'Zend\Authentication\AuthenticationService' => 'Zend\Authentication\AuthenticationService',
            ),
        );
    }

    public function getFormElementConfig() {
        return array(
            'factories' => array(
                'formAdminDynamic' => function($sm) {
                    $myForm	= new \Admin\Form\Dynamic($sm);
                    $myForm->setInputFilter(new \Admin\Filter\Dynamic());
                    return $myForm;
                },
                'formAdminDocument' => function($sm) {
                    $myForm	= new \Admin\Form\Document($sm);
                    $myForm->setInputFilter(new \Admin\Filter\Document());
                    return $myForm;
                },
                'formAdminMenu' => function($sm) {
                    $myForm	= new \Admin\Form\Menu($sm);
                    $myForm->setInputFilter(new \Admin\Filter\Menu());
                    return $myForm;
                },
                'formAdminSetting' => function($sm) {
                    $myForm	= new \Admin\Form\Setting($sm);
                    $myForm->setInputFilter(new \Admin\Filter\Setting());
                    return $myForm;
                },
                'formAdminUser' => function($sm) {
                    $myForm	= new \Admin\Form\User($sm);
                    $myForm->setInputFilter(new \Admin\Filter\User());
                    return $myForm;
                },
                'formAdminUserGroup' => function($sm) {
                    $myForm	= new \Admin\Form\UserGroup($sm);
                    $myForm->setInputFilter(new \Admin\Filter\UserGroup());
                    return $myForm;
                },
                'formAdminUserPermission' => function($sm) {
                    $myForm	= new \Admin\Form\UserPermission($sm);
                    $myForm->setInputFilter(new \Admin\Filter\UserPermission());
                    return $myForm;
                },
                'formAdminPostCategory' => function($sm) {
                    $myForm	= new \Admin\Form\PostCategory($sm);
                    $myForm->setInputFilter(new \Admin\Filter\PostCategory());
                    return $myForm;
                },
                'formAdminPostItem' => function($sm) {
                    $myForm	= new \Admin\Form\PostItem($sm);
                    $myForm->setInputFilter(new \Admin\Filter\PostItem());
                    return $myForm;
                },
                'formAdminProduct' => function($sm) {
                    $myForm	= new \Admin\Form\Product($sm);
                    $myForm->setInputFilter(new \Admin\Filter\Product());
                    return $myForm;
                },
                'formAdminForm' => function($sm) {
                    $myForm	= new \Admin\Form\Form($sm);
                    $myForm->setInputFilter(new \Admin\Filter\Form());
                    return $myForm;
                },
                'formAdminFormData' => function($sm) {
                    $myForm	= new \Admin\Form\FormData($sm);
                    $myForm->setInputFilter(new \Admin\Filter\FormData());
                    return $myForm;
                },
                'formAdminContact' => function($sm) {
                    $myForm	= new \Admin\Form\Contact($sm);
                    $myForm->setInputFilter(new \Admin\Filter\Contact());
                    return $myForm;
                },
                'formAdminSearch' => function($sm) {
                    $myForm	= new \Admin\Form\Search($sm);
                    return $myForm;
                },
                'formAdminLogin' => function($sm) {
                    $myForm	= new \Admin\Form\Login($sm);
                    $myForm->setInputFilter(new \Admin\Filter\Login());
                    return $myForm;
                },
                'formAdminPage' => function($sm) {
                	$myForm	= new \Admin\Form\Page($sm);
                	$myForm->setInputFilter(new \Admin\Filter\Page());
                	return $myForm;
                },
                'formAdminCourseCategory' => function($sm) {
                    $myForm = new \Admin\Form\CourseCategory($sm);
                    $myForm->setInputFilter(new \Admin\Filter\CourseCategory());
                    return $myForm;
                },
                'formAdminCourseItem' => function($sm) {
                    $myForm = new \Admin\Form\CourseItem($sm);
                    $myForm->setInputFilter(new \Admin\Filter\CourseItem());
                    return $myForm;
                },
                'formAdminCourseGroup' => function($sm) {
                $myForm = new \Admin\Form\CourseGroup($sm);
                $myForm->setInputFilter(new \Admin\Filter\CourseGroup());
                return $myForm;
                },
                'formAdminCourseDetail' => function($sm) {
                    $myForm = new \Admin\Form\CourseDetail($sm);
                    $myForm->setInputFilter(new \Admin\Filter\CourseDetail());
                    return $myForm;
                },
            )
        );
    }

    public function getViewHelperConfig() {
        return array(
            'invokables' => array(
                'xViewElementError'	    => '\ZendX\View\Helper\ElementError',
                'xViewElementErrors'	=> '\ZendX\View\Helper\ElementErrors',
                'xViewInfoPrice'	    => '\ZendX\View\Helper\InfoPrice',
                'xFormSelect'           => '\ZendX\Form\View\Helper\FormSelect',
                'xFormHidden'           => '\ZendX\Form\View\Helper\FormHidden',
                'xFormInput'            => '\ZendX\Form\View\Helper\FormInput',
                'xFormButton'           => '\ZendX\Form\View\Helper\FormButton',
                'linkAdmin'             => '\ZendX\View\Helper\Url\LinkAdmin',
                'linkAdminSort'         => '\ZendX\View\Helper\Url\LinkAdminSort',
                'linkAdminStatus'       => '\ZendX\View\Helper\Url\LinkAdminStatus',
                'linkAdminHtml'         => '\ZendX\View\Helper\Url\LinkAdminHtml',
                'blkMenu'               => '\Block\Menu',
                'blkBreadcrumb'         => '\Block\Breadcrumb',
            )
        );
    }
}
