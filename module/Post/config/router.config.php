<?php

$routeHome = array(
    'type' => 'Literal',
    'options' => array (
        'route' => '/',
        'defaults' => array (
            '__NAMESPACE__' => 'Post\Controller',
            'controller' 	=> 'Index',
            'action' 		=> 'index'
        )
    ),
);

$routePostItem = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/(?<alias>[^\/]*)(/)?',
        'defaults' 	=> array(
            '__NAMESPACE__' => 'Post\Controller',
            'controller'    => 'Index',
            'action'        => 'category',
        ),
        'spec' 		=> '/%alias%',
    ),
);

$routePostPaginator = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/(?<alias>[^\/]*)/page/(?<page>[0-9]*)(/)?',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Post\Controller',
            'controller' 		=> 'Index',
            'action' 			=> 'category',
        ),
        'spec' 		=> '/%alias%/page/%page%',
    ),
);

$routePostCategory = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/(?<alias>[^\/]*)(/)?',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Post\Controller',
            'controller' 		=> 'Index',
            'action' 			=> 'category',
        ),
        'spec' 		=> '/%alias%',
    ),
);

$routePost = array(
    'type' => 'Segment',
    'options' => array (
        'route' => '/post',
        'defaults' => array (
            '__NAMESPACE__' => 'Post\Controller',
            'controller' 	=> 'Index',
            'action' 		=> 'index'
        )
    ),
    'may_terminate' => true,
    'child_routes' => array (
        'default' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/[:controller[/:action[/id/:id]]][/]',
                'constraints' => array (
                    'controller' 	=> '[a-zA-Z0-9_-]*',
                    'action' 		=> '[a-zA-Z0-9_-]*',
                    'id' 		    => '[a-zA-Z0-9_-]*',
                ),
                'defaults' => array ()
            )
        )
    )
);

$routeLanguage = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/language/(?<code>[a-z]*)(/)?',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Admin\Controller',
            'controller' 		=> 'api',
            'action' 			=> 'language',
        ),
        'spec' 		=> '/language/%code%',
    ),
);

$routeSearch = array(
    'type' 		=> 'Segment',
    'options' => array (
        'route' => '/search',
        'defaults' => array (
            '__NAMESPACE__' => 'Post\Controller',
            'controller' 	=> 'Index',
            'action' 		=> 'search'
        )
    ),
);

$routeExercise = array(
    'type'      => 'Regex',
    'options'   => array(
        'regex'     => '/(?<post_alias>[^\/]*)/(?<alias>[^\/]*)(/)?',
        'defaults'  => array(
            '__NAMESPACE__' => 'Post\Controller',
            'controller'    => 'Index',
            'action'        => 'exercise',
        ),
        'spec'      => '/%post_alias%/%alias%',
    ),
);

return array (
    'router' => array(
        'routes' => array(
            'routePostCategory'  => $routePostCategory,
            'routePostPaginator' => $routePostPaginator,
            'routePostItem'      => $routePostItem,
            'routeHome'          => $routeHome,
            'routePost'          => $routePost,
            'routeLanguage'      => $routeLanguage,
            'routeSearch'        => $routeSearch,
            'routeExercise'      => $routeExercise,
        ),
    )
);