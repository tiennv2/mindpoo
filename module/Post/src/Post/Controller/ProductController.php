<?php

namespace Post\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class ProductController extends ActionController {
    protected $_settings;
    
    public function init() {
        $this->setLayout('layout/product');
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 5;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Thiết lập options
        $this->_options['tableName'] = 'Post\Model\PostItemTable';
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray(), $this->params()->fromRoute());
        
        // Thiết lập Meta
        $this->_params['meta'] = array(
            'author' => $this->_settings['General.Meta.Author']['value'],
            'image' => $this->_settings['General.Meta.Image']['image'],
            'title' => $this->_settings['General.Meta.Title']['value'],
            'description' => $this->_settings['General.Meta.Description']['description'],
            'keywords' => $this->_settings['General.Meta.Keywords']['description'],
        );
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['language'] = $this->_language;
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function indexAction() {
        $this->setLayout('home');
        
        return new ViewModel($this->_viewModel);
    }
    
    public function cartAction(){
        $ssFilter = new Container('myCart');
        $productCart = $ssFilter->data;
        $error = '';
        if($this->getRequest()->isPost()){
            if($this->_params['data']['name'] == null && $this->_params['data']['email'] == null && $this->_params['data']['phone'] == null && $this->_params['data']['address'] == null ){
                $error = 'Vui lòng nhập đầy đầy đủ các thông tin: Họ tên, email, điện thoại và địa chỉ giao hàng';
            } else {
                // Check contact xem có tồn tại không
                $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('phone' => $this->_params['data']['phone']), array('task' => 'by-phone'));
                if(!empty($contact)) {
                    $this->_params['data']['contact_id'] = $contact['id'];
                } else {
                    $this->_params['data']['contact_id'] = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'add-item'));
                }
                
                $this->getServiceLocator()->get('Admin\Model\ProductCartTable')->saveItem($this->_params, array('task' => 'add-item'));
                
                // Gửi mail
                if(!empty($this->_settings['General.System.EmailCart']['value'])) {
                    $email_cart = explode(',', $this->_settings['General.System.EmailCart']['value']);
                    $product = '';
                    if(!empty($productCart)) {
                        foreach ($productCart AS $key => $val){
                            $id             = $val['product']['id'];
                            $name           = $val['product']['name'];
                            $price_old      = '';
                            $product_price	= 0;
                            if(!empty($val['product']['product_price'])) {
                                if(!empty($val['product']['product_sale_percent'])) {
                                    $product_price	= $val['product']['product_price'] - ($val['product']['product_sale_percent'] / 100 * $val['product']['product_price']);
                                } else if(!empty($val['product']['product_sale_price'])) {
                                    $product_price	= $val['product']['product_price'] - $val['product']['product_sale_price'];
                                } else {
                                    $product_price	= $val['product']['product_price'];
                                }
                            }
                            $amount         = $val['amount'];
                            if($product_price > 0) {
                                $price      = $product_price * $amount;
                                $totalPrice = $totalPrice + $price;
                    
                                if($product_price != $val['product']['product_price']) {
                                    $price_old = '<span class="price_old mask_currency">'. $val['product']['product_price'] .'</span>';
                                }
                            } else {
                                $product_price = 'Liên hệ';
                                $price = '';
                            }
                    
                            $product .= '<p>'. $name .'<br>Đơn giá KM: '. number_format($product_price,0,",",".") .'<br>Số lượng: '. $amount .'<br>Thành tiền: '. number_format($price,0,",",".") .'</p>';
                        }
                    }
                    
                    $options['fromName']    = 'Hệ thống';
                    $options['to']          = current($email_cart);
                    $options['cc']          = $email_cart;
                    $options['toName']      = 'Đặt hàng';
                    $options['subject']     = 'Đặt hàng - ' . @date('d/m/Y H:i');
                    $options['content']     = '
                        <p>Họ tên: '. $this->_params['data']['name'] .'<br>
                        Điện thoại: '. $this->_params['data']['phone'] .'<br>
                        Email: '. $this->_params['data']['email'] .'<br>
                        Địa chỉ: '. $this->_params['data']['address'] .'<br>
                        Ghi chú: '. $this->_params['data']['note'] .'</p>
                        <p>SẢN PHẨM</p>
                        <p>'. $product .'</p>
                    ';
                    $mailObj = new \ZendX\Mail\Mail();
                    $mailObj->sendMail($options);
                }
                
                $ssFilter->submitData = $this->_params['data'];
                
                //$this->goRoute(array('route' => 'routePost/default', 'controller' => $this->_params['controller'], 'action' => 'success-cart'));
                $error = 'success';
            }
            echo $error;
            return $this->response;
        }
        
        // Thiết lập Meta
        $this->_params['meta']['title']  = 'Giỏ hàng';
        
        $this->_viewModel['productCart'] = $productCart;
        $this->_params['error']	         = $error;
        $this->_viewModel['params']	     = $this->_params;
        $viewModel = new ViewModel($this->_viewModel);
        return $viewModel;
    }
    
    public function addCartAction(){
        $ssFilter = new Container('myCart');
        //$ssFilter->setExpirationSeconds(500);
        //$ssFilter->getManager()->getStorage()->clear('myCart');
        
        //Lấy thông tin sản phẩm theo id
        $item       = $this->getServiceLocator()->get('Post\Model\PostItemTable')->getItem(array('id' => $this->_params['data']['id']));
        $dataCart   = $ssFilter->data;
        $keyCart    = $this->_params['data']['option'] . $this->_params['data']['id'];
        $json       = array();
            
        if(empty($dataCart[$keyCart])){
            $number = !empty((int)$this->_params['data']['number']) ? (int)$this->_params['data']['number'] : 1;
            $amount = !empty($number) ? $number : 1;
            
            $price = $amount * intval($price);
            $dataCart[$keyCart] = array(
                'product_id'                => $item['id'],
                'product_index'             => $item['index'],
                'product_name'              => $item['name'],
                'product_image'             => $item['image'],
                'product_image_medium'      => $item['image_medium'],
                'product_price'             => $item['product_price'],
                'product_sale_percent'      => $item['product_sale_percent'],
                'product_sale_price'        => $item['product_sale_price'],
                'product_post_category_id'  => $item['post_category_id'],
                'amount'                    => $amount, // Số lượng
                'option'                    => $this->_params['data']['option']
            );
        } else {
            $number = !empty((int)$this->_params['data']['number']) ? (int)$this->_params['data']['number'] : 1;
            
            $amount = $ssFilter->data[$keyCart]['amount'] + $number;
            $amount = !empty($amount) ? $amount: 1;
            
            $dataCart[$keyCart] = array(
                'product_id'                => $item['id'],
                'product_index'             => $item['index'],
                'product_name'              => $item['name'],
                'product_image'             => $item['image'],
                'product_image_medium'      => $item['image_medium'],
                'product_price'             => $item['product_price'],
                'product_sale_percent'      => $item['product_sale_percent'],
                'product_sale_price'        => $item['product_sale_price'],
                'product_post_category_id'  => $item['post_category_id'],
                'amount' => $amount, // Số lượng
                'option' => $this->_params['data']['option']
            );
        }
        $ssFilter->data = $dataCart;
        
        foreach ($dataCart AS $key => $value) {
            $json['total_product']++;
            $json['total_amount'] = $json['total_amount'] + $value['amount'];
        }
        
        if($this->getRequest()->isXmlHttpRequest()) {
            echo json_encode($json);
            return $this->response;
        } else {
            return $this->goRoute(array('route' => 'routePost/default', 'controller' => $this->_params['controller'], 'action' => 'cart'));
        }
    }
    
    public function listCartAction(){
        $ssFilter = new Container('myCart');
        $productCart = $ssFilter->data;
    
        $this->_viewModel['productCart'] = $productCart;
        $this->_viewModel['params']	     = $this->_params;
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
    
    public function updateCartAction(){
        $ssFilter = new Container('myCart');
        $productCart = $ssFilter->data;
        
        if(!empty($productCart[$this->_params['data']['key']])) {
            $productCart[$this->_params['data']['key']]['amount'] = !empty($this->_params['data']['amount']) ? $this->_params['data']['amount'] : 1;
        }
        $ssFilter->data = $productCart;
        
        return $this->response;
    }
    
    public function deleteCartAction(){
        // Xóa 1 sản phẩm trong giỏ hàng đi
        $ssFilter = new Container('myCart');
        $dataCarts = $ssFilter->data;
        unset($ssFilter->data[$this->_params['data']['key']]);
        
        return $this->response;
    }
    
    public function countCartAction(){
        $ssCart     = new Container('myCart');
        $totalCarts = $ssCart->data;
        $total = 0;
        if(count($totalCarts) > 0){
            foreach ($totalCarts AS $totalCart){
                $amount = $totalCart['amount'];
                $total += $amount;
            }
        }
        echo $total;
        return $this->response;
    }
    
    public function successCartAction(){
        $ssFilter = new Container('myCart');
        $productCart = $ssFilter->data;
        $submitData = $ssFilter->submitData;
        
        $ssFilter->getManager()->getStorage()->clear('myCart');
        $success = 'Bạn đã hoàn tất đặt hàng';
        
        // Thiết lập Meta
        $this->_params['meta']['title']  = 'Hoàn tất đặt hàng';
        
        $this->_viewModel['productCart'] = $productCart;
        $this->_viewModel['submitData']  = $submitData;
        $this->_viewModel['params']	     = $this->_params;
        $this->_viewModel['success']     = $success;
        return new ViewModel($this->_viewModel);
    }
    
    public function reportAction(){
        $ssFilter = new Container('myCart');
        $dataCart = $ssFilter->data;
    
        $json = array();
        $json['total_product'] = 0;
        $json['total_amount'] = 0;
        $json['total_price'] = 0;
        if(!empty($dataCart)) {
            foreach ($dataCart AS $key => $value) {
                $json['total_product']++;
                $json['total_amount'] = $json['total_amount'] + $value['amount'];
                
                $product_price	= 0;
                if(!empty($value['product']['product_price'])) {
                    if(!empty($value['product']['product_sale_percent'])) {
                        $product_price	= $value['product']['product_price'] - ($value['product']['product_sale_percent'] / 100 * $value['product']['product_price']);
                    } else {
                        $product_price	= $value['product']['product_price'];
                    }
                }
                
                $json['total_price'] = $json['total_price'] + ($product_price * $value['amount']);
            }
        }
        
        if($this->getRequest()->isXmlHttpRequest()) {
            if(!empty($json['total_price'])) {
                $json['total_price'] = number_format($json['total_price'], 0, '.', ',');
            }
            echo json_encode($json);
            return $this->response;
        } else {
            return $this->goRoute(array('route' => 'routePost/default', 'controller' => $this->_params['controller'], 'action' => 'cart'));
        }
    }
}
