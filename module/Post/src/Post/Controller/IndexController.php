<?php

namespace Post\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class IndexController extends ActionController {
    protected $_settings;
    
    public function init() {
        $this->setLayout('layout/default');
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 5;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Thiết lập options
        $this->_options['tableName'] = 'Post\Model\PostItemTable';
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray(), $this->params()->fromRoute());
        
        // Thiết lập Meta
        $this->_params['meta'] = array(
            'author'      => $this->_settings['General.Meta.Author']['value'],
            'image'       => $this->_settings['General.Meta.Image']['image'],
            'title'       => $this->_settings['General.Meta.Title']['value'],
            'description' => $this->_settings['General.Meta.Description']['description'],
            'keywords'    => $this->_settings['General.Meta.Keywords']['description'],
        );
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['language'] = $this->_language;
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function indexAction() {
        $this->setLayout('layout/home');
        
        return new ViewModel($this->_viewModel);
    }
    
    public function categoryAction() {
        // Lấy thông tin cấu hình module
        $settingPost = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'Post'), array('task' => 'cache-by-code')); // Sau này không cần
      
        // Lấy thông tin danh mục
        $category = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->getItem($this->_params['route'], array('task' => 'get-by-alias'));


        // Lấy thông tin page
        $page = $this->getServiceLocator()->get('Admin\Model\PageTable')->getItem($this->_params['route'], array('task' => 'get-by-alias'));
        
        // Lấy thông tin phần tử và danh mục
        $item = $this->getServiceLocator()->get('Post\Model\PostItemTable')->getItem($this->_params['route'], array('task' => 'get-by-alias'));
        $category_item = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->getItem(array('id' => $item['post_category_id']), array('task' => 'get-by-id'));

        if(!empty($page)) {
            if($page['alias'] == 'dang-ky') {
                $userInfo = new \ZendX\System\ContactInfo();
                if(!empty($userInfo->getContactInfo())) {
                    return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
                }
            }
	        // Thiết lập layout hiển thị
	        if(!empty($page['layout'])) {
	        	$this->setLayout('template/'. $page['layout']);
	        }

            // Thiết lập Meta
            $this->_params['meta']['image'] = $page['image_course'];
            $this->_params['meta']['title'] = $page['meta_title'] ? $page['meta_title'] : $page['name'];
            $this->_params['meta']['description'] = $page['meta_description'];
            $this->_params['meta']['keywords'] = $page['meta_keywords'];

	        $this->_viewModel['item']       = $page;
        } elseif (!empty($category) && empty($page) && empty($item)) {
            // Thiết lập layout hiển thị
            if(!empty($category['layout'])) {
                $this->setLayout('layout/'. $category['layout']);
            }
            
            // Lấy danh sách breadcrumb
            $breadcrumb = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->listItem($category, array('task' => 'list-breadcrumb'));
            $urlHelper  = $this->getServiceLocator()->get('viewhelpermanager')->get('url');
            foreach ($breadcrumb AS $key => $val) {
                $link   = $urlHelper('routePostCategory', array('alias' => $val['alias']), true);
                $name   = $val['name'];
                $this->_params['breadcrumb'][] = array(
                    'url' => $link,
                    'name' => $name
                );
            }
            
            // Lấy danh sách node trên nhánh
            $this->_params['branche'] = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->listItem($category, array('task' => 'list-branch'));
            
            // Thiết lập lại thông số phân trang khi đọc được cấu hình module
            $wordFirst = new \ZendX\Functions\WordFirst();
            $codeType = $wordFirst->upperFirst($category['type']);
            $this->_paginator['itemCountPerPage'] = $settingPost['Post.'. $codeType .'.ItemCountPerPage'] ? intval($settingPost['Post.'. $codeType .'.ItemCountPerPage']['value']) : 12;
            $this->_paginator['pageRange'] = $settingPost['Post.'. $codeType .'.PageRange'] ? intval($settingPost['Post.'. $codeType .'.PageRange']['value']) : 5;
            $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
            $this->_params['paginator'] = $this->_paginator;
            
            // Lấy danh sách dữ liệu
            $items = $this->getServiceLocator()->get('Post\Model\PostItemTable')->listItem($this->_params, array('task' => 'list-item'));
            $count = $this->getServiceLocator()->get('Post\Model\PostItemTable')->countItem($this->_params, array('task' => 'list-item'));
            
            // Update lượt view
            $ssFilter = new Container(__CLASS__);
            $ssCategory = $ssFilter->category;
            $ssCategory[$category['id']] = 1;
            $ssFilter->category = $ssCategory;
            if(empty($ssFilter->category[$category['id']])) {
                $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->saveItem(array('data' => array('id' => $category['id'])), array('task' => 'update-view'));
            }
            
            // Thiết lập Meta
            $this->_params['meta']['image'] = $category['image_medium'];
            $this->_params['meta']['title'] = $category['meta_title'] ? $category['meta_title'] : $category['name'];
            $this->_params['meta']['description'] = $category['meta_description'];
            $this->_params['meta']['keywords'] = $category['meta_keywords'];
            $this->_params['meta']['category_id'] = $category['id'];
            
            $this->_viewModel['category']   = $category;
            $this->_viewModel['items']      = $items;
            $this->_viewModel['count']      = $count;

        } elseif (!empty($item) || !empty($category_item) || $item['status'] != 0 || $category_item['status'] != 0) {
            if($item['type'] == 'ielts'){
                $item['list_exercise'] = $this->getServiceLocator()->get('Post\Model\ExerciseTable')->listItem(['data' => ['post_id' => $item['id']]], ['task' => 'list-item']);
            }
            // Lấy bài viết liên quan
            $itemInvolve    = $this->getServiceLocator()->get('Post\Model\PostItemTable')->listItem(array('item' => $item, 'category' => $category_item, 'settings' => $settingPost), array('task' => 'list-item-involve'));

            $item_df  = $this->getServiceLocator()->get('Post\Model\PostItemTable')->getItem($this->_params['route'], array('task' => 'get-by-alias'));
            // Thiết lập layout hiển thị
            if(!empty($item['layout'])) {
                $this->setLayout('layout/'. $item['layout']);
            } else if(!empty($category_item['layout'])) {
                $this->setLayout('layout/'. $category_item['layout']);
            } else {
                $this->setLayout('layout/default');
            }
            
            // Lấy danh sách breadcrumb
            $breadcrumb = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->listItem($category_item, array('task' => 'list-breadcrumb'));
            $urlHelper  = $this->getServiceLocator()->get('viewhelpermanager')->get('url');
            foreach ($breadcrumb AS $key => $val) {
                $link   = $urlHelper('routePostCategory', array('alias' => $val['alias'], 'index' => $val['index']), true);
                $name   = $val['name'];
                $this->_params['breadcrumb'][] = array(
                    'url' => $link,
                    'name' => $name
                );
            }

            $this->_params['breadcrumb'][] = array(
                'url' => null,
                'name' => $item['name']
            );

            // Update lượt view
            $ssFilter = new Container(__CLASS__);
            $ssItem = $ssFilter->item;
            $ssFilter->item = $ssItem;
            if(empty($ssFilter->item[$item['id']])) {
                $this->getServiceLocator()->get('Admin\Model\PostItemTable')->saveItem(array('data' => array('id' => $item['id'])), array('task' => 'update-view'));
            }
            
            // Thiết lập Meta
            $this->_params['meta']['image']       = $item['image_medium'];
            $this->_params['meta']['title']       = $item['meta_title'] ? $item['meta_title'] : $item['name'];
            $this->_params['meta']['description'] = $item['meta_description'];
            $this->_params['meta']['keywords']    = $item['meta_keywords'];
            $this->_params['meta']['category_id'] = $category_item['id'];
                
            $this->_viewModel['category']    = $category_item;
            $this->_viewModel['item']        = $item;
            $this->_viewModel['item_df']     = $item_df;
            $this->_viewModel['itemInvolve'] = $itemInvolve;
            $this->_viewModel['user']        = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        } else {
            return $this->goRoute(array('route' => 'routePost/default', 'controller' => 'notice', 'action' => 'no-data'));
        }
        //echo '<pre>'.json_encode($item).'</pre>';
        $this->_viewModel['params']     = $this->_params;
        return $this->_viewModel;
    }

    public function exerciseAction() {
        $layout = 'default';

        // Lấy thông tin phần tử và danh mục
        $post_info = $this->getServiceLocator()->get('Post\Model\PostItemTable')->getItem(['alias' => $this->_params['route']['post_alias']], array('task' => 'get-by-alias'));
        $item      = $this->getServiceLocator()->get('Post\Model\ExerciseTable')->getItem(['alias' => $this->_params['route']['alias'], 'post_id' => $post_info['id']], array('task' => 'get-by-alias'));
        $category  = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->getItem(array('id' => $post_info['post_category_id']), array('task' => 'get-by-id'));
        
        // Kiểm tra nếu không tồn tại thì đến trang thông báo
        if(empty($item) || empty($category) || $item['status'] == 0 || $category['status'] == 0) {
            $this->goRoute(array('route' => 'routePost/default', 'controller' => 'notice', 'action' => 'no-data'));
        }
        
        // Lấy bài viết liên quan
        $itemInvolve    = $this->getServiceLocator()->get('Post\Model\ExerciseTable')->listItem(array('data' => ['post_id' => $post_info['id']]), array('task' => 'list-involve', 'id' => $item['id']));

        // Thiết lập layout hiển thị
        if(!empty($item['layout'])) {
            $layout = $item['layout'];
        } else if(!empty($category['layout'])) {
            $layout = $category['layout'];
        }
        $this->setLayout('layout/'. $layout);
        
        // Lấy danh sách breadcrumb
        $breadcrumb = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->listItem($category, array('task' => 'list-breadcrumb'));
        $urlHelper  = $this->getServiceLocator()->get('viewhelpermanager')->get('url');
        foreach ($breadcrumb AS $key => $val) {
            $link   = $urlHelper('routePostCategory', array('alias' => $val['alias'], 'index' => $val['index']), true);
            $name   = $val['name'];
            $this->_params['breadcrumb'][] = array(
                'url' => $link,
                'name' => $name
            );
        }

        $this->_params['breadcrumb'][] = array(
            'url' => null,
            'name' => $post_info['name']
        );
        $this->_params['breadcrumb'][] = array(
            'url' => null,
            'name' => $item['name']
        );
        
        // Thiết lập Meta
        $this->_params['meta']['image'] = $item['image_medium'];
        $this->_params['meta']['title'] = $item['meta_title'] ? $item['meta_title'] : $item['name'];
        $this->_params['meta']['description'] = $item['meta_description'];
        $this->_params['meta']['keywords'] = $item['meta_keywords'];
        $this->_params['meta']['category_id'] = $category['id'];
            
        $this->_viewModel['category']    = $category;
        $this->_viewModel['post_info']   = $post_info;
        $this->_viewModel['item']        = $item;
        $this->_viewModel['itemInvolve'] = $itemInvolve;
        $this->_viewModel['user']        = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['params']      = $this->_params;
        return new ViewModel($this->_viewModel);
    }

    public function resultExerciseAction() {
        // Lấy thông tin phần tử và danh mục
        $item      = $this->getServiceLocator()->get('Post\Model\ExerciseTable')->getItem(['id' => $this->_params['data']['exercise_id']], array('task' => 'get-by-id'));
        $post_info = $this->getServiceLocator()->get('Post\Model\PostItemTable')->getItem(array('id' => $item['post_id']), array('task' => 'get-by-id'));
        $category  = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->getItem(array('id' => $post_info['post_category_id']), array('task' => 'get-by-id'));
            
        $this->_viewModel['item']      = $item;
        $this->_viewModel['post_info'] = $post_info;
        $this->_viewModel['user']      = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['params']    = $this->_params;
        $viewModel =  new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function searchAction() {
        // Lấy danh sách breadcrumb
        $this->_params['breadcrumb'][] = array(
            'url' => '',
            'name' => $this->_language['search'][$this->_language['language_active']]
        );
        
        // Thiết lập lại thông số phân trang khi đọc được cấu hình module
        $this->_paginator['itemCountPerPage'] = 10;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $_GET['page'] ? $_GET['page'] : 1;
        $this->_params['paginator'] = $this->_paginator;
        $this->_params['data']['keyword'] = $_GET['keyword'] ? $_GET['keyword'] : null;
        $this->_params['data']['type'] = $_GET['type'] ? $_GET['type'] : 'default';
        
        // Lấy danh sách dữ liệu
        $items = $this->getServiceLocator()->get('Post\Model\PostItemTable')->listItem($this->_params, array('task' => 'list-search'));
        $count = $this->getServiceLocator()->get('Post\Model\PostItemTable')->countItem($this->_params, array('task' => 'list-search'));
        
        // Thiết lập Meta
        $this->_params['meta']['title'] = $this->_language['search'][$this->_language['language_active']];
        
        $this->_viewModel['items']      = $items;
        $this->_viewModel['count']      = $count;
        $this->_viewModel['params']     = $this->_params;
        return $this->_viewModel;
    }
}
