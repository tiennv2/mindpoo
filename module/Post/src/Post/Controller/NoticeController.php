<?php

namespace Post\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;

class NoticeController extends ActionController {
    protected $_settings;
    
    public function init() {
        $this->setLayout('layout/default');
    
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
    
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 5;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
    
        // Thiết lập options
        $this->_options['tableName'] = 'Post\Model\PostItemTable';
    
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray(), $this->params()->fromRoute());
    
        // Thiết lập Meta
        $this->_params['meta'] = array(
            'author' => $this->_settings['General.Meta.Author']['value'],
            'image' => $this->_settings['General.Meta.Image']['image'],
            'title' => $this->_settings['General.Meta.Title']['value'],
            'description' => $this->_settings['General.Meta.Description']['description'],
            'keywords' => $this->_settings['General.Meta.Keywords']['description'],
        );
    
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['language'] = $this->_language;
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function noDataAction() {
        $this->setLayout('layout/notice');
        
        return new ViewModel($this->_viewModel);
    }
}
