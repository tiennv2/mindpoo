<?php

namespace Post\Controller;

use ZendX\Controller\ActionController;

class FormController extends ActionController {
    
    public function init() {
        $this->setLayout('template/default/full');
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function addAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->_params['form'] = $this->getServiceLocator()->get('Admin\Model\FormTable')->getItem(array('id' => $this->_params['data']['form_id'])); 
            $filter = new \Post\Filter\FormRegister($this->_params);
            
            $result = array();
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                // Check contact xem có tồn tại không
                $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('phone' => $this->_params['data']['phone']), array('task' => 'get-by-phone'));
                if(!empty($contact)) {
                    $this->_params['data']['contact_id'] = $contact['id'];
                } else {
                    $this->_params['data']['contact_id'] = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'add-item'));
                }
                
                $result['save'] = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->saveItem($this->_params, array('task' => 'public-add'));
            }
            
            echo json_encode($result);
        } else {
            $this->goRoute(array('route' => 'routePost/default', 'controller' => 'notice', 'action' => 'no-data'));
        }
        
        return $this->response;
    }
}
