<?php
namespace Post\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;

class PostItemTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $ssSystem  = New Container('system');
	            
	            $categories = array();
                foreach ($arrParam['branche'] AS $branche) {
                    $categories[] = $branche['id'];
                }
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> where -> in(TABLE_POST_ITEM .'.post_category_id', $categories);
                $select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
                
            })->current();
	    }
	    
	    if($options['task'] == 'list-search') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $ssSystem  = New Container('system');
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                
                if(!empty($arrParam['data']['keyword'])) {
                    $select -> where -> NEST
                                     -> like(TABLE_POST_ITEM .'.name', '%'. $arrParam['data']['keyword'] .'%')
                                     -> or
                                     -> like(TABLE_POST_ITEM .'.description', '%'. $arrParam['data']['keyword'] .'%')
                                     -> or
                                     -> like(TABLE_POST_ITEM .'.content', '%'. $arrParam['data']['keyword'] .'%')
                                     -> UNNEST;
                }
                
                $select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
                
            })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $ssSystem  = New Container('system');
			    
			    $paginator  = $arrParam['paginator'];
                $categories = array();
                foreach ($arrParam['branche'] AS $branche) {
                    $categories[] = $branche['id'];
                }
                
    			$select -> join( array('c' => TABLE_POST_CATEGORY), TABLE_POST_ITEM . '.post_category_id = c.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias'), $select::JOIN_INNER )
    			        -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage'])
    			        -> order(array(TABLE_POST_ITEM .'.ordering' => 'ASC', TABLE_POST_ITEM .'.created' => 'DESC'))
    			        -> where -> in(TABLE_POST_ITEM .'.post_category_id', $categories)
    			        -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);
    			$select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
    		});
		}
		
		if($options['task'] == 'list-search') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $ssSystem  = New Container('system');
			    $paginator  = $arrParam['paginator'];
                
    			$select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias'), $select::JOIN_INNER )
    			        -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage'])
    			        -> order(array(TABLE_POST_ITEM .'.ordering' => 'ASC', TABLE_POST_ITEM .'.created' => 'DESC'))
    			        -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);
    			
    			if(!empty($arrParam['data']['q'])) {
                    $select -> where -> NEST
                                     -> like(TABLE_POST_ITEM .'.name', '%'. $arrParam['data']['q'] .'%')
                                     -> or
                                     -> like(TABLE_POST_ITEM .'.description', '%'. $arrParam['data']['q'] .'%')
                                     -> UNNEST;
    			}
    			if(!empty($arrParam['data']['type'])) {
                    $select -> where -> equalTo(TABLE_POST_CATEGORY .'.type', $arrParam['data']['type']);
    			}
    			
    			$select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
    		});
		}
		
		if($options['task'] == 'list-item-box') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $ssSystem  = New Container('system');
		        
		        $limit = $arrParam['limit'] ? $arrParam['limit'] : 1;
		        
	            $select -> join( array('c' => TABLE_POST_CATEGORY), TABLE_POST_ITEM . '.post_category_id = c.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias'), $select::JOIN_INNER )
		                -> limit($limit)
	                    -> where -> equalTo('c.type', $arrParam['type'])
	                    -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);
	            $select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
	            
	            if(!empty($arrParam['box']) && $arrParam['box'] != 'box_new') {
	                $select -> where -> equalTo(TABLE_POST_ITEM .'.'. $arrParam['box'], 1);
				}
				
				if(!empty($arrParam['category'])) {
					if (is_array($arrParam['category'])) {
						/*
						 * Lấy bài viết từ nhiều thư mục khác nhau
						 * Cấu hình danh mục trong: Cấu hình hệ thống
						 * Author: DEVNGUYEN
						 */
						$strLiteral = '';
						foreach($arrParam['category'] as $category){
						    $strLiteral .= "post_category_id = '$category' OR ";
						}
						$strLiteral = substr($strLiteral, 0, -4);  //Xoá cụm từ ' OR' cuối cùng từ query string
						$select -> where -> NEST
						    -> literal($strLiteral)
						    -> UNNEST;
					} else {
						$select -> where -> equalTo(TABLE_POST_ITEM .'.post_category_id', $arrParam['category']);
					}
	            }
	            
	            if(!empty($arrParam['random'] == true)){
	                $select -> order(new \Zend\Db\Sql\Expression('RAND()'));
	            	
	            } elseif($arrParam['order']) {
	                $select -> order($arrParam['order']);
	            }else{
	                $select -> order(array(TABLE_POST_ITEM .'.ordering' => 'ASC', TABLE_POST_ITEM .'.created' => 'DESC'));
	            }
	            if(!empty($arrParam['post_type'])) {
	                $select -> where -> equalTo(TABLE_POST_ITEM .'.type', $arrParam['post_type']);
				}

	            $select -> order(array(TABLE_POST_ITEM .'.created' => 'DESC'));
		    });
		}
			
		if($options['task'] == 'list-category-box') {
		    $postCategoryTable = $this->getServiceLocator()->get('Post\Model\PostCategoryTable');
		    $categories = $postCategoryTable->listItem($arrParam, $options);
		    $result = array();
		    	
		    if(!empty($categories)) {
    		    foreach ($categories AS $category) {
    		        $childs = $category['childs'];
    		        $ids = array();
    		        
    		        if(count($childs) > 0) {
    		            foreach ($childs AS $child) {
    		                $ids[] = $child['id'];
    		            }
    		        }

    		        $category['items'] = $this->tableGateway->select(function (Select $select) use ($arrParam, $options, $ids){
    		            $limit = $arrParam['limit'] ? $arrParam['limit'] : 1;
    		            
    		            
    		            
    		            if(!empty($arrParam['random'] == true)){
    		                $select -> order(new \Zend\Db\Sql\Expression('RAND()'));
    		            } else {
    		                $select -> order(array(TABLE_POST_ITEM .'.ordering' => 'ASC', TABLE_POST_ITEM .'.created' => 'DESC'));
    		            }
    		        })->toArray();
    		        $result[] = $category;
    		    }
		    }
		}
		
		if($options['task'] == 'list-item-involve') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $item     = $arrParam['item'];
		        $category = $arrParam['category'];
		        $settings = $arrParam['settings'];
		        $limit    = $settings['Post.'. ucfirst($category['type']) .'.ItemInvolve'] ? intval($settings['Post.'. ucfirst($category['type']) .'.ItemInvolve']['value']) : 4;
		        
		        $select -> join( array('c' => TABLE_POST_CATEGORY), TABLE_POST_ITEM . '.post_category_id = c.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias'), $select::JOIN_INNER )
        		        -> limit($limit)
        		        -> order(array(TABLE_POST_ITEM .'.ordering' => 'ASC', TABLE_POST_ITEM .'.created' => 'DESC'))
        		        -> where -> equalTo(TABLE_POST_ITEM .'.post_category_id', $item['post_category_id'])
        		                 -> lessThan(TABLE_POST_ITEM .'.created', $item['created'])
		                         -> equalTo(TABLE_POST_ITEM .'.status', 1);
		    })->toArray();
		}
		
		if($options['task'] == 'list-item-box-by-view') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $ssSystem  = New Container('system');
		        
		        $limit = $arrParam['limit'] ? $arrParam['limit'] : 1;
		        
	            $select -> join( array('c' => TABLE_POST_CATEGORY), TABLE_POST_ITEM . '.post_category_id = c.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias'), $select::JOIN_INNER )
		                -> limit($limit)
	                    -> where -> equalTo('c.type', $arrParam['type'])
	                    -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);
	            $select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
	            
	            if(!empty($arrParam['box'])) {
	                $select -> where -> equalTo(TABLE_POST_ITEM .'.'. $arrParam['box'], 1);
				}
	            
	            $select -> order(array(TABLE_POST_ITEM .'.created' => 'ASC'));
		    });
		}

		if($options['task'] == 'get-by-index') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select->where->equalTo('index', $arrParam['index']);
    		})->current();
		}

		if($options['task'] == 'list-all') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $ssSystem  = New Container('system');
			    $arrData = $arrParam['data'];
    			$select -> order(array('ordering' => 'ASC', 'created' => 'DESC'))
    			        -> where -> equalTo('status', 1);
    			if($arrData['limit']){
    				$select -> limit($arrData['limit']);
    			}
    			if($arrData['post_category_id']){
    				$select -> where -> equalTo('post_category_id', $arrData['post_category_id']);
    			}
    			if($arrData['box_hot']){
    				$select -> where -> equalTo('box_hot', $arrData['box_hot']);
    			}
    		});
		}

		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null || $options['task'] == 'get-by-id') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		if($options['task'] == 'get-by-index') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('index', $arrParam['index']);
    		})->current();
		}
	
		if($options['task'] == 'get-by-alias') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('alias', $arrParam['alias']);
                $select->where->equalTo('status', 1);
    		})->current();
		}

		if($options['task'] == 'get-by-shortcode') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select->where->equalTo('shortcode', $arrParam['shortcode']);
    		})->current();
		}
	
		return $result;
	}
}