<?php
namespace Post\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;

class ExerciseTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $arrData = $arrParam['data'];
	            $ssSystem  = New Container('system');
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                if($arrData['post_id']){
                    $select -> where -> equalTo('post_id', $arrData['post_id']);
                }
            })->current();
	    }
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $arrData = $arrParam['data'];
			    $ssSystem  = New Container('system');
                
    			$select -> join( array('p' => TABLE_POST_ITEM), TABLE_EXERCISE . '.post_id = p.id', array('post_id' => 'id', 'post_index' => 'index', 'post_name' => 'name', 'post_alias' => 'alias'), $select::JOIN_INNER )
    			        -> order(array(TABLE_EXERCISE .'.ordering' => 'ASC', TABLE_EXERCISE .'.created' => 'DESC'))
    			        -> where -> equalTo(TABLE_EXERCISE .'.status', 1);
                if($arrData['post_id']){
                    $select -> where -> equalTo('post_id', $arrData['post_id']);
                }
    		});
		}

        if($options['task'] == 'list-involve') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $arrData = $arrParam['data'];
                $ssSystem  = New Container('system');
                
                $select -> join( array('p' => TABLE_POST_ITEM), TABLE_EXERCISE . '.post_id = p.id', array('post_id' => 'id', 'post_index' => 'index', 'post_name' => 'name', 'post_alias' => 'alias'), $select::JOIN_INNER )
                        -> order(array(TABLE_EXERCISE .'.ordering' => 'ASC', TABLE_EXERCISE .'.created' => 'DESC'))
                        -> where -> equalTo(TABLE_EXERCISE .'.status', 1);
                if($arrData['post_id']){
                    $select -> where -> equalTo('post_id', $arrData['post_id']);
                }
                $select -> where -> notEqualTo(TABLE_EXERCISE .'.id', $options['id']);
            });
        }
		return $result->toArray();
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null || $options['task'] == 'get-by-id') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		if($options['task'] == 'get-by-alias') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('alias', $arrParam['alias']);
                if($arrParam['post_id']){
                    $select->where->equalTo('post_id', $arrParam['post_id']);
                }
    		})->current();
		}
		return $result;
	}
}