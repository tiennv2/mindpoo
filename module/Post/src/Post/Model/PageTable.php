<?php
namespace Post\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PageTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null || $options['task'] == 'get-by-id') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		if($options['task'] == 'get-by-index') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('index', $arrParam['index']);
    		})->current();
		}
	
		if($options['task'] == 'get-by-alias') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('alias', $arrParam['alias']);
    		})->current();
		}
	
		return $result;
	}
}