<?php
namespace Post\Filter;

class FormRegister {
	protected $_error;
	
	public function __construct($data = null){
	    $dataForm = $data['form'];
	    $dataParam = $data['data'];
	    
	    if(empty($dataForm)) {
	        $this->_error['form_id'] = 'Không tìm thấy form đăng ký';
	    } else {
	        $configFields = new \ZendX\Functions\Form($dataForm['fields']);
	        $configFields = $configFields->getSetting();
	        
    	    $valid = new \Zend\Validator\NotEmpty();
    	    foreach ($configFields AS $config) {
    	        if($config['require'] == 'true') {
        	        if (!$valid->isValid($dataParam[$config['name']])) {
        	            $this->_error[$config['name']] = "Giá trị này bắt buộc phải nhập";
        	        }
    	        }
    	    }

    	    // Validate dữ liệu liên hệ
    	    if (!$valid->isValid($dataParam['name'])) {
    	    	$this->_error['name'] = "Giá trị này bắt buộc phải nhập";
    	    }
    	    if (!$valid->isValid($dataParam['phone'])) {
    	    	$this->_error['phone'] = "Giá trị này bắt buộc phải nhập";
    	    }
		    if (!$valid->isValid($dataParam['email'])) {
		    	$this->_error['email'] = "Giá trị này bắt buộc phải nhập";
		    }
    	    if(!empty($dataParam['email'])) {
    	    	$validEmail = new \Zend\Validator\EmailAddress();
    	    	if (!$validEmail->isValid(trim($dataParam['email']))) {
    	    		$this->_error['email'] = "Email không đúng định dạng";
    	    	}
    	    }
    	    if (!$valid->isValid($dataParam['location_city_id'])) {
    	    	$this->_error['location_city_id'] = "Giá trị này bắt buộc phải nhập";
    	    }
	    }
	}
	
	public function getError() {
	    return $this->_error;
	} 
}