<?php

namespace Post;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Module {

	public function onBootstrap(MvcEvent $e) {
		$eventManager        = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
	
		$adapter = $e->getApplication()->getServiceManager()->get('dbConfig');
		GlobalAdapterFeature::setStaticAdapter($adapter);
	}
	
	public function getConfig() {
        return array_merge(
            include __DIR__ . '/config/module.config.php',
            include __DIR__ . '/config/router.config.php'
        );
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig(){
        return array(
            'factories'	=> array(
                'Post\Model\PostCategoryTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_POST_CATEGORY, $adapter, null);
                    return new \Post\Model\PostCategoryTable($tableGateway);
                },
                'Post\Model\PostItemTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_POST_ITEM, $adapter, null);
                    return new \Post\Model\PostItemTable($tableGateway);
                },
                'Post\Model\ExerciseTable'  => function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_EXERCISE, $adapter, null);
                    return new \Post\Model\ExerciseTable($tableGateway);
                },
                'Post\Model\PageTable'	=> function ($sm) {
                    $adapter = $sm->get('dbConfig'); 
                    $tableGateway = new TableGateway(TABLE_PAGE, $adapter, null);
                    return new \Post\Model\PageTable($tableGateway);
                },
            ),
        );
    }
    
    public function getViewHelperConfig() {
        return array(
            'invokables' => array(
                'blkMenu'               => '\Block\Menu',
                'blkBreadcrumb'         => '\Block\Breadcrumb',
                'blkDocument'           => '\Block\Document',
                'blkPostItemBox'        => '\Block\PostItemBox',
                'blkPostCategoryBox'    => '\Block\PostCategoryBox',
            )
        );
    }

    public function getFormElementConfig() {
        return array(
            'factories' => array(
            )
        );
    }
}
