CKEDITOR.plugins.add('addshortcode',
{
    init: function (editor) {
        var pluginName = 'addshortcode';
        editor.ui.addButton('AddShortCode',
            {
                label: 'Thêm shortcode',
                command: 'AddShortCode',
                icon: CKEDITOR.plugins.getPath('addshortcode') + 'icon_shortcode.png',
            });
        var cmd = editor.addCommand('AddShortCode', {
        	exec: function(edt) {
                console.log(edt);
		        let content = edt.getData();
                var post_ids = prompt("Nhập mã shortcode các bài viết, cách nhau bởi dấu phẩy. VD: 11,12");
                if(post_ids){
		            content += '[post='+ post_ids +']';
		            edt.setData(content);
                }
		    }
        });
    },
});
