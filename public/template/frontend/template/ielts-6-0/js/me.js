/**
 * Desciption: Form đặt câu hỏi
 */
function formRegister(form) {
	var idForm 	= "#" + form;
	var urlAjax = "/post/form/add";
	var textBtn = $(idForm + ' .btnSuccess .btn').val();
		
	$.ajax({
		type: "POST",
		url: urlAjax,
		data: $(idForm).serialize(),
		dataType: "json",
		cache: false,
		beforeSend: function() {
			$(idForm + ' .form-group').removeClass('has-error');
			$(idForm + ' .alert').remove();
			$(idForm + ' .btnSuccess .btn').val('Đang gửi thông tin...').attr('disabled', 'disabled');
			$('body').append('<div class="page-loading"><div class="loader"></div></div>');
		},
		success: function(result){
			if(result.error) {
				if(result.error.form_id) {
					$(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">'+ result.error.form_id +'</div>');
				} else {
					$.each(result.error, function(key, value) {
						$(idForm + ' #input-'+ key).addClass('has-error');
					})
					$(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
				}
			} else {
				$(idForm + ' .btnSuccess').prepend('<div class="alert alert-success">Thông tin của bạn đã được gửi thành công</div>');
				$(idForm + ' .form-control').val('');
			}
			
			$(idForm + ' .btnSuccess .btn').val(textBtn).removeAttr('disabled', 'disabled');
			$('body .page-loading').remove();
		}
	});
}

$('#box_noiDung .title').click(function() {
    var parent = $(this).parent();
    $('.content', parent).slideToggle();
    /* $(this).css('padding','50px 24px 20px 60px'); */
});
if ($(".fancybox-button").size() > 0) {
    $(".fancybox-button").fancybox({
        groupAttr: 'data-rel',
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: true,
        helpers: {
            title: {
                type: 'inside'
            }
        }
    });

    $('.fancybox-video').fancybox({
        type: 'iframe'
    });
}


// Menu sticky
function stickyMenu() {
    var boxMenu = '#box_menu';
    var topPage = $(window).scrollTop();
    var topFix = 72;
    if(topPage >= topFix) {
        $(boxMenu).addClass('sticky');
        $(boxMenu).css({'box-shadow' : '0 2px 5px 0 rgba(0,0,0,0.1)'});
    } else {
        $(boxMenu).removeClass('sticky');
        $(boxMenu).removeAttr('style');
    }
}

// Menu Mobile
function close_memu_mobile() {
    if($('.box_header .menu.mobile').size() > 0) {
        $('.box_header .menu.mobile').addClass('hidden-xs hidden-sm');
        
    }
    if($('.menu_mobile_bg').size() > 0) {
        $('.menu_mobile_bg').remove();
    }
}
$('.menu_mobile a').click(function() {
    $('.box_header .menu').removeClass('hidden-xs hidden-sm').addClass('mobile');
    $('.box_header').append('<div class="menu_mobile_bg" onclick="javascript:close_memu_mobile();"></div>');
});

// Popup video
function play_video(url) {
    var html = '';
    html += '<div class="popup_video" onclick="close_popup()">';
        html += '<div class="popup_content">';
            html += '<div class="embed-responsive embed-responsive-16by9">';
                html += '<iframe class="embed-responsive-item" src="'+ url +'" frameborder="0" allowfullscreen></iframe>';
            html += '</div>';
            html += '<a href="javascript:void(0)" onclick="close_popup()" class="popup_close"><i class="fa fa-times"></i></a>';
        html += '</div>';
    html += '</div>';
    $('body').append(html);
}

// Close Popup video
function close_popup() {
    $('.popup_video').remove();
}

$(document).ready(function () {
	$(window).scroll(function () {
	    stickyMenu();
	});
	
    //Chi nhap so
    $(".auto_init").keypress(function (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });
    
    // Menu go box
    $('.go_box').click(function() {
        var box = $(this).attr('data-box');
        if($('#' + box).length) {
            var top_scroll = 0;
            top_scroll = $("#" + box).offset().top;
            $('html,body').animate({
                scrollTop: top_scroll - 60
            }, 'slow');
        }
        $('.box_header .menu.mobile').addClass('hidden-xs hidden-sm');
        $('.go_box').removeClass('active');
        $(this).addClass('active');
        $('.menu_mobile_bg').remove();
    });

    // Popup video, image
    if ($(".fancybox-button").size() > 0) {
        $(".fancybox-button").fancybox({
            groupAttr: 'data-rel',
            prevEffect: 'none',
            nextEffect: 'none',
            closeBtn: true,
            helpers: {
                title: {
                    type: 'inside'
                }
            }
        });

        $('.fancybox-video').fancybox({
            type: 'iframe'
        });
    }
});

$(document).ready(function(e){
    $('header').scrollToFixed();
    $('.testi-slide').owlCarousel({
        loop:true,
        margin:30,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav: false,
                dots: true,
            },
            550:{
                items:2,
                nav:true,
                dots: false,
            },
            992:{
                items:3,
                nav:true,
                dots: false,
            }
        }
    });

    $('.member-slide').owlCarousel({
        center            :false,
        loop              :true,
        margin            :30,
        autoplay          :false,
        autoplayTimeout   :4000,
        autoplayHoverPause:true,
        responsiveClass   :true,
        autoHeight        :true,
        responsive        :{
            0:{
                items:1,
                nav  : false,
                dots : true,
            },
            768:{
                items:2,
                nav:true,
                dots: false,
            },
            992:{
                items:3,
                nav:true,
                dots: false,
            }
        }
    });

    $('.teacher-slide').owlCarousel({
        center            :false,
        loop              :true,
        margin            :30,
        autoplay          :true,
        autoplayTimeout   :4000,
        autoplayHoverPause:true,
        responsiveClass   :true,
        responsive        :{
            0:{
                items:1,
                nav  : false,
                dots : true,
            },
            768:{
                items:2,
                nav:true,
                dots: false,
            },
            992:{
                items:4,
                nav:true,
                dots: false,
            }
        }
    });

    // YTB
    $('.item-ytb').click(function(){
        $('body .item-ytb').each(function(){
            $(this).find('.item-ytb-content iframe').remove();
        });
        let ytb_id = $(this).data('ytb');
        $(this).find('.item-ytb-content').append('<iframe height="270" src="https://www.youtube.com/embed/'+ ytb_id +'?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
    });

    // Load địa điểm học
    $.ajax({
        type: 'POST',
        url: 'https://langmaster.vn/erp/api/sale-document/list',
        crossDomain: true,
        data: {
            where:{
                code: 'training-location'
            }
        },
        dataType: 'json',
        success: function(responseData, textStatus, jqXHR) {
            var htmlSelect = '';
            $.each(responseData, function(key, val) {
                if(val['address']) {
                    htmlSelect += '<option value="'+ val['id'] +'">'+ val['address'] +'</option>';
                }
            });
            $('select[name="training_location_id"]').append(htmlSelect);
        }
    });


    //Countdown
    // Set the date we're counting down to
    var countDownDate = new Date().getTime() + 77 * 60 * 60 * 1000 - 27 * 60 * 1000;
    // Update the count down every 1 second
    var x = setInterval(function() {
        // Get today's date and time
        var now = new Date().getTime();
        
        // Find the distance between now and the count down date
        var distance = countDownDate - now;
        
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
        $('.count-date .item-count-number').html(days);
        $('.count-hour .item-count-number').html(hours);
        $('.count-minute .item-count-number').html(minutes);
        $('.count-second .item-count-number').html(seconds);
        if (distance < 0) {
            clearInterval(x);
        }
    }, 1000);
    $('.count-down').css("opacity", "1");
});

function formRegisterCRM(idForm) {
    $.ajax({
        type: 'POST',
        url: 'https://langmaster.vn/erp/api/sale-form-data/add',
        crossDomain: true,
        data: $(idForm).serialize(),
        dataType: 'json',
        beforeSend: function() {
            $(idForm + ' .form-group').removeClass('has-error');
            $(idForm + ' .alert').remove();
            $('body').append('<div class="page-loading"><div class="loader"></div></div>');
        },
        success: function(responseData, textStatus, jqXHR) {
            var result = responseData;
            if(result.error) {
                if(result.error.form_id) {
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">'+ result.error.form_id +'</div>');
                } else {
                    $.each(result.error, function(key, value) {
                        $(idForm + ' .input-'+ key).addClass('has-error');
                    })
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
                }
            } else {
                $(idForm + ' .btnSuccess').prepend('<div class="alert alert-success">Thông tin của bạn đã được gửi thành công</div>');
                $(idForm + ' .form-control').not(idForm + ' .form-control.my_btn').val('');
            }
            
            $('body .page-loading').remove();
        },
    });
}

/* GoBox */
function goBox(element) {
    var top_scroll = 0;
    top_scroll = $(element).offset().top;
    $('html,body').animate({
        scrollTop: top_scroll - 64
    }, 'slow');
}