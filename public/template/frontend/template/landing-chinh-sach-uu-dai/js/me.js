/**
 * Desciption: Form đặt câu hỏi
 */
function formRegister(form) {
	var idForm 	= "#" + form;
	var urlAjax = "/dang-ky-form-data";
	var textBtn = $(idForm + ' .btnSuccess .btn').val();
		
	$.ajax({
		type: "POST",
		url: urlAjax,
		data: $(idForm).serialize(),
		dataType: "json",
		cache: false,
		beforeSend: function() {
			$(idForm + ' .form-group').removeClass('has-error');
			$(idForm + ' .alert').remove();
			$(idForm + ' .btnSuccess .btn').val('Đang gửi thông tin...').attr('disabled', 'disabled');
			$('body').append('<div class="page-loading"><div class="loader"></div></div>');
		},
		success: function(result){
			if(result.error) {
				if(result.error.form_id) {
					$(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">'+ result.error.form_id +'</div>');
				} else {
					$.each(result.error, function(key, value) {
						$(idForm + ' #input-'+ key).addClass('has-error');
					})
					$(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
				}
			} else {
				$(idForm + ' .btnSuccess').prepend('<div class="alert alert-success">Thông tin của bạn đã được gửi thành công</div>');
				$(idForm + ' .form-control').val('');
			}
			
			$(idForm + ' .btnSuccess .btn').val(textBtn).removeAttr('disabled', 'disabled');
			$('body .page-loading').remove();
		}
	});
}

$(document).ready(function () {
    //Chi nhap so
    $(".auto_init").keypress(function (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });
});