$(document).ready(function(e){
    $('.member-slide').owlCarousel({
        center            : true,
        loop              :true,
        margin            :30,
        autoplay          :true,
        autoplayTimeout   :4000,
        autoplayHoverPause:true,
        responsiveClass   :true,
        responsive        :{
            0:{
                items:1,
                nav  : false,
                dots : true,
            },
            768:{
                items:2,
                nav:true,
                dots: false,
            },
            992:{
                items:3,
                nav:true,
                dots: false,
            }
        }
    });

    $('.teacher-slide').owlCarousel({
        center            :false,
        loop              :true,
        margin            :30,
        autoplay          :true,
        autoplayTimeout   :4000,
        autoplayHoverPause:true,
        responsiveClass   :true,
        responsive        :{
            0:{
                items:1,
                nav  : false,
                dots : true,
            },
            768:{
                items:2,
                nav:true,
                dots: false,
            },
            992:{
                items:4,
                nav:true,
                dots: false,
            }
        }
    });
});

$(document).ready(function(e){
    $(".chosen-select").chosen();
});