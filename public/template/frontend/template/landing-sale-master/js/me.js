$(document).ready(function(e){
	$('.box_menu').scrollToFixed();

	if ($(".fancybox-button").size() > 0) {            
	    $(".fancybox-button").fancybox({
	        groupAttr: 'data-rel',
	        prevEffect: 'none',
	        nextEffect: 'none',
	        closeBtn: true,
	        helpers: {
	            title: {
	                type: 'inside'
	            }
	        }
	    });

	    $('.fancybox-video').fancybox({
	        type: 'iframe'
	    });
	}

	$('.langers-slide').owlCarousel({
		loop : true,
		nav  : true,
		dots : false,
		items: 1,
		responsiveClass:true,
	    responsive:{
	        0:{
	            nav  : false,
				dots : true,
	        },
	        767:{
	            nav  : true,
				dots : false,
	        },
	    }
	});

	var id_menu = '#box_menu';

	var h_menu = [];
	$(id_menu + ' .collapsed').click(function() {
		$(id_menu + ' .navbar-collapse').addClass('open');
		$(id_menu + ' .navbar-collapse-background').css({'left': 0});
		
	});

	$(id_menu + ' .navbar-collapse-background').click(function() {
		$(id_menu + ' .navbar-collapse').removeClass('open');
		$(id_menu + ' .navbar-collapse-background').css({'left': '-100%'});
	});

	$(id_menu + ' .navbar-collapse a').click(function() {
		$(id_menu + ' .navbar-collapse li').removeClass('active');
		$(this).parent().addClass('active');
		
		var target = $(this).attr('data-target');
		goBox(target);

		if($(id_menu + ' .navbar-collapse.open').size() > 0) {
			$(id_menu + ' .navbar-collapse').removeClass('open');
			$(id_menu + ' .navbar-collapse-background').css({'left': '-100%'});
		}
	});

});
/* GoBox */
function goBox(id_box) {
	var top_scroll = 0;
    top_scroll = $("#" + id_box).offset().top;
    $('html,body').animate({
        scrollTop: top_scroll -70
    }, 'slow');
}