<?php
    $url_current    = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . '://'. $_SERVER[HTTP_HOST] .''. $_SERVER[REQUEST_URI] .'';
    $strpos_page    = strpos($url_current, 'page');
    $canonical_url  = explode('page', $url_current);

    echo $this  -> headTitle(!empty($strpos_page) ? 'Trang ' . str_replace('/', '', $canonical_url[1]) . ' - ' . $this->arrParams['meta']['title'] : $this->arrParams['meta']['title']);
    
    echo $this  -> headMeta()
                -> appendHttpEquiv('X-UA-Compatible', 'IE=edge')
                -> setCharset('utf-8')
                -> appendName('viewport', 'width=device-width, initial-scale=1.0')
                -> appendName('author', $this->arrParams['meta']['author'])
                -> appendName('description', $this->arrParams['meta']['description'])
                -> appendName('keywords', $this->arrParams['meta']['keywords'])
                -> appendProperty('og:type', 'website')
                -> appendProperty('og:title', $this->arrParams['meta']['title'])
                -> appendProperty('og:description', $this->arrParams['meta']['description'])
                -> appendProperty('og:image', $this->arrParams['meta']['image'])
                -> appendName('geo.region', 'VN-HN')
                -> appendName('geo.placename', 'Ha Noi')
                -> appendName('geo.position', '21;106');

    // Canonical link & Alternate link
    echo $this  -> headLink(array('rel' => 'alternate', 'href' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . '://'. $_SERVER[HTTP_HOST] .'', 'hreflang' => 'vi-vn'), 'PREPEND');
    
    if (!empty($strpos_page)) {
        echo $this  -> headLink(array('rel' => 'canonical', 'href' => $canonical_url[0]), 'PREPEND');
    } else {
        echo $this  -> headLink(array('rel' => 'canonical', 'href' => substr($url_current, 0, strrpos($url_current, '?')) ? substr($url_current, 0, strrpos($url_current, '?')) : $url_current), 'PREPEND');
    }
	
	// CSS
	echo $this  -> headLink(array('rel' => 'icon', 'href' => $setting['General.System.Favicon']['image'], 'type' => 'image/x-icon'), 'PREPEND')
                -> appendStylesheet($this->arrParams['template']['urlPlugin'] . '/fancybox/source/jquery.fancybox.css')
                -> appendStylesheet($this->arrParams['template']['urlPlugin'] . '/fancybox/source/jquery.fancybox-thumbs.css')
                -> appendStylesheet($this->arrParams['template']['urlPlugin'] . '/video-js/video-js.css')
                -> appendStylesheet($this->arrParams['template']['urlCss'] . '/bootstrap.min.css')
                -> appendStylesheet($this->arrParams['template']['urlCss'] . '/fontawesome-all.min.css')
                -> appendStylesheet($this->arrParams['template']['urlCss'] . '/bxslider.css')
                -> appendStylesheet($this->arrParams['template']['urlCss'] . '/owl.carousel.min.css')
                -> appendStylesheet($this->arrParams['template']['urlCss'] . '/owl.theme.default.min.css')
                -> appendStylesheet($this->arrParams['template']['urlCss'] . '/livechat.css?v='. date('dmYHis'))
                -> appendStylesheet($this->arrParams['template']['urlCss'] . '/fonts.css?v='. date('dmYHis'))
                -> appendStylesheet($this->arrParams['template']['urlCss'] . '/user.css?v='. date('dmYHis'))
                -> appendStylesheet($this->arrParams['template']['urlCss'] . '/style.css?v='. date('dmYHis'))
                -> appendStylesheet($this->arrParams['template']['urlCss'] . '/online.css?v='. date('dmYHis'));
?>
