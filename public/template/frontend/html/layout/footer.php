<a href="javascript:;" onclick="goTop();" class="btn-gotop"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></a>

<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/jquery.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/bootstrap.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/scrolltofixed.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/fancybox/source/jquery.fancybox.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/fancybox/source/jquery.fancybox-thumbs.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/bxslider/jquery.bxslider.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/hover-dropdown.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/numeric.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/recorder.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/video-js/video.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/owl.carousel.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/app.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/me.js';?>"></script>

<script type="text/javascript">
	$(document).ready(function() {
		App.init();
	});

	function goTop() {
		$('html,body').animate({
            scrollTop: 0
        }, 'slow');
	}
</script>
<?php echo $this->headScript(); ?>