<?php include_once $this->arrParams['template']['pathHtml'] . '/box/trial-register.php';?>
<section class="box-footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="footer_social">
                        <p class="title"><a href="/"><img src="<?php echo $setting['General.System.Logo']['image'];?>" alt="Công Ty Giáo Dục MindPoo"></a></p>
                        <p class="text"><span>MindPoo</span> - Cho Đi Giá Trị Để Nhận Lại Giá Trị</p>
                        <p class="network">
                            Social network
                            <a href="<?php echo $setting['General.System.Facebook']['value'];?>" target="_blank"><span><img alt="facebook" src="<?php echo $this->arrParams['template']['urlImg'] . '/social-1.png'; ?>"></span></a>
                            <a href="<?php echo $setting['General.System.Google']['value'];?>" target="_blank"><span><img alt="google+" src="<?php echo $this->arrParams['template']['urlImg'] . '/social-2.png'; ?>"></span></a>
                            <a href="<?php echo $setting['General.System.Youtube']['value'];?>" target="_blank"><span><img alt="youtube" src="<?php echo $this->arrParams['template']['urlImg'] . '/social-3.png'; ?>"></span></a>
                        </p>
                    </div>
                </div>
                
                <div class="col-sm-5 text-justify">
                    <div class="receive">
                        <h3 class="title hidden-xs">Đăng ký nhận tin</h3>
                        <p class="text">Để lại thông tin để nhận được những tài liệu hay về phát triển bản thân</p>
                        <form id="frmDangkyNhanTin" name="frmDangkyNhanTin" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input-name">
                                        <input required type="text" class="form-control" id="name" name="name" placeholder="Họ Tên">
                                    </div>                     
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="input-phone">
                                        <input required type="text" class="form-control" id="phone" name="phone" placeholder="Điện thoại">
                                    </div>                     
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="input-email">
                                        <input required type="text" class="form-control" id="email" name="email" placeholder="Email">
                                    </div>                     
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group" id="input-location_city_id">
                                        <select name="location_city_id" class="form-control">
                                            <option value="" selected="selected">- Tỉnh thành bạn đang sinh sống -</option>
                                            <?php
                                            $location_city = json_decode(LOCATION_CITY, true);
                                            foreach ($location_city as $key => $val) {
                                                echo '<option value="'. $key .'">'. $val .'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="btnSuccess">
                                        <input type="hidden" value="155819945236c7u33z2978" name="form_id">
                                        <input type="hidden" name="source_group_id" value="1475114382-d0w9-n376-0b3i-6rh2qnkjl815">
                                        <input type="hidden" name="source_channel_id" value="1475118253-f69m-4h09-9q8p-12538c0gfl04">
                                        <input type="hidden" name="source" value="<?php echo $_SERVER['REQUEST_URI'];?>">
                                        <input type="hidden" name="source_detail" value="<?php echo $_COOKIE['HTTP_REFERER'];?>">
                                        <input class="form-control my_btn" id="btn-gui" type="button" value="ĐĂNG KÝ NHẬN TIN" onclick="formRegister('frmDangkyNhanTin');" rel="nofollow" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="fanpage">
                        <h3 class="title">MINDPOO TRÊN FACEBOOK</h3>
                        <div class="fb-page" data-href="<?php echo $setting['General.System.Facebook']['value'];?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="livechat" id="livechat">
    <a href="<?php echo $setting['General.System.FacebookPageChat']['value']; ?>" target="_blank"><i class="fas fa-comment-alt-lines"></i>Chat để nhận tư vấn</a>
</div>