<section class="box-trial-register">
    <form action="#" id="frmRegister" name="frmRegisterTuVan" method="POST" class="">
        <div class="container container-box">
            <div class="register-box">
                <div class="box-content">
                    <div class="box-title title-one">Đăng ký nhận tư vấn về sản phẩm</div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="form-group" id="input-name">
                                <input name="name" type="text" class="form-control" placeholder="Họ và tên">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="form-group" id="input-phone">
                                <input name="phone" type="text" class="form-control" placeholder="Điện thoại">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="form-group" id="input-email">
                                <input name="email" type="text" class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-sm-6">
                            <div class="form-group" id="input-location_city_id">
                                <select name="location_city_id" class="form-control">
                                    <option value="" selected="selected">- Chọn tỉnh thành sinh sống -</option>
                                    <?php
                                    $location_city = json_decode(LOCATION_CITY, true);
                                    foreach ($location_city as $key => $val) {
                                        echo '<option value="'. $key .'">'. $val .'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="btnSuccess"> </div>
                            <input type="hidden" value="1570735562403g50i3a8n4" name="form_id">
                            <input type="hidden" name="source" value="<?php echo $_SERVER['REQUEST_URI'];?>">
                            <input type="hidden" name="source_detail" value="<?php echo $_COOKIE['HTTP_REFERER'];?>">
                            <div class="btn_register"><a href="javascript:;" onclick="formRegister('frmRegister')" rel="nofollow">ĐĂNG KÝ NHẬN TƯ VẤN</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>