<?php
	$userInfo = new \ZendX\System\ContactInfo();
	$userInfo = $userInfo->getContactInfo();
?>
<section class="box_top">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <span class="hotline">
                	<i class="fas fa-phone-alt"></i>
                	<?php
                    	$hotline = explode(' - ', $setting['General.System.Hotline']['value']);
                    	foreach ($hotline AS $key => $val) {
                    		if($key == 0) {
                    			echo '<a href="tel:'. str_replace('.', '', $val) .'">'. $val .'</a>';
                    		} else {
                    			echo ' - <a href="tel:'. str_replace('.', '', $val) .'">'. $val .'</a>';
                    		}
                    	}

                	?>
                    
                </span>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="top-right-header">
                    <div class="social">
                        <a href="<?php echo $setting['General.System.Facebook']['value'];?>" target="_blank" class="item"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                        <a href="<?php echo $setting['General.System.Youtube']['value'];?>" target="_blank" class="item"><i class="far fa-play-circle"></i></a>
                        <a href="<?php echo $setting['General.System.Google']['value'];?>" target="_blank" class="item"><i class="fab fa-google-plus-g"></i></i></a>
                    </div>
                    <div class="user">
                        <ul class="nav navbar-nav">
                            <?php
                            if(!empty($userInfo)) { 
                                ?>
                                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'index'));?>"><?php echo $userInfo['name'];?></a></li>
                                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'logout'));?>">Đăng xuất</a></li>
                                <?php
                            } else { 
                                ?>
                                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'login'));?>">Đăng nhập</a></li>
                                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'register'));?>">Đăng ký</a></li>
                                <?php
                            } 
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo $this->blkMenu($this->arrParams, array('code' => 'Menu.Main', 'cache' => true), 'main'); ?>
