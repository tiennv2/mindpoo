<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml            : true,
            version          : 'v6.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<?php if (0){?><!-- Your customer chat code -->
<div class="fb-customerchat hidden-xs" attribution=setup_tool page_id="<?php echo $setting['General.System.FacebookPageID']['value']; ?>" logged_in_greeting="<?php echo $setting['General.System.FacebookChatTextLogin']['value']; ?>" logged_out_greeting="<?php echo $setting['General.System.FacebookChatTextLogout']['value']; ?>"></div>
<?php } ?>