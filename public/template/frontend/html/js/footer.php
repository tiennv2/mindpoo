<?php
// Set Cookie để lưu vết người dùng khi vào website từ 1 link khác
if(strpos($_SERVER['HTTP_REFERER'], DOMAIN) < 0) {
    setcookie("HTTP_REFERER", $_SERVER['HTTP_REFERER'], time()+30*24*60*60);
}
?>
<script>
    function formRegisterCRM(idForm) {
        var type_form = $('[name="type_form"]').val();
        $.ajax({
            type: 'POST',
            url: '/post/form/add',
            crossDomain: true,
            data: $(idForm).serialize(),
            dataType: 'json',
            beforeSend: function() {
                $(idForm + ' .form-group').removeClass('has-error');
                $(idForm + ' .alert').remove();
                $('body').append('<div class="page-loading"><div class="loader"></div></div>');
            },
            success: function(responseData, textStatus, jqXHR) {
                var result = responseData;
                if(result.error) {
                    if(result.error.form_id) {
                        $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">'+ result.error.form_id +'</div>');
                    } else {
                        $.each(result.error, function(key, value) {
                            $(idForm + ' #input-'+ key).addClass('has-error');
                        })
                        $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
                    }
                } else {
                    let text_alert = 'Thông tin của bạn đã được gửi thành công';
                    if(type_form == 'sign_up'){
                        text_alert = 'Đăng ký thành công! Vui lòng nhấn <a href="<?php echo DOMAIN; ?>/user/index/login">vào đây</a> để đăng nhập!';
                    }
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-success">'+ text_alert +'</div>');
                    $(idForm + ' .form-control').not(idForm + ' .form-control.my_btn').val('');
                }
                $('body .page-loading').remove();
            },
        });
    }

    function formDataCRM(idForm) {
        var add_success = false;
        $.ajax({
            type: 'POST',
            url: '<?php echo DOMAIN_ERP; ?>/api/sale-form-data/add',
            crossDomain: true,
            data: $(idForm).serialize(),
            dataType: 'json',
            async: false,
            beforeSend: function() {
                $(idForm + ' .form-group .alert-error').remove();
                $(idForm + ' .form-group').removeClass('has-error');
                $(idForm + ' .alert').remove();
                $('body').append('<div class="page-loading"><div class="loader"></div></div>');
            },
            success: function(responseData, textStatus, jqXHR) {
                var result = responseData;
                if(result.error) {
                    if(result.error.form_id) {
                        $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">'+ result.error.form_id +'</div>');
                    } else {
                        $.each(result.error, function(key, value) {
                            $(idForm + ' #input-'+ key).addClass('has-error');
                            $(idForm + ' #input-'+ key).append('<div class="alert-error">'+ value +'</div>');
                        })
                        $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
                    }
                } else {
                    let text_alert = 'Thông tin của bạn đã được gửi thành công';
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-success">'+ text_alert +'</div>');
                    $(idForm + ' .form-control').not(idForm + ' .form-control.my_btn').val('');
                    add_success =  true;
                }
                $('body .page-loading').remove();
            },
        });
        return add_success;
    }

    function formRegister(form) {
    var idForm  = "#" + form;
    var urlAjax = "/post/form/add";
    var textBtn = $(idForm + ' .btnSuccess .btn').val();
        
    $.ajax({
        type: "POST",
        url: urlAjax,
        data: $(idForm).serialize(),
        dataType: "json",
        cache: false,
        beforeSend: function() {
            $(idForm + ' .form-group').removeClass('has-error');
            $(idForm + ' .alert').remove();
            $(idForm + ' .btnSuccess .btn').val('Đang gửi thông tin...').attr('disabled', 'disabled');
            $('body').append('<div class="page-loading"><div class="loader"></div></div>');
        },
        success: function(result){
            if(result.error) {
                if(result.error.form_id) {
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">'+ result.error.form_id +'</div>');
                } else {
                    $.each(result.error, function(key, value) {
                        $(idForm + ' #input-'+ key).addClass('has-error');
                    })
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
                }
            } else {
                $(idForm + ' .btnSuccess').prepend('<div class="alert alert-success">Thông tin của bạn đã được gửi thành công.</div>');
                $(idForm + ' .form-control').val('');
            }
            
            $(idForm + ' .btnSuccess .btn').val(textBtn).removeAttr('disabled', 'disabled');
            $('body .page-loading').remove();
        }
    });
}

</script>