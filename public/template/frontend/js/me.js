var modulePost = 'post';

/**
 * Desciption: Form đăng ký
 */
function formRegister(form) {
	var idForm 	= "#" + form;
	var urlAjax = "/post/form/add";
	var textBtn = $(idForm + ' .btnSuccess .btn').val();
		
	$.ajax({
		type: "POST",
		url: urlAjax,
		data: $(idForm).serialize(),
		dataType: "json",
		cache: false,
		beforeSend: function() {
			$('body').append('<div class="page-loading"><div class="loader"></div></div>');
			$(idForm + ' .form-group').removeClass('has-error');
			$(idForm + ' .alert').remove();
		},
		success: function(result){
			if(result.error) {
				$.each(result.error, function(key, value) {
					$(idForm + ' #input-'+ key).addClass('has-error');
				})
				$(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
			} else {
				$(idForm + ' .btnSuccess').prepend('<div class="alert alert-success">Thông tin của bạn đã được gửi thành công</div>');
				$(idForm + ' .form-control').val('');
			}
			
			$('body .page-loading').remove();
		}
	});
}

/**
 * Desciption: Hiển thị số lượng sản phẩm trong giỏ hàng
 */
function viewCart(){
	$.ajax({
		type: "POST",
		url: '/post/product/report',
		dataType: "json",
		cache: false,
		beforeSend: function() {
			 
		},
		success: function(result){
			//var xhtml = '';
			//xhtml = '<a href="/post/product/cart" class="box_reportCart"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="total">'+ result.total_product +'</span></a>';
			//$('body').append(xhtml);
			$('#box_cart .cart_amount').html(result.total_amount);
			$('#box_cart .cart_price').html(result.total_price + ' VNĐ');
		}
	});
}
viewCart();

/**
 * Desciption: Play video
 */
function playVideo(data) {
	$(data).html('<iframe width="560" height="315" src="https://www.youtube.com/embed/'+ $(data).attr('data-id') +'?rel=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>')
}

/**
 * Desciption: Đếm số ký tự
 */
function countLeft(field, count, max) {
	if ($(field).val().length > max)
		$(field).val($(field).val().substring(0, max));
	else
		$(count + ' .value').html(max - $(field).val().length);
}

/**
 * Desciption: Thêm sản phẩm vào giỏ hàng
 */
function productCart(){
	var ajaxUrl	= '/' + moduleName + '/product/product-cart';
}

/**
 * Desciption: Tìm kiếm
 */
function popupSearch(){
	var xhtml = '<div class="popup_search">'+ 
			        '<div class="popup_wrapper">'+ 
					    '<div class="popup_content">'+ 
					        '<form action="/search">'+ 
					            '<div class="form-group">'+ 
					                '<div class="input-group">'+ 
					                    '<input name="keyword" type="text" class="form-control" placeholder="Tìm kiếm" value="">'+ 
					                    '<div class="input-group-addon"><button type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button></div>'+ 
					                '</div>'+ 
					            '</div>'+ 
					        '</form>'+ 
					    '</div>'+ 
					    '<a href="javascript:;" class="popup_close" onclick="popupSearchClose()">Close <i class="fa fa-close" aria-hidden="true"></i></a>'+ 
					'</div>'+ 
				'</div>';
	
	$('.popup_search').remove();
	$('body').append(xhtml);
}

function popupSearchClose() {
	$('.popup_search').remove();
}


$(document).ready(function() {
	/* Sidebar */
	$('.item-sidebar ul li.has-childs>a>i').click(function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		$(this).parents('li.has-childs').children('ul').slideToggle('fast');
	});

	$('.box_menuMain').scrollToFixed();
	if($(window).width() > 767){
		$('.fix-sidebar').scrollToFixed({
			marginTop: $('.box_menuMain').outerHeight(true) + 10,
	        limit: function() {
	            var limit = 0;
	            limit = $('.box-trial-register').offset().top - $(this).outerHeight(true) - 65;
	            return limit;
	        },
	        zIndex: 99
		});
	}
});

/* GoBox */
function goBox(element) {
    var top_scroll = 0;
    top_scroll = $(element).offset().top;
    $('html,body').animate({
        scrollTop: top_scroll - 80
    }, 'slow');
}

// Popup video
function playVideoPopup(url) {
    var html = '';
    html += '<div class="popup-video" onclick="closePopup()">';
        html += '<div class="popup-content">';
            html += '<div class="embed-responsive embed-responsive-16by9">';
                html += '<iframe class="embed-responsive-item" src="'+ url +'" frameborder="0" allowfullscreen></iframe>';
            html += '</div>';
            html += '<a href="javascript:void(0)" onclick="closePopup()" class="popup-close"><i class="fa fa-times"></i></a>';
        html += '</div>';
    html += '</div>';
    $('body').append(html);
}

// Close Popup video
function closePopup() {
    $('.popup-video').remove();
}

// Popup image
function openPopupImg(url, name = null) {
    var html = '';
    var html = `<div class="popup-basic popup-image" onclick="closePopupImg()">
    				<div class="popup-content">
    					<img src="`+ url +`" alt="` + name + `" />
    					<a href="javascript:void(0)" onclick="closePopupImg()" class="popup-close"><i class="fa fa-times"></i></a>
    				</div>
    			</div>`;
    $('body').append(html);
}

// Close Popup image
function closePopupImg() {
    $('.popup-image').remove();
}

function formatNumber(number){
    return number ? number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') : 0;
}