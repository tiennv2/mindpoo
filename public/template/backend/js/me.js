/**
 * Author: NamNV
 * Desciption: Thực hiện submit form
 * 
 * @params: Mảng các tham số được truyền vào
 * @options: Mảng các tùy chọn hoặc phân vùng làm việc
 */
function submitForm(url) {
	if(url != ""){
		$(formAdmin).attr('action', url);
	}
	jQuery(formAdmin).submit();
}

/**
 * Author: NamNV
 * Desciption: Sự kiện nhấn enter ở ô input
 */
function keywordEnter() {
	jQuery(formAdmin + ' input[name="filter_keyword"]').keypress(function (event) {
		if(event.charCode == 13) {
			event.preventDefault();
			jQuery(formAdmin).submit();
		}
	});
}


/**
 * Author: NamNV
 * Desciption: Thực hiện sắp xếp danh sách
 * 
 * @orderColumn: Cột sẽ sắp xếp
 * @orderBy: Chiều sắp xếp
 */
function sortList(orderColumn, orderBy) {
	
	jQuery(formAdmin + ' input[name="order_by"]').val(orderColumn);
	jQuery(formAdmin + ' input[name="order"]').val(orderBy);
	
	jQuery(formAdmin).submit();
}

/**
 * Author: NamNV
 * Desciption: Thay đổi trạng thái
 * 
 * @type: Phân loại thực hiện là nhiều hay một phần tử: item/multi
 * @option: Là Id nếu type = item, load trạng thái 0/1 nếu type = multi
 */
function changeStatus(type, option) {
	var itemStatus = option;
	
	if(type == 'item') {
		jQuery('#tr_' + option + ' .checkboxes').attr('checked', true);
		jQuery('#tr_' + option + ' .checkboxes').parents('tr').addClass("active");
		
		itemStatus = jQuery('#tr_' + option + ' .btn-status').attr('data-status');
	}
	
	var itemId = [];
	jQuery(formAdmin + ' .checkboxes').each(function () {
		checked = jQuery(this).is(":checked");
        if (checked) {
        	itemId.push(jQuery(this).val());
        }
    });
	
	if(itemId.length > 0) {
		var actionForm  = jQuery(formAdmin).attr('action').split('/');
    	var ajaxUrl 	= '/' + actionForm[1] + '/' + actionForm[2] + '/status';
    	if(actionForm[0] != '') {
    		ajaxUrl 	= '/' + actionForm[0] + '/' + actionForm[1] + '/status';
    	}
		var classRemove = (itemStatus == 0) ? 'default' : 'green';
		var classAdd 	= (itemStatus == 0) ? 'green' : 'default';
		var statusNew 	= (itemStatus == 0) ? 1 : 0;
		
		jQuery.ajax({
			url: ajaxUrl,
			type: 'POST',
			data: {
				cid: itemId,
				status: itemStatus
			},
			beforeSend: function() {
				pageLoading('loading', '.table-scrollable');
			},
			success: function(result) {
				if(result == 'no-access') {
					xToastr('error', xMessage['no-access'], '');
				} else {
					for(var i = 0; i < itemId.length; i++) {
						jQuery('#tr_' + itemId[i] + ' .btn-status').removeClass(classRemove).addClass(classAdd);
						jQuery('#tr_' + itemId[i] + ' .btn-status').attr({'onclick': 'javascript:changeStatus(\'item\', \''+ itemId[i] +'\');', 'data-status': statusNew});
						jQuery('#tr_' + itemId[i] + ' .checkboxes').attr('checked', false);
						jQuery('#tr_' + itemId[i] + ' .checkboxes').parents('tr').removeClass("active");
					}
					
					jQuery(formAdmin + ' .group-checkable').attr('checked', false);
				}
				
				pageLoading('close', '.table-scrollable');
			},
			complete: function(){
			}
		});
	} else {
		xToastr('error', xMessage['no-checked'], '');
	}
}

/**
 * Author: NamNV
 * Desciption: Popup ajax đến action
 * 
 * @ajaxUrl: Link đến Action thực hiện
 * @option: Đối tượng mở rộng
 */
function popupAction(ajaxUrl, option) {
	Form.extendedModals('ajax');
	
	var $modal 	= $('#ajax-modal');
	
	$('body').modalmanager('loading');
	$modal.load(ajaxUrl, option, function(){
		$modal.modal();
	});
	
	$modal.on('click', '.save-close', function(){
		$.ajax({
			url: ajaxUrl,
			type: 'POST',
			data: $modal.find('form').serialize(),
			beforeSend: function() {
				$modal.modal('loading');
				$modal.find('.btn').addClass('disabled');
			},
			success: function(result) {
				if(result == 'success') {
					$modal.modal('hide');
					location.reload();
				} else {
					$modal.modal('loading');
					$modal.find('.btn').removeClass('disabled');
					$modal.find('.modal-body').html(result);
					reloadScript();
				}
			},
			error: function (request, status, error) {
				console.log(error);
			}
		});
	});
	
	$modal.on('shown.bs.modal', function (e) {
		reloadScript();
	});
	$modal.on('hidden.bs.modal', function (e) {
		$modal.html('');
	});
}

/**
 * Author: NamNV
 * Desciption: Thêm quyền truy cập
 * 
 * @row: Phần tử muốn thêm
 */
function insertPermission(row) {
	var ajaxUrl = '/' + moduleName + '/' + controllerName + '/add';
	var id 		= jQuery(row).attr('data-id');
	
	jQuery.ajax({
		url: ajaxUrl,
		type: 'POST',
		data: {
			name: $('#row_' + id + ' #name').val(),
			module: $('#row_' + id + ' #module').val(),
			controller: $('#row_' + id + ' #controller').val(),
			action: $('#row_' + id + ' #action').val(),
			status: 1,
			ordering: 255
		},
		beforeSend: function() {
			pageLoading('loading', '.page-container');
		},
		success: function(result) {
			if(result == 'no-access') {
				xToastr('error', xMessage['no-access'], '');
			} else if(result == 'record-exists'){
				xToastr('error', xMessage['record-exists'], '');
			} else {
				$('#row_' + id).remove(),
				xToastr('success', xMessage['success'], '');
			}
			
			pageLoading('close', '.page-container');
		},
		complete: function(){
		}
	});
}

/**
 * Author: NamNV
 * Desciption: Xóa phần tử
 * 
 * @type: Phân loại thực hiện là nhiều hay một phần tử: item/multi
 * @option: Là Id nếu type = item, ngược lại all - xóa tất cả các phần tử được chọn
 */
function deleteItem(type, option) {
	var itemId = [];
	
	if(type == 'item') {
		jQuery('#tr_' + option + ' .checkboxes').attr('checked', true);
		jQuery('#tr_' + option + ' .checkboxes').parents('tr').addClass("active");
		
		itemId.push(option);
	} else {
		jQuery(formAdmin + ' .checkboxes').each(function () {
			checked = jQuery(this).is(":checked");
	        if (checked) {
	        	itemId.push(jQuery(this).val());
	        }
	    });
	}
	
	if(itemId.length > 0) {
		Form.extendedModals('confirm');
		var $modal 	= $('#confirm-modal');
		
		$modal.modal();
		$modal.find('.modal-body').html("Nếu xóa phần tử sẽ không thể khôi phục lại. Bạn có chắc chắn muốn xóa?");
	    $modal.on('click', '.confirm', function(){
	    	var actionForm  = jQuery(formAdmin).attr('action').split('/');
	    	var ajaxUrl 	= '/' + actionForm[1] + '/' + actionForm[2] + '/delete';
	    	if(actionForm[0] != '') {
	    		ajaxUrl 	= '/' + actionForm[0] + '/' + actionForm[1] + '/delete';
	    	}
			submitForm(ajaxUrl);
		});
	    
		$modal.on('hidden.bs.modal', function (e) {
			$modal.html('');
		});
	} else {
		xToastr('error', xMessage['no-checked'], '');
	}
}

/**
 * Author: NamNV
 * Desciption: Sắp xếp list
 */
function changeOrdering() {
	var itemId = [];
	
	jQuery(formAdmin + ' .checkboxes').each(function () {
		checked = jQuery(this).is(":checked");
        if (checked) {
        	itemId.push(jQuery(this).val());
        }
    });
	
	if(itemId.length > 0) {
		var ajaxUrl = jQuery(formAdmin).attr('action').replace('/filter', '/ordering');
		submitForm(ajaxUrl);
	} else {
		xToastr('error', xMessage['no-checked'], '');
	}
}

/**
 * Author: NamNV
 * Desciption: Di chuyển Node
 */
function moveNode(id, type) {
	var ajaxUrl = '/' + moduleName + '/' + controllerName + '/move';
	
	jQuery.ajax({
		url: ajaxUrl,
		type: 'POST',
		data: {
			'move-id': id,
			'move-type': type
		},
		beforeSend: function() {
			pageLoading('loading', '.page-container');
		},
		success: function(result) {
			if(result == 'no-access') {
				xToastr('error', xMessage['no-access'], '');
			} else if(result == 'record-exists'){
				xToastr('error', xMessage['record-exists'], '');
			} else {
				$('#row_' + id).remove(),
				xToastr('success', xMessage['success'], '');
			}
			
			pageLoading('close', '.page-container');
			location.reload();
		},
		complete: function(){
		}
	});
}

/**
 * Author: NamNV
 * Desciption: Submit form
 * 
 * @type: Loại: Lưu, Lưu & Mới,Lưu & Đóng
 */
function controlSubmitForm(type) {
	var itemId = [];
	if(type != undefined) {
		if(jQuery('input[name="control-action"]').size() > 0) {
			jQuery('input[name="control-action"]').val(type);
		} else {
			jQuery(formAdmin).append('<input type="hidden" name="control-action" value="'+ type +'">');
		}
	}
	
	var ajaxUrl 	= $(formAdmin).attr('action');
	submitForm(ajaxUrl);
}

/**
 * Author: NamNV
 * Desciption: Submit form & Confirm
 * 
 * @type: Loại: Lưu, Lưu & Mới,Lưu & Đóng
 * @message: Nội dung thông báo xác nhận comfirm
 */
function controlSubmitFormConfirm(type, message) {
	Form.extendedModals('confirm');
	var $modal 	= $('#confirm-modal');
	
	$modal.modal();
	$modal.find('.modal-body').html(message);
    $modal.on('click', '.confirm', function(){
    	var itemId = [];
    	if(type != undefined) {
    		if(jQuery('input[name="control-action"]').size() > 0) {
    			jQuery('input[name="control-action"]').val(type);
    		} else {
    			jQuery(formAdmin).append('<input type="hidden" name="control-action" value="'+ type +'">');
    		}
    	}
    	
    	var ajaxUrl = $(formAdmin).attr('action');
    	submitForm(ajaxUrl);
	});
    
	$modal.on('hidden.bs.modal', function (e) {
		$modal.html('');
	});
}

/**
 * Author: NamNV
 * Desciption: Submit form & Stack
 * 
 * @type: Loại: Lưu, Lưu & Mới,Lưu & Đóng
 * @message: Nội dung thông báo xác nhận comfirm
 */
function controlSubmitFormStack(type, message) {
	Form.extendedModals('stack');
	var $modal 	= $('#stack-modal');
	
	$modal.modal();
	$modal.find('.control-label').html(message);
	$modal.on('click', '.save', function(){
		if($modal.find('input[name="input-stack"]').val() && $modal.find('input[name="input-stack"]').val() != '') {
			var itemId = [];
			if(type != undefined) {
				if(jQuery('input[name="control-action"]').size() > 0) {
					jQuery('input[name="control-action"]').val(type);
				} else {
					jQuery(formAdmin).append('<input type="hidden" name="control-action" value="'+ type +'">');
				}
			}
			
			jQuery(formAdmin).append('<input type="hidden" name="input-stack" value="'+ $modal.find('input[name="input-stack"]').val() +'">');
			
			var ajaxUrl = $(formAdmin).attr('action');
			submitForm(ajaxUrl);
		} else {
			xToastr('error', 'Vui lòng nhập ' + message, '');
		}
	});
	
	$modal.on('hidden.bs.modal', function (e) {
		$modal.html('');
	});
}

/**
 * Author: NamNV
 * Desciption: Check all list table
 */
function checkAll() {
	jQuery(formAdmin + ' .table .group-checkable').change(function () {
        var set = jQuery(this).attr("data-set");
        var checked = jQuery(this).is(":checked");
        jQuery(set).each(function () {
            if (checked) {
                $(this).attr("checked", true);
                $(this).parents('tr').addClass("active");
            } else {
                $(this).attr("checked", false);
                $(this).parents('tr').removeClass("active");
            }                    
        });
    });

    jQuery(formAdmin + ' .table tbody tr .checkboxes').change(function(){
         $(this).parents('tr').toggleClass("active");
    });
}

/**
 * Author: NamNV
 * Desciption: Check all group
 */
function checkAllGroup(group_id) {
	jQuery('#' + group_id + ' .group-check').toggleClass('check');
	
	var set = '#' + group_id + ' .checkboxes';
	var group_check = jQuery('#' + group_id + ' .group-check.check').size();
	
    jQuery(set).each(function () {
    	$(this).attr("checked", true);
        if (group_check) {
        	jQuery(this).attr("checked", true);
        } else {
        	jQuery(this).attr("checked", false);
        }
    });
}

/**
 * Author: NamNV
 * Desciption: Change Paginator
 */
function changePagination() {
	jQuery('#pagination_option').change(function () {
		submitForm();
	});
}

/**
 * Author: NamNV
 * Desciption: Chọn file upload với textbox
 */
function openFile(element, type) {
    window.KCFinder = {
        callBack: function(url) {
        	jQuery(formAdmin + ' input[name="'+ element +'"]').val(url);
        	jQuery(formAdmin + ' #view_'+ element).attr({'href': url}).css('display', 'inline-block');
        	jQuery(formAdmin + ' #remove_'+ element).css('display', 'inline-block');
            window.KCFinder = null;
        }
    };
    window.open('/public/scripts/kcfinder/browse.php?type='+ type +'&lng=vi', 'kcfinder_textbox',
        'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
        'resizable=1, scrollbars=0, width=1000, height=600'
    );
}

/**
 * Author: NamNV
 * Desciption: Xóa trắng ô file upload với textbox
 */
function removeFile(field) {
	jQuery(formAdmin + ' input[name="'+ field +'"]').val('');
	jQuery(formAdmin + ' #view_'+ field).attr({'href': ''}).css('display', 'none');
	jQuery(formAdmin + ' #remove_'+ field).css('display', 'none');
}

/**
 * Author: NamNV
 * Desciption: Tạo chuỗi random
 */
function ramdomString(lenght) {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < lenght; i++ ) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

/**
 * Author: NamNV
 * Desciption: Hide/Show button up/down table-tree
 * Note: Developer
 */
function buttonListTree() {
	if(jQuery(formAdmin + ' .table-tree').size() > 0) {
		var elements = jQuery(formAdmin + ' .table-tree').find('.spinner-input');
		elements.each(function(index, el) {
			var levelCurrent 	= jQuery(this).attr('data-level');
			var valueCurrent 	= jQuery(this).val();
            var levelNext 		= elements.eq(index + 1).attr('data-level');
            var valueNext 		= elements.eq(index + 1).val();
            
            if(valueCurrent == 1) {
            	jQuery(this).parent().addClass('hide-up');
            }
            
            if((levelCurrent != levelNext) && (valueCurrent >= valueNext)) {
            	jQuery(this).parent().addClass('hide-down');
            }
		});
	}
}

/**
 * Author: NamNV
 * Desciption: Page loading
 */
function pageLoading(type, el) {
	if(!el) {
		el = 'body';
	}
	
	if(type == 'loading') {
		App.blockUI(el);
	} else if(type == 'close'){
		App.unblockUI(el);
	}
}

/**
 * Author: NamNV
 * Desciption: Cập nhật nội dung vào modal
 */
function modalMessage(modal, message) {
	var xhtml = '<div class="modal-header">' +
					'<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
					'<h4 class="modal-title">Thông báo từ hệ thống</h4>' +
				'</div>' +
				'<div class="modal-body">'+ message +'</div>' +
				'<div class="modal-footer">' +
					'<button type="button" data-dismiss="modal" class="btn btn-default">Đóng</button>' +
				'</div>'; 
	$(modal).html(xhtml);
}

/**
 * Author: NamNV
 * Desciption: Tạo đường dẫn tính
 */
function createAlias(source, target) {
	var data = $(source).val();
	//if(!$(target).val() || $(target).val() == '' || $('input[name="id"]').val() == '') {
		data= data.toLowerCase();
		
		data = data.replace(/à|ả|ã|á|ạ|ă|ằ|ẳ|ẵ|ắ|ặ|â|ầ|ẩ|ẫ|ấ|ậ/g,"a");
		data = data.replace(/è|ẻ|ẽ|é|ẹ|ê|ề|ể|ễ|ế|ệ/g,"e");
		data = data.replace(/ì|ỉ|ĩ|í|ị/g,"i");
		data = data.replace(/ò|ỏ|õ|ó|ọ|ô|ồ|ổ|ỗ|ố|ộ|ơ|ờ|ở|ỡ|ớ|ợ/g,"o");
		data = data.replace(/ù|ủ|ũ|ú|ụ|ư|ừ|ử|ữ|ứ|ự/g,"u");
		data = data.replace(/ỳ|ỷ|ỹ|ý/g,"y");
		data = data.replace(/đ/g,"d");
		data = data.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\“|\”|\&|\#|\[|\]|~/g,"-");
		data = data.replace(/-+-/g,"-");
		data = data.replace(/^\-+|\-+$/g,"");
		
		$(target).val(data);
	//}
}


/**
 * Author: NamNV
 * Desciption: Toastr
 */
function xToastr(type, msg, title) {
	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "positionClass": "toast-top-right",
	  "onclick": null,
	  "showDuration": 300,
	  "hideDuration": 300,
	  "timeOut": 4000,
	  "extendedTimeOut": 1000,
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}

    toastr[type](msg, title); // Wire up an event handler to a button in the toast, if it exists
}

/**
 * Author: NamNV
 * Desciption: Popup Preview Image/Video/Iframe
 * Source: fancyapps.com
 */
function fancyboxPreview(className) {
	jQuery("." + className).fancybox({
		openEffect	: 'elastic',
		closeEffect	: 'elastic'
	});
}

/**
 * Author: NamNV
 * Desciption: Chặn copy nội dung
 */
function noCopy() {
	$('body').css({
		'-webkit-touch-callout': 'none',
		'-webkit-user-select': 'none',
		'-moz-user-select': 'none',
		'-ms-user-select': 'none',
		'-o-user-select': 'none',
		'user-select': 'none'
	});
	document.onselectstart = new Function ("return false");
    if (window.sidebar){
        document.onmousedown = false;
        document.onclick = true;
    }
}

/**
 * Author: NamNV
 * Desciption: Chặn chuột phải
 */
function noRightMouse() {
	$(document).bind("contextmenu",function(e){
		e.preventDefault();
	});
}

/**
 * Author: NamNV
 * Desciption: sự kiện click tr vào table
 */
function trClick() {
	jQuery(".table-hover tr").click(function(){
		if (window.event.ctrlKey) {
			if(jQuery(this).hasClass('active')) {
				jQuery(this).removeClass('active');
			} else {
				jQuery(this).addClass('active');
			}
	    } else {
			jQuery(".table-hover tr").removeClass('active');
			jQuery(this).addClass('active');
	    }
	});
}

/**
 * Author: ThaoTT
 * Desciption: Cấu hình báo cáo biểu đồ
 * @arrChart: Mảng dữ liệu truyền vào
 * @nameChart: Id element của chart
 * @typeChart: Kiểu biểu đồ: column, line, bar
 */
function reportChart(arrChart, nameChart, typeChart){
	Highcharts.setOptions({
		lang: {
			thousandsSep: ','
		}
	});
	var dataChart = JSON.parse(arrChart);
	
	switch (typeChart){
		case "column":
		case "bar":
			var myChart = Highcharts.chart(nameChart, {
		        chart: {
		            type: typeChart
		        },
		        title: { text: '' },
		        yAxis: { title: { text: '' } },
		        legend: {
		            enabled: true
		        },
		        plotOptions: {
		            series: {
		                borderWidth: 0,
		                dataLabels: {
		                    enabled: true,
	                    	formatter: function () {
	                    		return Highcharts.numberFormat(this.y,0);
	                    	}
		                },
	                    maxPointWidth: 50,
	                    groupPadding: 0.1
		            }
		        },
		        tooltip: {
		            pointFormat: '<b>{point.y}</b>'
		        },
		        xAxis: {
		            categories: dataChart.categories
		        },
		        series: dataChart.series
		    });
			if(typeChart == 'column') {
				myChart.setSize(null, 450, false);
			}
			if(typeChart == 'bar') {
				var heightColumn = 50;
				if(dataChart.series.length > 1) {
					heightColumn = (dataChart.series.length - 1) * 50;
				}
				myChart.setSize(null, dataChart.categories.length * heightColumn, false);
			}
			break;
		case "pie":
			var myChartPie =  Highcharts.chart(nameChart, {
		        chart: {
		            type: typeChart
		        },
		        title: {
		            text: ''
		        },
		        tooltip: {
		            pointFormat: '<b>{point.y:,.1f}</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                    }
		                }
		            }
		        },
		        series: [{data : dataChart}]
		    });
			break;
	}
}

/**
 * Author: NamNV
 * Desciption: Function replace all trong javascript
 */
String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
};

/**
 * Author: NamNV
 * Desciption: Nạp lại các thư viện script khi sử dụng ajax, modal
 */
function reloadScript() {
	App.init();
  	Form.init();
}

/**
 * Author: NamNV
 * Desciption: Sự kiện chung toàn hệ thống
 */
function init() {}

/**
 * Author: NamNV
 * Desciption: Nạp tất cả các function cần khởi tạo khi chạy ứng dụng
 */
jQuery(document).ready(function() {
	init();
	//noCopy();
	//noRightMouse();
	trClick();
	keywordEnter();
	checkAll();
	changePagination();
});

/**
 * Author: TienNV
 * Desciption: Nạp tất cả các function cần khởi tạo khi chạy ứng dụng
 */
//Tính khoảng cách chiều cao phần nội dung
function resizeDocument() {
    var h_window = $(window).height();
    var h_content = h_window - $('.header.navbar').outerHeight() - $('.page-content-wrapper .page-control').outerHeight() - $('.page-content-wrapper .alert').outerHeight() - $('.page-content-wrapper .page-filter').outerHeight() - $('.paginations').outerHeight() - 10;
    $('#table-manager .table-scrollable').css({'height': h_content + 'px'});
    $( window ).resize(function() {
        var h_window = $(window).height();
        var h_content = h_window - $('.header.navbar').outerHeight() - $('.page-control').outerHeight() - $('.page-filter').outerHeight() - $('.paginations').outerHeight() - 10;
        $('#table-manager .table-scrollable').css({'height': h_content + 'px'});
    });
}
resizeDocument();
$(window).resize(function() {
    resizeDocument()
});