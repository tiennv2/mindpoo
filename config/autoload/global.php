<?php

return array(
    'db' => array(
        'adapters' => array(
            'dbConfig' => array(
                'driver'   => 'Pdo_Mysql',
                'database' => 'mindpoo',
                'username' => 'root',
                'password' => '',
                'hostname' => 'localhost',
                'port'     => '',
                'charset'  => 'utf8'
            ),
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Db\Adapter\AdapterAbstractServiceFactory'
        )
    )
);

?>