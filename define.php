<?php
// Đường dẫn đến thư mục chứa thư mục hiện thời
chdir(dirname(__FILE__));

define('APP_KEY', 'x2017');

// Định nghĩa môi trường thực thi của ứng dụng
define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));	// development - production

// Định nghĩa đường dẫn đến thư mục ứng dụng
define('PATH_APPLICATION', realpath(dirname(__FILE__)));
define('PATH_MODULE', PATH_APPLICATION . '/module');
define('PATH_LIBRARY', PATH_APPLICATION . '/library');
define('PATH_VENDOR', PATH_APPLICATION . '/vendor');
define('PATH_PUBLIC', PATH_APPLICATION . '/public');
define('PATH_BLOCK', PATH_APPLICATION . '/block');
define('PATH_CAPTCHA', PATH_PUBLIC . '/captcha');
define('PATH_FILES', PATH_PUBLIC . '/files');
define('PATH_SCRIPTS', PATH_PUBLIC . '/scripts');
define('PATH_TEMPLATE', PATH_PUBLIC . '/template');

// Định nghĩa đường dẫn url
define('URL_APPLICATION', '');
define('URL_PUBLIC', URL_APPLICATION . '/public');
define('URL_FILES', URL_PUBLIC . '/files');
define('URL_SCRIPTS', URL_PUBLIC . '/scripts');
define('URL_TEMPLATE', URL_PUBLIC . '/template');

// HTMLPurifier
define('HTMLPURIFIER_PREFIX', PATH_VENDOR);

define('LANGUAGE', 'vi - Tiếng Việt;en - Tiếng Anh;jp - Tiếng Nhật');

// Table name
define('TABLE_PREFIX', 'x_');
define('TABLE_MENU',                TABLE_PREFIX . 'menu');
define('TABLE_SETTING',             TABLE_PREFIX . 'setting');
define('TABLE_DYNAMIC',             TABLE_PREFIX . 'dynamic');
define('TABLE_DOCUMENT',            TABLE_PREFIX . 'document');
define('TABLE_USER',                TABLE_PREFIX . 'user');
define('TABLE_USER_GROUP',          TABLE_PREFIX . 'user_group');
define('TABLE_USER_PERMISSION',     TABLE_PREFIX . 'user_permission');
define('TABLE_POST_CATEGORY',       TABLE_PREFIX . 'post_category');
define('TABLE_POST_ITEM',           TABLE_PREFIX . 'post_item');
define('TABLE_FORM',                TABLE_PREFIX . 'form');
define('TABLE_FORM_DATA',           TABLE_PREFIX . 'form_data');
define('TABLE_PRODUCT_CART',        TABLE_PREFIX . 'product_cart');
define('TABLE_CONTACT',             TABLE_PREFIX . 'contact');
define('TABLE_PAGE',                TABLE_PREFIX . 'page');
define('TABLE_EXERCISE',            TABLE_PREFIX . 'exercise');
define('TABLE_POST_TEST',           TABLE_PREFIX . 'post_test');

define('TABLE_COURSE_CATEGORY',     TABLE_PREFIX . 'course_category');
define('TABLE_COURSE_GROUP',        TABLE_PREFIX . 'course_group');
define('TABLE_COURSE_ITEM',         TABLE_PREFIX . 'course_item');
define('TABLE_COURSE_DETAIL',       TABLE_PREFIX . 'course_detail');

define('DOMAIN', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]");
if (APPLICATION_ENV == 'development') {
	define('DOMAIN_ERP', 'http://erp.langmaster');
} else {
	define('DOMAIN_ERP', 'https://langmaster.vn/erp');	
}

define('DOMAIN_ERP_ROOT', 'https://langmaster.vn');

define('TYPE_IELTS', json_encode(array(
	'post'  => 'Bài viết',
	'ielts' => 'Bài luyện thi'
)));
define('TYPE_CATE', json_encode(array(
	'default' => 'Bài viết',
	'product' => 'Sản phẩm',
	'gallery' => 'Album ảnh',
	'video'   => 'Video',
	'file'    => 'Tài liệu',
	'ielts'   => 'NEWS',
	'teacher' => 'Giảng viên'
)));
define('TYPE_EXERCISE', json_encode(array(
	'listenning'       => 'Nghe', 
	'speaking'         => 'Nói', 
	'reading'          => 'Đọc', 
	'writing'          => 'Viết',
	'grammar'          => 'Ngữ pháp (chia động từ, trắc nghiệm, viết lại câu)',
	'arrange_sentence' => 'Ngữ pháp (sắp xếp câu)',
)));
define('LOCATION_CITY', json_encode(array(
	'160273292871b628007748' => 'Hà Nội',
	'160273294565314847v7m5' => 'TP Hồ Chí Minh',
	'1602732951365197ms9477' => 'Tỉnh khác',
)));