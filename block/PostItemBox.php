<?php
namespace Block;
use Zend\View\Helper\AbstractHelper;

class PostItemBox extends AbstractHelper {
	
	public function __invoke($params = null, $options = null, $layout = 'default', $type_select = null){
	    $view      = $this->getView();
	    $settings  = $view->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
	    $language  = $view->viewModel['language'];
	    $language_active = $view->viewModel['language']['language_active'];
	    
	    $cache     = $view->getHelperPluginManager()->getServiceLocator()->get('cache');
	    $cache_key = 'PostItemBox_' . $options['box'] .'_'. $options['type'] .'_'. $language_active;
	    $result    = $cache->getItem($cache_key);
	    
	    if (empty($result)) {
	        $result    = '';
	        if ($type_select == 'most-view') {
	        	$table     = $view->getHelperPluginManager()->getServiceLocator()->get('Post\Model\PostItemTable');
	        	$items     = $table->listItem($options, array('task' => 'list-item-box-by-view'));
	        } else {
	        	$table     = $view->getHelperPluginManager()->getServiceLocator()->get('Post\Model\PostItemTable');
	        	$items     = $table->listItem($options, array('task' => 'list-item-box'));
	        }

	        require 'PostItemBox/'. $options['type'] .'_'. $layout .'.phtml';
	        
	        if($options['cache'] == true && $settings['General.System.Cache']['value'] == 'true') {
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    return $result;
	}
}