<?php
namespace Block;
use Zend\View\Helper\AbstractHelper;

class PostCategoryBox extends AbstractHelper {
	
	public function __invoke($params = null, $options = null, $layout = 'default'){
	    $view      = $this->getView();
	    $settings  = $view->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
	    $language  = $view->viewModel['language'];
	    $language_active = $view->viewModel['language']['language_active'];
	    
	    $cache     = $view->getHelperPluginManager()->getServiceLocator()->get('cache');
	    $cache_key = 'PostCategoryBox_' . $options['box'] .'_'. $options['type'] .'_'. $language_active;
	    $result    = $cache->getItem($cache_key);
	    	
	    if (empty($result)) {
	        $result        = '';
	        $table         = $view->getHelperPluginManager()->getServiceLocator()->get('Post\Model\PostItemTable');
	        $categories    = $table->listItem($options, array('task' => 'list-category-box'));
	        	
	        require 'PostCategoryBox/'. $options['type'] .'_'. $layout .'.phtml';
	        
	        if($options['cache'] == true && $settings['General.System.Cache']['value'] == 'true') {
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    return $result;
	}
}