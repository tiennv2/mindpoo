<?php
namespace Block;
use Zend\View\Helper\AbstractHelper;

class Menu extends AbstractHelper {
	
	public function __invoke($params = null, $options = null, $layout = 'default'){
	    $view      = $this->getView();
	    $settings  = $view->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
	    $language  = $view->viewModel['language'];
	    $language_active = $view->viewModel['language']['language_active'];
	    
	    $cache = $this->getView()->getHelperPluginManager()->getServiceLocator()->get('cache');
	    $cache_key = 'Menu_' . str_replace('.', '_', $options['code']) .'_'. $language_active;
	    $result = $cache->getItem($cache_key);
	    
	    if (empty($result)) {
	        $table     = $this->getView()->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\MenuTable');
    	    $arrMenu   = $table->listItem($options, array('task' => 'list-by-code'));
    	    
    	    $xhtml     = '';
    	    require 'Menu/'. $layout .'.phtml';
    	    
    	    if($options['cache'] == true && $settings['General.System.Cache']['value'] == 'true') {
    	        $cache->setItem($cache_key, $result);
    	    }
		}
	    
	    return $result;
	}
}