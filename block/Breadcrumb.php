<?php
namespace Block;
use Zend\View\Helper\AbstractHelper;

class Breadcrumb extends AbstractHelper {
	
	public function __invoke($params = null, $options = null, $layout = 'default'){
	    $view      = $this->getView();
	    $settings  = $view->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
	    $language  = $view->viewModel['language'];
	    $language_active = $view->viewModel['language']['language_active'];
	    
	    $result = '';
	    require 'Breadcrumb/'. $layout .'.phtml';
	    
	    return $result;
	}
}