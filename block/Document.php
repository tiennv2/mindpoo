<?php
namespace Block;
use Zend\View\Helper\AbstractHelper;

class Document extends AbstractHelper {
	
	public function __invoke($params = null, $code, $options = null){
	    $view      = $this->getView();
	    $settings  = $view->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
	    $language  = $view->viewModel['language'];
	    $language_active = $view->viewModel['language']['language_active'];
	    
	    $cache = $this->getView()->getHelperPluginManager()->getServiceLocator()->get('cache');
	    $cache_key = 'Document_'. str_replace('.', '_', $code) .'_'. $language_active;
	    $result = $cache->getItem($cache_key);
	     
	    if (empty($result)) {
	        $dynamic   = $view->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\DynamicTable')->getItem(array('code' => $code), array('task' => 'code'));
	        $items     = $view->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => $code, 'status' => 1)), array('task' => 'list-all'));
	        $result    = '';
	        $layout    = $code;
	        if(!empty($options['layout'])) {
	            $layout = $options['layout'];
	        }
	        require 'Document/'. $layout .'.phtml';
	        
	        if($options['cache'] == true && $settings['General.System.Cache']['value'] == 'true') {
	            $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    return $result;
	}
}